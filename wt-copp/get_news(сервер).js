<%

	var accept_image = ".jpg,.jpeg,.png";
	var accept_file = ".doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip";
	var role_docID = "0x5555555555555537"; // Раздел портала - новости
	var role_tags = undefined; // Директория тэгов

  function getDocs() {
    var result = [];
    var docsQuery = XQuery("for $elem in documents where $elem/parent_document_id = "+role_docID+" return $elem");

    for (doc in docsQuery) {
      teDoc = OpenDoc(UrlFromDocID(Int(doc.id))).TopElem;
      creator = teDoc.doc_info.creation;
      modificator = teDoc.doc_info.modification;
      creator_obj = {"id": String(creator.user_id), "login": String(creator.user_login), "fullname": undefined, "date": String(creator.date)};
      if (creator_obj.id) creator_obj.fullname = String(XQuery("for $elem in collaborators where $elem/id = "+creator_obj.id+" return $elem")[0].fullname);
      modificator_obj = {"id": String(modificator.user_id), "login": String(modificator.user_login), "fullname": undefined, "date": String(modificator.date)};
      if (modificator_obj.id) modificator_obj.fullname = String(XQuery("for $elem in collaborators where $elem/id = "+modificator_obj.id+" return $elem")[0].fullname);
	
      result.push({
        id: Int(doc.id),
        name: String(doc.name),
	comment: StrCharRangePos(String(teDoc.comment),0,50),
	images: [],
	image: (String(teDoc.resource_id) !="" ? "/download_file.html?file_id="+String(teDoc.resource_id) : ""),
	files: [],
	tags: getTags(teDoc),
	creator: getCreatorObj(teDoc).creator,
	modificator: getCreatorObj(teDoc).modificator,
	create: getCreateDate(doc.create_date),
	create_day: Day(doc.create_date),
	create_month: getMonth(Month(doc.create_date)),
	create_date: StrCharRangePos(String(doc.create_date),0,10),
	create_time: StrCharRangePos(String(doc.create_date),11,16)
      });
    }
    return result;
  }

function getCreatorObj (teDoc) {
	creator = teDoc.doc_info.creation;
	modificator = teDoc.doc_info.modification;
	creator_obj = {"id": String(creator.user_id), "login": String(creator.user_login), "fullname": undefined, "date": String(creator.date)};
	if (creator_obj.id) creator_obj.fullname = String(XQuery("for $elem in collaborators where $elem/id = "+creator_obj.id+" return $elem")[0].fullname);
	modificator_obj = {"id": String(modificator.user_id), "login": String(modificator.user_login), "fullname": undefined, "date": String(modificator.date)};
	if (modificator_obj.id) modificator_obj.fullname = String(XQuery("for $elem in collaborators where $elem/id = "+modificator_obj.id+" return $elem")[0].fullname);
	return {creator: creator_obj, modificator: modificator_obj};
}

function getCreateDate (date) {
	obj = {};
	obj.hour = String(Hour(date)).length == 1 ? "0"+Hour(date) : Hour(date);
	obj.minute = String(Minute(date)).length == 1 ? "0"+Minute(date) : Minute(date);
	obj.time = obj.hour+":"+obj.minute;
	obj.day = String(Day(date)).length == 1 ? "0"+Day(date) : Day(date);
	obj.month = Month(date);
	obj.month_name = getMonth(obj.month);
	obj.year = Year(date);
	obj.ISOdate = StrXmlDate(date, false);
	return obj;
}

function getMonth (month) {
	var a = "декабря";
	switch (month) {
		case 1: a = "января"; break;
		case 2: a = "февраля"; break;
		case 3: a = "марта"; break;
		case 4: a = "апреля"; break;
		case 5: a = "мая"; break;
		case 6: a = "июня"; break;
		case 7: a = "июля"; break;
		case 8: a = "августа"; break;
		case 9: a = "сентября"; break;
		case 10: a = "октября"; break;
		case 11: a = "ноября"; break;
	}
	return a;
}

function getAllTags () {
	alltags = XQuery("for $elem in tags return $elem");
	arrTags = [];
	for (t in alltags) {
		tagObject = {"id": Int(t.id), "name": String(t.name)};
		arrTags.push(tagObject);
	}
	return arrTags;
}

function getTags (doc) {
	docTags = [];
	for (t in doc.tags) {
		tagObject = {"id": Int(t.tag_id), "name": String(t.tag_name)};
		docTags.push(tagObject);
	}
	return docTags;
}

function getNewTags (arr) {
	arrTags = [];
	response.arr = arr;
	for (item in arr) {
		t = XQuery("for $elem in tags where $elem/id = "+item.id+" return $elem")[0];
		tagObject = {"id": String(t.id), "name": String(t.name), "desc": String(""), "require_acknowledgement": String("0")};
		arrTags.push(tagObject);
	}
	return arrTags;
}

function getFiles (teDoc, img) {
	arr = [];
	
	for (file in teDoc.files) {
		fileExist = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = "+XQueryLiteral(file.file_id)+" return $elem")) != undefined;
		if (fileExist) {
		try {
		teFile = OpenDoc(UrlFromDocID(Int(file.file_id))).TopElem;
		format =  ArraySort(StrLowerCase(String(teFile.file_url)).split("."), "key", "-")[0];
		sort = img ? accept_image : accept_file;
		if (StrContains(sort, format, false)) {
		arr.push({
			id: String( teFile.id ),
			name: String( teFile.name ),
			size: Int( teFile.size ),
			url: "/download_file.html?file_id=" + String( teFile.id )
		});
		}
		} catch (e) {
			response.error += e+"; ";
		}
		}
	}
	return arr;
}

function getImg (id) {
  fileExist = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = "+XQueryLiteral(id)+" return $elem")) != undefined;

  if (fileExist) {
	imgDoc = OpenDoc(UrlFromDocID(Int(id))).TopElem;
	obj = {};
	obj.id = String(id);
	obj.name = String(imgDoc.name);
	obj.url = "/download_file.html?file_id=" + String(id);
	return obj;
  }
}

function putFiles (arr, imgs) {
  for (file in arr) {
	bytes = StrLen( file.FileName );
  	fileName = String( file.FileName );
	dots = fileName.split(".");
	fileType = String(dots[dots.length-1]);

	newDoc = tools.new_doc_by_name('resource');
	newDoc.BindToDb(DefaultDb);
	localFile = fileName;
	xLocalFile = String("x-local://wt_data/attachments/" + newDoc.TopElem.id + "." + fileType);
	PutUrlData( xLocalFile, file );

	if (FilePathExists( UrlToFilePath( xLocalFile ) )) {
	fileSize = UrlFileSize( xLocalFile );
	} else {
	fileSize = bytes;
	}
	newDoc.TopElem.file_url = xLocalFile;
	newDoc.TopElem.size = fileSize;
	newDoc.TopElem.name = fileName;
	newDoc.TopElem.file_name = fileName;
	newDoc.TopElem.allow_unauthorized_download = 1;
	newDoc.TopElem.type = imgs ? "img" : "file";
	newChild = doc.TopElem.files.AddChild();
	newChild.file_id = String(newDoc.TopElem.id);
	//if (imgs) newChild.desc = "gallery";

	newDoc.Save();
  }
}

function removeFiles (teDoc, img) {
  arr = teDoc.files;
  removed = [];

  for (file in arr) {
	fileExist = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = "+XQueryLiteral(file.file_id)+" return $elem")) != undefined;

	if (fileExist) {
	try {
		fileDoc = OpenDoc(UrlFromDocID(Int(file.file_id))).TopElem;
		format =  ArraySort(StrLowerCase(String(fileDoc.file_url)).split("."), "key", "-")[0];
		sort = img ? accept_image : accept_file;
		if (StrContains( sort, format, false )) removed.push(file.file_id);
		
		//DeleteUrl(file.url);
	} catch (e) {
		response.error += e+"; ";
	}
	} else { removed.push(file.file_id) }
  }
  for (elem in removed) {
	for (file in arr) {
		if (file.file_id == elem) teDoc.files.DeleteChildByKey(elem);
	}
	//teDoc.files.DeleteChildByKey();
  }
}

function openInfoDoc(doc_id) {

  var result = {};
  teDoc = OpenDoc(UrlFromDocID(Int(doc_id))).TopElem;

  result.id = Int(teDoc.id);
  result.name = String(teDoc.name);
  result.text_area = String(teDoc.text_area);
  result.comment = StrCharRangePos(String(teDoc.comment),0,50);
  result.creator = getCreatorObj(teDoc).creator;
  result.modificator = getCreatorObj(teDoc).modificator;
  result.create = getCreateDate(teDoc.create_date);
  result.create_date = StrCharRangePos(String(teDoc.create_date),0,10);
  result.create_day = Day(teDoc.create_date);
  result.create_month = getMonth(Month(teDoc.create_date));
  result.create_time = StrCharRangePos(String(teDoc.create_date),11,16);
  result.image = teDoc.resource_id ? getImg(teDoc.resource_id) : [];
  result.files = getFiles(teDoc, false);
  result.images = getFiles(teDoc, true);
  result.lists = {};
  result.lists.tags = getAllTags();
  result.tags = getTags(teDoc);

  return result;

}

function createOrModify(form) {

  var doc;

  if (String(itemID).length) {

    doc = OpenDoc(UrlFromDocID(Int(itemID)));

  } else {

    doc = tools.new_doc_by_name('document');
    doc.BindToDb(DefaultDb);
    doc.TopElem.parent_document_id = '0x5555555555555537';
    //doc.TopElem.doc_info.creation.user_id = curUser.id;

  }
    response.user = String(Request.Session);


  if (form.HasProperty("name")) doc.TopElem.name = String(form.GetProperty("name"));
  if (form.HasProperty("text_area")) doc.TopElem.text_area = String(form.GetProperty("text_area"));
  if (form.HasProperty("comment")) doc.TopElem.comment = String(form.GetProperty("comment"));
  
  if (form.HasProperty("create_date")) {
	date = String(form.GetProperty("create_date"));
	if (date != "") {
	time = String(form.GetProperty("create_time"));
	time = time != "" ? time + ":00" : "00:00:00";
	doc.TopElem.create_date = date + " " + time;
	}
  }

  arri = [];
  arrf = [];
  for (i = 0; i <= 10; i++) {
	if (form.HasProperty("images"+i)) {
	  arri.push(form.GetProperty("images"+i));
	}
	if (form.HasProperty("files"+i)) {
	  arrf.push(form.GetProperty("files"+i));
	}
  }
  if (arri.length) {
	removeFiles(doc.TopElem, true);
	putFiles(arri, true);
  }
  if (arrf.length) {
	removeFiles(doc.TopElem, false);
	putFiles(arrf, true);
  }

  doc.TopElem.tags.Clear();
  arrTags = getNewTags(tools.read_object(form.GetProperty("tags")));
  for (t in arrTags) {
	_newobj = doc.TopElem.tags.AddChild();
	_newobj.tag_id = t.id;
	_newobj.tag_name = t.name;
	_newobj.desc = t.desc;
	_newobj.require_acknowledgement = t.require_acknowledgement;
  }

  response.arri = arri.length;
  response.arrf = arrf.length;
  response.tags = tools.read_object(form.GetProperty("tags"));

  response.id = String( doc.TopElem.id );
  doc.Save();


  return doc;

}

function deleteDoc(id) {

  var teDoc = OpenDoc(UrlFromDocID(Int(id))).TopElem;
  DeleteDoc(UrlFromDocID(Int(id)));

  // TODO: removes all files

}

var response = {};
response.error = '';

try {

  if (Request.Method == "GET") {

    if (Request.Query.HasProperty('id')) {
      response.data = openInfoDoc(Request.Query.id);
    } else {
      response.data = getDocs();
    }
    response.lists = {};
    response.lists.tags = getAllTags();
    //response.env = Request.Session.Env;

  } else if (Request.Method == "POST" ) {
    if (Request.Session.Env.curUserID == "") {
	response.error = "Доступ запрещён";
    } else {
    var form = Request.Form;
    var itemID = Request.Form.HasProperty( 'id' ) ? Request.Form.GetProperty( 'id' ) : "";

    response.body = tools.read_object(Request.Body);
    if (Request.Query.HasProperty('delete')) {
      deleteDoc(response.body.id);
    } else {
      createOrModify(form);
    }
    }
  }

} catch (err) { response.error = String(err); }

Response.Write(tools.object_to_text(response, 'json'));
%>