
if (Trim(action) == "create_action" || Trim(action) == "knowledge_part_selection" )
{
		if (Trim(action) == "create_action")
		{
			try
			{
				_module_programDoc =  OpenNewDoc( 'x-local://wtv/wtv_compound_program.xmd' );
				_module_programDoc.BindToDb( DefaultDb );	
				_module_programDoc.Save();
				
				RESULT.cp_id = _module_programDoc.DocID;
			}
			catch(err)
			{
				ERROR = 1;
				MESSAGE= "Error: " + err;
			}
		}

		if (Trim(action) == "knowledge_part_selection")
		{
			
			try
			{
				if (cp_knowledge_classifier_id != "" && cp_knowledge_classifier_id != undefined)
				{
					all_knowledges_arr = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = " +OptInt(cp_knowledge_classifier_id)+" return $elem");
					selected_knowledges_arr = XQuery("for $kp in knowledge_parts,$ko in knowledge_objects where $kp/knowledge_classifier_id = " + OptInt(cp_knowledge_classifier_id) + " and $ko/object_id = " + OptInt(cp_id) + " and $ko/knowledge_part_id = $kp/id  return $kp");
					curDoc = OpenDoc(UrlFromDocID(OptInt(cp_id)));
					curDocTE = curDoc.TopElem;
					_owned = "";
					knowledge_arr = [];
					kps = curDocTE.knowledge_parts;
					for (kp in kps)
					{
						try
						{
							kpDoc = OpenDoc(UrlFromDocID(OptInt(kp.knowledge_part_id)));
							kpTE = kpDoc.TopElem;
							if (OptInt(kpTE.knowledge_classifier_id) == OptInt(cp_knowledge_classifier_id))
							{
								_owned = _owned + " and $elem/id != "+OptInt(kp.knowledge_part_id);
							}
						}
						catch(err) {};
					}
					owned_knowledges_arr = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = " +OptInt(cp_knowledge_classifier_id)+""+_owned+" return $elem");
					
				}
				
				if (ArrayCount(selected_knowledges_arr) > 0) {selected_object_ids = ArrayMerge(selected_knowledges_arr, 'PrimaryKey', ';' )} else {selected_object_ids = ''}
				display_object_ids = ArrayMerge(all_knowledges_arr, 'PrimaryKey', ';' );
				
				RESULT.res_selected_object_ids = selected_object_ids;
				RESULT.res_display_object_ids = display_object_ids;
				RESULT.res_classifier_id = cp_knowledge_classifier_id;
			}
			catch(err)
			{
				ERROR = 1;
				MESSAGE = "Error: " + err;
			}

		}
		
		
}
else
{

	if (cp_id != undefined && cp_id != '')
	{
		try
		{
			curDoc = OpenDoc(UrlFromDocID(OptInt(cp_id)));
			curDocTE = curDoc.TopElem;


			if (Trim(action) == "save_new_cp_first_time")
			{

				curDocTE.custom_elems.ObtainChildByKey("cp_moderator").value = cp_moderator;
				curDocTE.custom_elems.ObtainChildByKey("cp_status").value = "Проект";
				curDocTE.custom_elems.ObtainChildByKey('event_form').value = cp_event_form;
				try
				{
					_cp_moderator = ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/id="+OptInt(cp_moderator)+" return $elem"));

					
					if (_cp_moderator != undefined)
					{
						
						if (_cp_moderator.org_id != '')
						{
							
							curDocTE.custom_elems.ObtainChildByKey('educ_org').value = OptInt(_cp_moderator.org_id);
							org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+_cp_moderator.org_id+"' and $elem/catalog_name='compound_program' return $elem"));

							if (org_category == undefined)
							{
								_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
								_new_org_category.BindToDb( DefaultDb );
								_new_org_category.Save();
								_new_org_category.TopElem.code = "org_"+cp_moderator.org_id;
								_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(_cp_moderator.org_id)+"return $elem")).name;
								_new_org_category.TopElem.catalog_name = "compound_program";
								_new_org_category.Save();
								org_category = _new_org_category;
								for (role in curDocTE.role_id)
								{
									role.Delete();
								}
								curDocTE.role_id.ObtainByValue(OptInt(org_category.DocID));
								
							}
							else
							{
								_role_exists = false;
								for (role in curDocTE.role_id)
								{
									if (OptInt(role) == OptInt(org_category.id))
									{
										_role_exists = true;
									}
								}
								if (_role_exists == false)
								{
									curDocTE.role_id.ObtainByValue(OptInt(org_category.id));
								}
							}
						
						}
					}
				}
				catch(err)
				{
					ERROR=1;
					MESSAGE = err;
				}
				curDoc.Save();
			}
			
			if (Trim(action) == "change_cp_status")
			{
				try
				{
					cp_list_pageDoc = OpenDoc(UrlFromDocID(OptInt(copp_compound_programs_list_page)));
					cp_list_pageDocTE = cp_list_pageDoc.TopElem;
					cp_catalog = "";
					_is_cp_is_in_catalog = false;
					cps_to_delete_or_add_from_cp_list_page_arr = [];
					for (catalog in cp_list_pageDocTE.catalogs)
					{
						if (catalog.type == "compound_program")
						{
							cp_catalog = catalog;
							for (object in catalog.objects)
							{
								if (OptInt(object.object_id) == OptInt(curDoc.DocID))
								{
									_is_cp_is_in_catalog = true;
									cps_to_delete_or_add_from_cp_list_page_arr.push(object);
								}
							}
						}
					}
					_file_id = "";
					if (cp_status_attached_file != "" && cp_status_attached_file != undefined && cp_status_attached_file != null)
					{
						_file_id = "<brk> Прикрепленный ресурс<splt> " + "id"+cp_status_attached_file;
					}
					_col = ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/id = " +curUserID+" return $elem")).fullname;
					curDocTE.custom_elems.ObtainChildByKey('status_log').value = 
					curDocTE.custom_elems.ObtainChildByKey('status_log').value + " /  Дата<splt> "+ Date() + "<brk> Кто менял<splt> "+ _col + 
						"<brk> Сменил со статуса<splt> " + curDocTE.custom_elems.ObtainChildByKey("cp_status").value + "<brk> Поменял на статус<splt> " 
						+ cp_status   + "<brk> Комментарий<splt> " + cp_status_comment + _file_id;
					curDocTE.custom_elems.ObtainChildByKey("cp_status").value = cp_status;
					curDoc.Save();
					if (Trim(cp_status) == "Принята")
					{
						
						if (_is_cp_is_in_catalog == false)
						{
							if (cp_catalog != "")	
							{
								added_cp = cp_catalog.objects.AddChild();
								added_cp.object_id = OptInt(curDoc.DocID);
								cp_list_pageDoc.Save();
							}
							
						}
					}
					else
					{

						if (_is_cp_is_in_catalog == true) 
						{
							for (cp in cps_to_delete_or_add_from_cp_list_page_arr)
							{
								cp.Delete();
							}
							cp_list_pageDoc.Save();
						}
					}
					
					RESULT.msg = "Статус изменен на: <b>" + cp_status+"</b>"; //+ " Comment: " + cp_status_comment + " cp_file: " + cp_status_attached_file;
				}
				catch(err)
				{
					ERROR=1;
					MESSAGE = "Ошибка смены статуса: " + err;
				}
			}
			
			
			curDocTE.name = String(cp_name);

			curDocTE.desc = String(cp_desc);

			curDocTE.custom_elems.ObtainChildByKey('cp_moderator').value = cp_moderator;

			curDocTE.custom_elems.ObtainChildByKey('cp_educ_type').value = copp_cp_type_selector;

			curDocTE.custom_elems.ObtainChildByKey('issued_document').value = issued_doc_id; 

			cp_educ_org = OptInt(Trim(cp_educ_org));

			if (Trim(action) != "save_new_cp_first_time")
			{		
				
				//меняем каталоги у всего, что связано с модульной программой, если организация была изменена

				if (String(Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)) != String(Trim(cp_educ_org)))
				{
					
					if (cp_educ_org != undefined)
					{
						//модульгая программа
						
						try
						{
							prev_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+"' and $elem/catalog_name='compound_program' return $elem"));
							new_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+cp_educ_org+"' and $elem/catalog_name='compound_program' return $elem"));

							if (new_org_category == undefined)
							{
								_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
								_new_org_category.BindToDb( DefaultDb );
								_new_org_category.Save();
								_new_org_category.TopElem.code = "org_"+cp_educ_org;
								_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(cp_educ_org)+"return $elem")).name;
								_new_org_category.TopElem.catalog_name = "compound_program";
								_new_org_category.Save();
								new_org_category = _new_org_category;

								for (role in curDocTE.role_id)
								{
									if (prev_org_category != undefined)
									{
										if (OptInt(role) == OptInt(prev_org_category.id))
										{
											try{role.Delete();}catch(err){}
										}
									}
									
								}

								curDocTE.role_id.ObtainByValue(OptInt(new_org_category.DocID));
							}
							else
							{
								_role_exists = false;
								for (role in curDocTE.role_id)
								{
									if (prev_org_category != undefined)
									{
										if (OptInt(role) == OptInt(prev_org_category.id))
										{
											try{role.Delete();}catch(err){}
										}
									}
									if (OptInt(role) == OptInt(new_org_category.id))
									{
										_role_exists = true;
										
									}
								}
								if (_role_exists == false)
								{

									curDocTE.role_id.ObtainByValue(OptInt(new_org_category.id));
								}
							}
							curDoc.Save();
						}
						catch(err)
						{

						}

					}
					else
					{
						prev_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value+"' and $elem/catalog_name='compound_program' return $elem"));
						for (role in curDocTE.role_id)
						{
							if (prev_org_category != undefined)
							{
								if (OptInt(role) == OptInt(prev_org_category.id))
								{
									try{role.Delete();}catch(err){}

								}
							}
						}
						curDoc.Save();
					}


					//набор программ
					for (ep in curDocTE.programs)
					{

						if (ep.type=="education_program")
						{
							education_program = ArrayOptFirstElem(XQuery("for $elem in education_programs where $elem/id="+OptInt(ep.object_id)+" return $elem"));

							if (education_program != undefined)
							{
								education_programDoc = OpenDoc(UrlFromDocID(OptInt(education_program.id)));
								education_programDocTE = education_programDoc.TopElem;
								try
								{
									prev_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+"' and $elem/catalog_name='education_program' return $elem"));
									new_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+cp_educ_org+"' and $elem/catalog_name='education_program' return $elem"));

									if (new_org_category == undefined)
									{
										_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
										_new_org_category.BindToDb( DefaultDb );
										_new_org_category.Save();
										_new_org_category.TopElem.code = "org_"+cp_educ_org;
										_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(cp_educ_org)+"return $elem")).name;
										_new_org_category.TopElem.catalog_name = "education_program";
										_new_org_category.Save();
										new_org_category = _new_org_category;

										for (role in education_programDocTE.role_id)
										{
											if (prev_org_category != undefined)
											{
												if (OptInt(role) == OptInt(prev_org_category.id))
												{
													try{role.Delete();}catch(err){}
												}
											}
											
										}

										education_programDocTE.role_id.ObtainByValue(OptInt(new_org_category.DocID));
									}
									else
									{
										_role_exists = false;
										for (role in education_programDocTE.role_id)
										{
											if (prev_org_category != undefined)
											{
												if (OptInt(role) == OptInt(prev_org_category.id))
												{
													try{role.Delete();}catch(err){}
												}
											}
											if (OptInt(role) == OptInt(new_org_category.id))
											{
												_role_exists = true;
												
											}
										}
										if (_role_exists == false)
										{

											education_programDocTE.role_id.ObtainByValue(OptInt(new_org_category.id));
										}
									}
									education_programDoc.Save();
								}
								catch(err)
								{

								}


								//учебные программы (Занятия)

								try
								{

									for (em in education_programDocTE.education_methods)
									{
										education_method = ArrayOptFirstElem(XQuery("for $elem in education_methods where $elem/id="+OptInt(em.education_method_id)+" return $elem"));

										if (education_method != undefined)
										{
											education_methodDoc = OpenDoc(UrlFromDocID(OptInt(education_method.id)));
											education_methodDocTE = education_methodDoc.TopElem;

											try
											{
												prev_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+"' and $elem/catalog_name='education_method' return $elem"));
												new_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+Trim(cp_educ_org)+"' and $elem/catalog_name='education_method' return $elem"));

												if (new_org_category == undefined)
												{
													_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
													_new_org_category.BindToDb( DefaultDb );
													_new_org_category.Save();
													_new_org_category.TopElem.code = "org_"+cp_educ_org;
													_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(cp_educ_org)+"return $elem")).name;
													_new_org_category.TopElem.catalog_name = "education_method";
													_new_org_category.Save();
													new_org_category = _new_org_category;

													for (role in education_methodDocTE.role_id)
													{
														if (prev_org_category != undefined)
														{
															if (OptInt(role) == OptInt(prev_org_category.id))
															{
																try{role.Delete();}catch(err){}
															}
														}
														
													}

													education_methodDocTE.role_id.ObtainByValue(OptInt(new_org_category.DocID));
												}
												else
												{
													_role_exists = false;
													for (role in education_methodDocTE.role_id)
													{
														if (prev_org_category != undefined)
														{
															if (OptInt(role) == OptInt(prev_org_category.id))
															{
																try{role.Delete();}catch(err){}
															}
														}
														if (OptInt(role) == OptInt(new_org_category.id))
														{
															_role_exists = true;
															
														}
													}
													if (_role_exists == false)
													{

														education_methodDocTE.role_id.ObtainByValue(OptInt(new_org_category.id));
													}
												}
												education_methodDoc.Save();
											}
											catch(err)
											{

											}
	
											//электронный курс

											if (education_methodDocTE.type == 'course' && education_methodDocTE.course_id != '' && education_methodDocTE.course_id != undefined)
											{
												course = ArrayOptFirstElem(XQuery("for $elem in courses where $elem/id="+OptInt(education_methodDocTE.course_id)+" return $elem"));

												if (course != undefined)
												{
													courseDoc = OpenDoc(UrlFromDocID(OptInt(course.id)));
													courseDocTE = courseDoc.TopElem;
													
													try
													{
														prev_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+"' and $elem/catalog_name='course' return $elem"));
														new_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+cp_educ_org+"' and $elem/catalog_name='course' return $elem"));

														if (new_org_category == undefined)
														{
															_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
															_new_org_category.BindToDb( DefaultDb );
															_new_org_category.Save();
															_new_org_category.TopElem.code = "org_"+cp_educ_org;
															_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(cp_educ_org)+"return $elem")).name;
															_new_org_category.TopElem.catalog_name = "course";
															_new_org_category.Save();
															new_org_category = _new_org_category;

															for (role in courseDocTE.role_id)
															{
																if (prev_org_category != undefined)
																{
																	if (OptInt(role) == OptInt(prev_org_category.id))
																	{
																		try{role.Delete();}catch(err){}
																	}
																}
																
															}

															courseDocTE.role_id.ObtainByValue(OptInt(new_org_category.DocID));
														}
														else
														{
															_role_exists = false;
															for (role in courseDocTE.role_id)
															{
																if (prev_org_category != undefined)
																{
																	if (OptInt(role) == OptInt(prev_org_category.id))
																	{
																		try{role.Delete();}catch(err){}
																	}
																}
																if (OptInt(role) == OptInt(new_org_category.id))
																{
																	_role_exists = true;
																	
																}
															}
															if (_role_exists == false)
															{

																courseDocTE.role_id.ObtainByValue(OptInt(new_org_category.id));
															}
														}
														courseDoc.Save();
													}
													catch(err)
													{

													}

													//ресурсы курса

													for (part in courseDocTE.parts)
													{

														if (part.type == 'resource')
														{
															if (part.object_id != "" && part.object_id != undefined)
															{
																resource = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id="+OptInt(part.object_id)+" return $elem"));

																if (resource != undefined)
																{
																	resourceDoc = OpenDoc(UrlFromDocID(OptInt(resource.id)));
																	resourceDocTE = resourceDoc.TopElem;

																	try
																	{
																		prev_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+"' and $elem/catalog_name='resource' return $elem"));
																		new_org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+cp_educ_org+"' and $elem/catalog_name='resource' return $elem"));

																		if (new_org_category == undefined)
																		{
																			_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
																			_new_org_category.BindToDb( DefaultDb );
																			_new_org_category.Save();
																			_new_org_category.TopElem.code = "org_"+cp_educ_org;
																			_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(cp_educ_org)+"return $elem")).name;
																			_new_org_category.TopElem.catalog_name = "resource";
																			_new_org_category.Save();
																			new_org_category = _new_org_category;

																			for (role in resourceDocTE.role_id)
																			{
																				if (prev_org_category != undefined)
																				{
																					if (OptInt(role) == OptInt(prev_org_category.id))
																					{
																						try{role.Delete();}catch(err){}
																					}
																				}
																				
																			}

																			resourceDocTE.role_id.ObtainByValue(OptInt(new_org_category.DocID));
																		}
																		else
																		{
																			_role_exists = false;
																			for (role in resourceDocTE.role_id)
																			{
																				if (prev_org_category != undefined)
																				{
																					if (OptInt(role) == OptInt(prev_org_category.id))
																					{
																						try{role.Delete();}catch(err){}
																					}
																				}
																				if (OptInt(role) == OptInt(new_org_category.id))
																				{
																					_role_exists = true;
																					
																				}
																			}
																			if (_role_exists == false)
																			{

																				resourceDocTE.role_id.ObtainByValue(OptInt(new_org_category.id));
																			}
																		}
																		resourceDoc.Save();
																	}
																	catch(err)
																	{

																	}

																}


															}

															
														}

													}
													//конец обработки ресурсов
												}

											}
											//конец обработки курсов

										}

									}

								}
								catch(err)
								{

								}
								//конец обработки занятий

							}

						}

					}
					//конец обработки набора программ
					
	
					curDocTE.custom_elems.ObtainChildByKey('educ_org').value = cp_educ_org;

				}
				//

				
			}

			if (cp_img != '' && cp_img != null && cp_img != undefined)
			{
				curDocTE.resource_id.Clear();
				curDocTE.resource_id = OptInt(cp_img);
				
			}
			else
			{
				curDocTE.resource_id.Clear();
			}
			
			if (curDocTE.code == "")
			{
				//curDocTE.code = "cmp_pr_"+Random(0,999999999);
				curDocTE.code = "cmp_pr_"+curDocTE.id;
			}

			if (cp_knowledge_level_id != '' && cp_knowledge_level_id != undefined &&  cp_knowledge_level_id != null)
			{
				kps = curDocTE.knowledge_parts;
			
				for (kp in kps)
				{
					try
					{
						kpDoc = OpenDoc(UrlFromDocID(OptInt(kp.knowledge_part_id)));
						kpTE = kpDoc.TopElem;
						if (OptInt(kpTE.knowledge_classifier_id) == OptInt(cp_knowledge_level_classifier_id))
						{
							kp.Delete();
						}
					}
					catch(err)
					{

					}
				}
				_knowledge = ArrayOptFirstElem(XQuery("for $elem in knowledge_parts where $elem/id = " + OptInt(cp_knowledge_level_id)+" return $elem"));
				new_knowledge_level = kps.AddChild();
				new_knowledge_level.knowledge_part_id = OptInt(cp_knowledge_level_id);
				new_knowledge_level.knowledge_part_name = _knowledge.name;
			}

			if (Trim(action) == "add_knowledges")
			{
				knowledges_ids_arr = ParseJson(selected_object_ids);
				kp_to_delete_arr = [];
				added_list = "";
				deleted_list = "";
				for (kp in curDocTE.knowledge_parts)
				{
					try
					{
						kpDoc = OpenDoc(UrlFromDocID(OptInt(kp.knowledge_part_id)));
						kpTE = kpDoc.TopElem;
						
						if (OptInt(kpTE.knowledge_classifier_id) == OptInt(cp_knowledge_classifier_id))
						{
							
							try
							{
								kp_to_delete_arr.push(kp);
								deleted_list = deleted_list + "["+kp.knowledge_part_name + "] ";

							}
							catch(err)
							{
								RESULT.alrt = "Ошибка: " + err;
							}
						}
					}
					catch(err)
					{

					}
					
					
				
				}
				
				for (kp in kp_to_delete_arr)
				{
					
					kp.Delete();
				}
				
				for (knowledge_id in knowledges_ids_arr)
				{
					  knowledge = ArrayOptFirstElem(XQuery("for $elem in knowledge_parts where $elem/id = " + OptInt(knowledge_id)+" return $elem"));
					_new_knowledge = curDocTE.knowledge_parts.AddChild();
					_new_knowledge.knowledge_part_id = OptInt(knowledge_id);
					_new_knowledge.knowledge_part_name = knowledge.name;
					added_list = added_list + "["+knowledge.name + "] ";
					
				}
				//RESULT.alrt = "<b>Успешно привязаны:</b> " + added_list + " <br><b>Отвязаны:</b> " + deleted_list;
				RESULT.alrt = "Изменения успешно сохранены";
			}
			if (Trim(action) == "delete_knowledge")
			{
				comp_id = selected_object_ids;
				for (kp in curDocTE.knowledge_parts)
				{
					if (OptInt(kp.knowledge_part_id) == OptInt(comp_id))
					{
						try
						{
							kp.Delete();
						}
						catch(err)
						{
							MESSAGE = "Ошибка: " + err;
						}

					}
				}

			}

			//собираем все цены учебных программ в наборе программ,а также количество участников в общую сумму и записываем эти данные в модульную программу
		
			dollar_course = 1;
			euro_course = 1;
			eng_fund_sterling_course = 1;
			grivna_course = 1;				
			for (program in curDocTE.programs)
			{
				duration = 0;
				person_num = 0;
				cost = 0;
				currency = 'RUR';
				education_methods_arr = XQuery("for $elem in education_program_education_methods where $elem/education_program_id = " + OptInt(program.object_id) + " return $elem");
				for (ep in education_methods_arr)
				{
					if (ep.cost != '' && OptInt(ep.cost) != undefined)
					{
						if (ep.cost_type == 'group')
						{
							if (ep.currency == '')
							{
								cost =  OptInt(cost) + OptInt(ep.cost);
							}
							if (ep.currency == 'RUR')
							{
								cost =  OptInt(cost) + OptInt(ep.cost);
							}
							if (ep.currency == 'USD')
							{
								cost = OptInt(cost) + OptInt(ep.cost) * dollar_course;
							}
							if (ep.currency == 'EUR')
							{
								cost = OptInt(cost) + OptInt(ep.cost) * euro_course;
							}
							if (ep.currency == 'GBR')
							{
								cost = OptInt(cost) + OptInt(ep.cost) * eng_fund_sterling_course;
							}
							if (ep.currency == 'UAH')
							{
								cost = OptInt(cost) + OptInt(ep.cost) * grivna_course;
							}
						}
						else if (ep.cost_type == 'person')
						{
							if (ep.currency == '')
							{
								cost =  OptInt(cost) + (OptInt(ep.cost) * OptInt(ep.person_num)) ;
							}			
							if (ep.currency == 'RUR')
							{
								cost =  OptInt(cost) + (OptInt(ep.cost) * OptInt(ep.person_num)) ;
							}
							if (ep.currency == 'USD')
							{
								cost = OptInt(cost) + ((OptInt(ep.cost) *  dollar_course )* OptInt(ep.person_num));
							}
							if (ep.currency == 'EUR')
							{
								cost = OptInt(cost) + ((OptInt(ep.cost) *  euro_course )* OptInt(ep.person_num));
							}
							if (ep.currency == 'GBR')
							{
								cost = OptInt(cost) + ((OptInt(ep.cost) *  eng_fund_sterling_course )* OptInt(ep.person_num));
							}
							if (ep.currency == 'UAH')
							{
								cost = OptInt(cost) + ((OptInt(ep.cost) *  grivna_course )* OptInt(ep.person_num));
							}
						}
					}
					else
					{
						if (OptInt(cost) == undefined)
						{
							cost = 0;
						}
					}
					if (ep.duration != "" && OptInt(ep.duration) != undefined)
					{
						try {duration = OptInt(duration) + OptInt(ep.duration)}catch(err){duration=duration;}
					}
					if (ep.person_num != "" && OptInt(ep.person_num) != undefined)
					{
						try {person_num = OptInt(person_num) + OptInt(ep.person_num)}catch(err){}
					}

				
				}
				try
				{
					program.duration = duration;
				}
				catch(err)
				{
					program.duration = 0;
				}
				//program.person_num = OptInt(person_num);
				try
				{
					program.cost = OptInt(cost);
				}
				catch(err)
				{
					program.cost = OptInt(0);
				}
				
			}
			
			curDoc.Save();

			if (Trim(action) != "module_data_update")
			{
				ERROR = 0;
				MESSAGE = 'Изменения <span style="color:green">успешно</span> сохранены';
			}
			if (Trim(action) == "module_data_update")
			{
				ERROR = 0;
				MESSAGE = 'Данные <span style="color:green">успешно</span> обновлены';
			}
		}
		catch(err)
		{
			ERROR = 1;
			MESSAGE = "Ошибка: "+err;
		}
		

	}
	else
	{

		ERROR = 1;
		MESSAGE = "Ошибка: module_program_id is null"  + cp_id;
		
	}
}


