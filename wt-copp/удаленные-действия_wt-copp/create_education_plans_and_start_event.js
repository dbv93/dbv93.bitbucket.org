try
{
	_eventDoc = "";
	_group_for_eventDoc = "";
	total_count = 0;
	objects_to_count = 0;
	object_type = object_catalog;
	try
	{
		_eventDoc = OpenDoc(UrlFromDocID(OptInt(event_id)));
		
	}
	catch(err)
	{
		ERROR = 1;
		MESSAGE = "Некорректный ID мероприятия";
	}
	
	
	if (_eventDoc != "" && _eventDoc != undefined)
	{
		_eventDocTE = _eventDoc.TopElem;
		total_count = ArrayCount(_eventDocTE.collaborators);
		
		if (Trim(action) == 'start')
		{
			if (_eventDocTE.status_id == 'project' || _eventDocTE.status_id == 'plan')
			{
				for (group in _eventDocTE.groups)
				{
					for (col in _eventDocTE.collaborators)
					{
						_epDoc = OpenNewDoc( 'x-local://wtv/wtv_education_plan.xmd' );
						_epDoc.BindToDb( DefaultDb );
						_epDoc.Save();
						_epDocTE = _epDoc.TopElem;
						_epDocTE.person_id = col.collaborator_id;
						_epDocTE.person_fullname = col.person_fullname;
						_epDocTE.code =  _eventDocTE.code;
						_epDocTE.group_id = group.group_id;
						_epDocTE.event_id = _eventDoc.DocID;
						_epDocTE.create_date = Date();
						if (_eventDocTE.finish_date != "" && OptDate(_eventDocTE.finish_date) != undefined)
						{
							_epDocTE.finish_date = OptDate(_eventDocTE.finish_date);
						}
						_epDocTE.state_id = 0;
						if (String(Trim(object_type)) == "compound_program")
						{
							_epDocTE.compound_program_id = _eventDocTE.compound_program_id;
							_epDocTE.name =  "План обучение по "  +  _eventDocTE.compound_program_id.ForeignElem.name;
							objectDoc = OpenDoc(UrlFromDocID(OptInt(_eventDocTE.compound_program_id)));
							objectDocTE = objectDoc.TopElem;
							
							for (ep in objectDocTE.programs)
							{
								added_program = _epDocTE.programs.AddChild();
								//tools.object_filling( 'education_plan', _epDocTE, em.id);
								tools.common_filling( 'collaborator', _epDocTE,  col.collaborator_id);
								added_program.name = ep.name;
								added_program.type = "folder";
								added_program.create_date = Date();
								added_program.result_type = "active_learning";
								added_program.state_id = 0;
								added_program.required = 1;
								tools.activate_education_program_to_person(col.collaborator_id, ep.object_id);
								_epDoc.Save();

								if (OptInt(ep.object_id) != undefined && Trim(ep.object_id) != "")
								{
									education_program_education_methods_arr = XQuery("for $elem in education_program_education_methods where $elem/education_program_id="+OptInt(ep.object_id)+" order by $elem/code ascending return $elem");
									education_methods_all_arr = XQuery("for $elem in education_methods where $elem/type='course' and $elem/course_id !='' order by $elem/code ascending return $elem");
									education_methods_arr = ArrayIntersect(education_methods_all_arr, education_program_education_methods_arr,"This.id", "This.education_method_id");

									for (em in education_methods_arr)
									{
			
										added_course = _epDocTE.programs.AddChild();
										try
										{
											added_course.name = em.course_id.ForeignElem.name;
										}
										catch(err)
										{

										}
										added_course.type = "course";
										added_course.education_method_id = em.id;
										added_course.object_id = em.course_id;
										added_course.create_date = added_program.create_date;
										added_course.result_type = added_program.result_type;
										added_course.state_id = added_program.state_id;
										added_course.required = added_program.required;
										added_course.parent_progpam_id = added_program.id;
						
							
										/*_courseDocTE = OpenDoc(UrlFromDocID(OptInt(em.course_id))).TopElem;

										if (_courseDocTE != undefined && _courseDocTE.ChildExists('parts'))
										{
											for (part in _courseDocTE.parts)
											{
												if (part.type == 'lesson')
												{
													_course_to_assign = ArrayOptFirstElem(XQuery("for $elem in courses where $elem/base_url='"+String(part.url)+"' return $elem"));
													if (_course_to_assign != undefined)
													{
														assigned_course =  tools.activate_course_to_person( OptInt(col.collaborator_id), OptInt(_course_to_assign.id), "" , "",  "" ,""  , "","" , "",OptInt(_epDoc.DocID));
														added_sub_course =  _epDocTE.programs.AddChild();
														added_course.type = "course";
														added_course.education_method_id = em.id;
														added_course.object_id = _course_to_assign.id;
														added_course.create_date = added_program.create_date;
														added_course.result_type = added_program.result_type;
														added_course.state_id = added_program.state_id;
														added_course.required = added_program.required;
														added_course.parent_progpam_id = added_course.id;
														
													}
												}
												if (part.type == 'test')
												{
													_test_to_assign = ArrayOptFirstElem(XQuery("for $elem in assessments where $elem/base_url='"+String(part.assessment_id)+"' return $elem"));
													if (_test_to_assign != undefined)
													{
														assigned_assessment =  tools.activate_course_to_person( OptInt(col.collaborator_id), OptInt(_course_to_assign.id), "" , "",  "" ,""  , "","" , "",OptInt(_epDoc.DocID));
														added_sub_course =  _epDocTE.programs.AddChild();
														added_course.type = "course";
														added_course.education_method_id = em.id;
														added_course.object_id = _course_to_assign.id;
														added_course.create_date = added_program.create_date;
														added_course.result_type = added_program.result_type;
														added_course.state_id = added_program.state_id;
														added_course.required = added_program.required;
														added_course.parent_progpam_id = added_course.id;
														
													}
												}
											}
										}*/
									}
								}
							

							}

							for (custom_elem in objectDocTE.custom_elems)
							{
								new_elem = _epDocTE.custom_elems.AddChild();
								new_elem.name = custom_elem.name;
								new_elem.value = custom_elem.value;
							}
							
						}
						if (String(Trim(object_type)) == "education_method")
						{
							_epDocTE.compound_program_id = _eventDocTE.education_method_id;
							_epDocTE.name =  "План обучение по "  +  _eventDocTE.education_method_id;
							objectDoc = OpenDoc(UrlFromDocID(OptInt(_eventDocTE.education_method_id)));
							objectDocTE = objectDoc.TopElem;
							added_program = _epDocTE.programs.AddChild();
							tools.common_filling( 'collaborator', _epDocTE,  col.collaborator_id);
							added_program.name = objectDocTE.name;
							added_program.type = 'education_method';
							added_program.object_id = objectDoc.DocID;
							added_program.create_date = Date();
							added_program.result_type = "active_learning";
							added_program.state_id = 0;
							added_program.required = 1;
							for (custom_elem in objectDocTE.custom_elems)
							{
								new_elem = _epDocTE.custom_elems.AddChild();
								new_elem.name = custom_elem.name;
								new_elem.value = custom_elem.value;
							}
							
						}
						_epDoc.Save();
						objects_to_count++;
						
					}
				}
				
				ERROR=0;
				RESULT.event_id = _eventDoc.DocID;

				if (objects_to_count > 0)
				{
					_eventDocTE.status_id = 'active';
					_eventDoc.Save();
					MESSAGE = 'Учебные планы <span style="color:green">успешно</span> созданы для: '+objects_to_count+' / ' + total_count + ' сотрудника(ов) ';
				}
				else
				{
					MESSAGE = 'Операция <span style="color:red">прервана</span><br> В данной учебной группе не было найдено ни одного сотрудника';
				}
			}
			else
			{
				ERROR = 1;
				if (_eventDocTE.status_id == 'active')
				{
					MESSAGE = 'Операция <span style="color:red">прервана</span><br> Данное учебное мероприятие уже <span style="color:green">активировано</span>';
				}
				if (_eventDocTE.status_id == 'close')
				{
					MESSAGE = 'Операция <span style="color:red">прервана</span><br> Данное учебное мероприятие уже <span style="color:grey">завершено</span>';
				}
				if (_eventDocTE.status_id == 'cancel')
				{
					MESSAGE = 'Операция <span style="color:red">прервана</span><br> Данное учебное мероприятие было <span style="color:orange">отменено</span>';
				}
			}
		}
		if (Trim(action) == 'cancel')
		{
			try
			{
				_eventDocTE.status_id = 'cancel';
				_eventDoc.Save();
				ERROR = 0;
				MESSAGE = 'Мероприятие  <span style="color:orange">отменено</span>';
			}
			catch(err)
			{
				ERROR = 1;
				MESSAGE = err;
			}
		}
		
			
	}
	else
	{
		ERROR = 1;
		MESSAGE  = "Ошибка при попытке открыть карточку объекта";
	}
	
	

}
catch(err)
{
	ERROR=1;
	MESSAGE = "Ошибка: " + err;
}