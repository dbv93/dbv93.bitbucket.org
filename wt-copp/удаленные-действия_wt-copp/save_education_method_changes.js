
if (Trim(action) == "add_new_file" || Trim(action) == "save_mark_params" || Trim(action) == "edit_resource_mark_type" ||  Trim(action)  == "delete_attached_file")
{
		if (Trim(action) == "edit_resource_mark_type")
		{
			_courseDoc=OpenDoc(UrlFromDocID(OptInt(p_attached_course_id)));
			_courseDocTE = _courseDoc.TopElem;
			_the_part = "";
			for (part in _courseDocTE.parts)
			{
				if (Trim(part.code) == Trim(p_attached_course_part))
				{
					_the_part = part;
				}
			}
		
			if (_the_part != '')
			{

				try
				{
					_the_part.max_score = OptInt(Trim(p_attached_file_mark_type));
					_courseDoc.Save();
					MESSAGE = "Изменения успешно применены";
				}
				catch(err)
				{
					ERROR=1;
					MESSAGE = "ERROR: " + err;
				}
			}
			else
			{
				ERROR = 1;
				MESSAGE = "Поле с файлом пустое. Операция прервана";
			}

		}
		
		if (Trim(action) == "add_new_file")
		{
			_courseDoc=OpenDoc(UrlFromDocID(OptInt(p_attached_course_id)));
			_courseDocTE = _courseDoc.TopElem;
			try
			{
	
				if (Trim(String(p_attachment_resource_type)) == "resource")
				{
					if (p_attachment_resource_id != '' && p_attachment_resource_id != undefined && p_attachment_resource_id != null)
					{

						try
						{
							
							_fileDoc = OpenDoc(UrlFromDocID(OptInt(p_attachment_resource_id)));
							_fileDoc.TopElem.custom_elems.ObtainChildByKey('doc_type').value = Trim(String(p_attached_resource_file_doc_type));
							_fileDoc.Save();
							if (Trim(String(p_attached_resource_file_doc_type)) == "Дистанционный курс")
							{
								_fileDoc.TopElem.custom_elems.ObtainChildByKey('course_import_type').value = Trim(String(p_attachment_import_type));
								
							}
							curDoc = OpenDoc(UrlFromDocID(OptInt(cp_id)));
							curDocTE = curDoc.TopElem;
							if (Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != "" && OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != undefined)
							{
								org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value+"' and $elem/catalog_name='resource' return $elem"));

								if (org_category == undefined)
								{
									_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
									_new_org_category.BindToDb( DefaultDb );
									_new_org_category.Save();
									_new_org_category.TopElem.code = "org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value;
									_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+"return $elem")).name;
									_new_org_category.TopElem.catalog_name = "resource";
									_new_org_category.Save();
									org_category = _new_org_category;
									
									for (role in _fileDoc.TopElem.role_id)
									{
										role.Delete();
									}
									_fileDoc.TopElem.role_id.ObtainByValue(OptInt(org_category.DocID));
									
								}
								else
								{
									_role_exists = false;
									for (role in _fileDoc.TopElem.role_id)
									{
										if (OptInt(role) == OptInt(org_category.id))
										{
											_role_exists = true;
										}
									}
									if (_role_exists == false)
									{
										_fileDoc.TopElem.role_id.ObtainByValue(OptInt(org_category.id));
									}
								}
	
							}
							_fileDoc.Save();
							_new_part = _courseDocTE.parts.AddChild();
							_new_part.type = Trim(String(p_attachment_resource_type));
							_new_part.code = "PART_" + OptInt(ArrayCount(_courseDocTE.parts)) +1;
							_new_part.name = Trim(String(p_attachment_resource_name));
							_new_part.desc = Trim(String(p_attachment_resource_desc));
							_new_part.object_id = OptInt(p_attachment_resource_id);
							try
							{
								_new_part.max_score = OptInt(Trim(p_attached_file_mark_type));
							}
							catch(err)
							{
								ERROR = 1;
								MESSAGE = "Операция прервана. Необходимо указать тип оценки в занятие";
							}
							_courseDoc.Save();
							 MESSAGE = "Файл успешно загружен";
						}
						catch(err)
						{
							ERROR=1;
							MESSAGE = "ERROR: " + err;
						}
					}
					else
					{
						ERROR = 1;
						MESSAGE = "Поле с файлом пустое. Операция прервана";
					}
				}
				
				if (Trim(String(p_attachment_resource_type)) == "inline")
				{					
					if (Trim(String(p_attachment_resource_url)) != '' && p_attachment_resource_url != undefined && p_attachment_resource_url != null)
					{
						try
						{
							_new_part = _courseDocTE.parts.AddChild();
							_new_part.type = Trim(String(p_attachment_resource_type));
							_new_part.code = "PART_" + OptInt(ArrayCount(_courseDocTE.parts)) +1;
							_new_part.name = Trim(String(p_attachment_resource_name));
							_new_part.desc = Trim(String(p_attachment_resource_desc));
							_new_part.url = "/view_doc.html?mode=poll&object_id="+Trim(p_attachment_resource_url);
							_courseDoc.Save();
							MESSAGE = 'Опрос <span style="color:green">успешно</span> прикреплен';
						}
						catch(err)
						{
							ERROR = 1;
							MESSAGE = "Ошибка: "+err;
						}
						
					}
					else
					{
						ERROR = 1;
						MESSAGE = "Ошибка: Опрос не выбран";
					}

						 
					
				}
				if (Trim(String(p_attachment_resource_type)) == "library_material")
				{					
					if (p_attachment_resource_id_type1 != '' && p_attachment_resource_id_type1 != undefined && p_attachment_resource_id_type1 != null)
					{
						//_file = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id="+ OptInt(p_attachment_resource_id_type1) +" return $elem"));
						
						if (p_attachment_resource_name_type1 != '' && p_attachment_resource_name_type1 != undefined && p_attachment_resource_name_type1 != null)
						{
							_new_lib_materialDoc = OpenNewDoc( 'x-local://wtv/wtv_library_material.xmd' );
							_new_lib_materialDoc.BindToDb( DefaultDb );
							_new_lib_materialDoc.Save();
							_new_lib_materialDocTE = _new_lib_materialDoc.TopElem;
							_new_lib_materialDocTE.name = Trim(String(p_attachment_resource_name_type1));
							_new_lib_materialDocTE.code = "lib_m_"+_new_lib_materialDoc.DocID;
							if (p_attachment_library_section_id != '' && p_attachment_library_section_id != undefined && p_attachment_library_section_id != null)
							{
								_new_lib_materialDocTE.section_id = OptInt(p_attachment_library_section_id);
							}
							if (p_attachment_library_system_id != '' && p_attachment_library_system_id != undefined && p_attachment_library_system_id != null)
							{
								_new_lib_materialDocTE.library_system_id = OptInt(p_attachment_library_system_id);
							}
							_new_lib_materialDocTE.file_name = OptInt(p_attachment_resource_id_type1);
							_new_lib_materialDoc.Save();
							_new_part = _courseDocTE.parts.AddChild();
							_new_part.type = Trim(String(p_attachment_resource_type));
							_new_part.code = "PART_" + OptInt(ArrayCount(_courseDocTE.parts)) +1;
							_new_part.name = Trim(String(p_attachment_resource_name_type1));
							_new_part.desc = Trim(String(p_attachment_resource_desc_type1));
							_new_part.object_id = OptInt(_new_lib_materialDoc.DocID);
							_courseDoc.Save();
							
						}

						 MESSAGE = 'Файл <span style="color:green">успешно</span> загружен';
					}
					else
					{
						ERROR = 1;
						MESSAGE = "При загрузке файла возникла ошибка. Операция прервана";
					}
				}
				if (Trim(String(p_attachment_resource_type)) == "test")
				{					
					if (p_attachment_resource_url != '' && OptInt(p_attachment_resource_url) != undefined && p_attachment_resource_url != null)
					{
						_assessment = ArrayOptFirstElem(XQuery("for $elem in assessments where $elem/id="+ OptInt(p_attachment_resource_url) +" return $elem"));
						
						if ( _assessment != undefined)
						{	
							_assessmentDocTE = OpenDoc(UrlFromDocID(OptInt(_assessment.id))).TopElem;
							_new_part = _courseDocTE.parts.AddChild();
							_new_part.type = Trim(String(p_attachment_resource_type));
							_new_part.code = "PART_" + OptInt(ArrayCount(_courseDocTE.parts)) +1;
							_new_part.name = Trim(String(_assessment.title));
							_new_part.desc = Trim(String(p_attachment_resource_desc));
							_new_part.assessment_id = OptInt(_assessment.id);
							_new_part.url = _assessmentDocTE.publish_url;
							try
							{
								_new_part.max_score = OptInt(Trim(p_attached_file_mark_type));
							}
							catch(err)
							{
								ERROR = 1;
								MESSAGE = "Операция прервана. Необходимо указать тип оценки в занятие";
							}
							_courseDoc.Save();
							
						}

						 MESSAGE = 'Тест <span style="color:green">успешно</span> загружен в модуль';
					}
					else
					{
						ERROR = 1;
						MESSAGE = "При загрузке файла возникла ошибка. Операция прервана";
					}
				}
				
				
				if (Trim(String(p_attachment_resource_type)) == "lesson")
				{					
					if (p_attachment_resource_url != '' && OptInt(p_attachment_resource_url) != undefined && p_attachment_resource_url != null)
					{
						/*_file = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id="+ OptInt(p_attachment_resource_id_type1) +" return $elem"));
						_fileDoc = OpenDoc(UrlFromDocID(OptInt(p_attachment_resource_id_type1)));
						_url = _fileDoc.TopElem.file_url;
						_url_to_extract = "x-local://wt/web/webtutor/"
						try
						{
							tools.zip_extract(_url, _url_to_extract );
							ERROR = 0;
							_url = "/"+Trim(String(p_attachment_resource_url));
						
						}
						catch(err)
						{
							ERROR=1;
							MESSAGE = "ERROR: " + err;
						}*/
						
						_course_query = ArrayOptFirstElem(XQuery("for $elem in courses where $elem/id="+OptInt(p_attachment_resource_url)+" return $elem"));

						if (_course_query != undefined)
						{
							_attached_courseDocTE = OpenDoc(UrlFromDocID(OptInt(_course_query.id))).TopElem;
							_new_lessonDoc = OpenNewDoc( 'x-local://wtv/wtv_course_module.xmd' );
							_new_lessonDoc.BindToDb( DefaultDb );
							_new_lessonDoc.Save();
							_new_lessonDocTE = _new_lessonDoc.TopElem;
							_new_lessonDocTE.name = Trim(String(_course_query.name));
							_new_lessonDocTE.code = "lsn_"+_new_lessonDoc.DocID;
							_new_lessonDocTE.url = _course_query.base_url;
							//_new_lessonDocTE.eid = Trim(String(p_attachment_external_id));
							_new_lessonDocTE.import_type = Trim(String(_attached_courseDocTE.import_type));
							//_new_lessonDocTE.mastery_score_relative = OptInt(p_attachment_score_relative);
							//_new_lessonDocTE.mastery_score = OptInt(p_attachment_mastery_score);
							_new_lessonDoc.Save();

							_new_part = _courseDocTE.parts.AddChild();
							_new_part.type = Trim(String(p_attachment_resource_type));
							_new_part.code = "PART_" + OptInt(ArrayCount(_courseDocTE.parts)) +1;
							_new_part.name = Trim(String(_course_query.name));
							_new_part.desc = Trim(String(p_attachment_resource_desc));
							_new_part.course_module_id = OptInt(_new_lessonDoc.DocID);
							_new_part.url = _new_lessonDocTE.url;
							//_new_part.mastery_score = OptInt(_new_lessonDocTE.mastery_score);
							try
							{
								_new_part.max_score = OptInt(Trim(p_attached_file_mark_type));
							}
							catch(err)
							{
								ERROR = 1;
								MESSAGE = "Операция прервана. Необходимо указать тип оценки в занятие";
							}
							_courseDoc.Save();
							MESSAGE = 'Курс <span style="color:green">успешно</span> прикреплен';
						}
						
					}
					else
					{
						ERROR = 1;
						MESSAGE = "При добваление курса возникла ошибка. Операция прервана";
					}
				}

				if (Trim(String(p_attachment_resource_type)) == "learning_task")
				{					
					if (p_attachment_resource_name_type1 != '' && p_attachment_resource_name_type1 != undefined && p_attachment_resource_name_type1 != null)
					{

							_new_learning_taskDoc = OpenNewDoc( 'x-local://wtv/wtv_learning_task.xmd' );
							_new_learning_taskDoc.BindToDb( DefaultDb );
							_new_learning_taskDoc.Save();
							_new_learning_taskDocTE = _new_learning_taskDoc.TopElem;
							_new_learning_taskDocTE.name = Trim(String(p_attachment_resource_name_type1));
							_new_learning_taskDocTE.code = "lt_"+_new_learning_taskDoc.DocID;
							_new_learning_taskDoc.Save();

							_new_part = _courseDocTE.parts.AddChild();
							_new_part.type = Trim(String(p_attachment_resource_type));
							_new_part.code = "PART_" + OptInt(ArrayCount(_courseDocTE.parts)) +1;
							_new_part.name = Trim(String(p_attachment_resource_name_type1));
							_new_part.desc = Trim(String(p_attachment_resource_desc_type1));
							_new_part.object_id = OptInt(_new_learning_taskDoc.DocID);
							_courseDoc.Save();
							

						 MESSAGE = "Задание успешно создано";
					}
					else
					{
						ERROR = 1;
						MESSAGE = "При загрузке файла возникла ошибка. Операция прервана";
					}
				}
			}
			catch(err)
			{
				ERROR=1;
				MESSAGE= "ERROR: " + err;
			}
		}
		if (Trim(action) == "delete_attached_file")
		{
			try
			{
				_courseDoc=OpenDoc(UrlFromDocID(OptInt(p_attached_course_id)));
				_courseDocTE = _courseDoc.TopElem;
				_files_to_delete_arr = [];
				_files_to_delete_names_list = "";
				for (part in _courseDocTE.parts)
				{
					if (String(part.code) == Trim(String(file_to_delete_part_code))) 
					{
						_files_to_delete_names_list = _files_to_delete_names_list + part.code + ",";
						_files_to_delete_arr.push(part);
					}
				}
				for (file in _files_to_delete_arr)
				{
					file.Delete();
				}
				_courseDoc.Save();
				MESSAGE = "Файлы успешно удалены из модуля";
			}
			catch(err)
			{
				ERROR = 1;
				MESSAGE = "Error: " + err;
			}
		}
}
else
{

	if (ep_id != undefined && ep_id != '')
	{
		try
		{
			curDoc = OpenDoc(UrlFromDocID(OptInt(cp_id)));
			curDocTE = curDoc.TopElem;
			curEPDoc =  OpenDoc(UrlFromDocID(OptInt(ep_id)));
			curEPDocTE = curEPDoc.TopElem;

			
			if (Trim(action)  == "create_new_course")
			{

				try
				{
					_education_methodDoc = OpenDoc(UrlFromDocID(OptInt(p_id)));
					_education_methodDocTE = _education_methodDoc.TopElem;
					_new_course_for_emDoc = OpenNewDoc( 'x-local://wtv/wtv_course.xmd' );
					_new_course_for_emDoc.BindToDb( DefaultDb );
					_new_course_for_emDoc.Save();
					_new_course_for_emDocTE = _new_course_for_emDoc.TopElem;
					_new_course_for_emDocTE.name = "Курс для модуля: " + _education_methodDocTE.name;
					_new_course_for_emDocTE.code =  "cr_"+ _education_methodDoc.DocID;
					_new_course_for_emDocTE.status = "secret";
					_new_course_for_emDoc.Save();
					_education_methodDocTE.course_id = _new_course_for_emDoc.DocID;
					_education_methodDoc.Save();
					RESULT.em_id = p_id;
					RESULT.new_course_id = _new_course_for_emDoc.DocID;

					if (Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != "" && OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != undefined && curDocTE.custom_elems.ObtainChildByKey('educ_org').value != null)
					{
						org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value+"' and $elem/catalog_name='course' return $elem"));
		
						if (org_category == undefined)
						{
							_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
							_new_org_category.BindToDb( DefaultDb );
							_new_org_category.Save();
							_new_org_category.TopElem.code = "org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value;
							_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+"return $elem")).name;
							_new_org_category.TopElem.catalog_name = "course";
							_new_org_category.Save();
							org_category = _new_org_category;
							
							for (role in _new_course_for_emDocTE.role_id)
							{
								role.Delete();
							}
							_new_course_for_emDocTE.role_id.ObtainByValue(OptInt(org_category.DocID));
							
						}
						else
						{
							_role_exists = false;
							for (role in _new_course_for_emDocTE.role_id)
							{
								if (OptInt(role) == OptInt(org_category.id))
								{
									_role_exists = true;
								}
							}
							if (_role_exists == false)
							{
								_new_course_for_emDocTE.role_id.ObtainByValue(OptInt(org_category.id));
							}
						}
						_new_course_for_emDoc.Save();
					}
					
				}
				catch(err)
				{
					ERROR = 1;
					MESSAGE = "Error: " + err + "p_id: " + p_id;
				}
				

			}
			if (Trim(action)  == "attach_new_files")
			{
				
				if (p_attachment_resource_id_type1 != '' && p_attachment_resource_id_type1 != undefined) 
				{
					_file = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id="+ OptInt(p_attachment_resource_id_type1) +" return $elem"));
					_new_part = _new_course_for_emDocTE.parts.AddChild();
					_new_part.code = "PART_1";
					_new_part.type = "resource";
					_new_part.name = _file.name;
					_new_part.url = "/webtutor/cp_course/"+_new_part.code+".html"
					_new_part.desc = String(p_attachment_resource_desc_type1);
					_new_part.disp_scrolling = 0;
					_new_part.resizable = 0;
					_new_part.is_mandatory = 0;
					_new_part.is_visible = 0;
					_new_part.set_status_side = "course";
					_new_part.score_factor = 1;
					_new_part.attempts_num = 1;
					_new_part.object_id = _file.id;
					
					if (curDocTE.custom_elems.ObtainChildByKey('educ_org').value != "" && curDocTE.custom_elems.ObtainChildByKey('educ_org').value != undefined)
					{
						org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value+"' and $elem/catalog_name='resource' return $elem"));
						_resourceDoc = OpenDoc(UrlFromDocID(OptInt(p_attachment_resource_id_type1)));
						_resourceDocTE = _resourceDoc.TopElem;
						if (org_category == undefined)
						{
							_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
							_new_org_category.BindToDb( DefaultDb );
							_new_org_category.Save();
							_new_org_category.TopElem.code = "org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value;
							_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+"return $elem")).name;
							_new_org_category.TopElem.catalog_name = "resource";
							_new_org_category.Save();
							org_category = _new_org_category;
							
							for (role in _resourceDocTE.role_id)
							{
								role.Delete();
							}
							_resourceDocTE.role_id.ObtainByValue(OptInt(org_category.DocID));
							
						}
						else
						{
							_role_exists = false;
							for (role in _resourceDocTE.role_id)
							{
								if (OptInt(role) == OptInt(org_category.id))
								{
									_role_exists = true;
								}
							}
							if (_role_exists == false)
							{
								_resourceDocTE.role_id.ObtainByValue(OptInt(org_category.id));
							}
						}
						_resourceDoc.Save();
					}
					_new_course_for_emDoc.Save();
				}

			}
			if (Trim(action)  == "action_edit_program_mark_type")
			{
				try
				{
					
					_education_methodDoc = OpenDoc(UrlFromDocID(OptInt(p_id)));
					_education_methodTE = _education_methodDoc.TopElem;
					_education_methodTE.custom_elems.ObtainChildByKey('mark_type').value = Trim(p_mark_type);
					_education_methodDoc.Save();
					if (OptInt(p_attached_course_id) != undefined && OptInt(p_attached_course_id) != null && OptInt(p_attached_course_id) != "")
					{
						courseDoc = OpenDoc(UrlFromDocID(OptInt(p_attached_course_id)));
						courseDocTE = courseDoc.TopElem;
						for (part in courseDocTE.parts)
						{
							
							
							if (part.type == "resource")
							{

								if (Trim(p_mark_type) == "mixed")
								{
									part.max_score = 1;
									courseDoc.Save();
								}
								if (Trim(p_mark_type) == "simple")
								{
									part.max_score = 1;
									courseDoc.Save();
								}
								if (Trim(p_mark_type) == "score")
								{
									part.max_score = 5;
									courseDoc.Save();
								}
								
							}
						}
					}
					
					MESSAGE = "Изменения модуля успешно сохранены";
				}
				catch(err)
				{		
					MESSAGE = "Ошибка: " + err;
				}

			}
			if (Trim(action)  == "save_program_changes")
			{

				try
				{
					_education_methodDoc = OpenDoc(UrlFromDocID(OptInt(p_id)));
					_education_methodTE = _education_methodDoc.TopElem;
					_education_methodTE.name = p_name_new;
					_education_methodTE.type = 'course';
					_education_methodTE.cost_type = p_cost_type_new;
					_education_methodTE.cost = p_cost_new;
					_education_methodTE.duration = p_duration_new;
					_education_methodTE.person_num = p_person_num_new;
					_education_methodTE.custom_elems.ObtainChildByKey('mark_type').value = Trim(p_mark_type);
				
					if (Trim(curDocTE.custom_elems.ObtainChildByKey('event_form').value) == "Очная")
					{
						_education_methodTE.desc = Trim(p_fulltime_desc);
					}
					lectors_arr = "";
					if (p_lectors_new != '' && p_lectors_new != null && p_lectors_new != undefined)
					{
						try
						{
							lectors_arr = p_lectors_new.split(";");
						}
						catch(err)
						{

						}
					}
					lecotrs_to_delete_arr = [];

					for (lector in _education_methodTE.lectors)
					{
						lecotrs_to_delete_arr.push(lector);
						
					}	
					for (lector in lecotrs_to_delete_arr)
					{
						lector.Delete();
					}
					if (lectors_arr != "")
					{
						for (lector in lectors_arr)
						{
							if (Trim(lector) != "")
							{
								_lector = _education_methodTE.lectors.AddChild();
								_lector.lector_id = OptInt(Trim(lector));
								
							}
						}
					}
					if (Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != "" && OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != undefined && curDocTE.custom_elems.ObtainChildByKey('educ_org').value != null)
					{
						org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value+"' and $elem/catalog_name='education_method' return $elem"));

						if (org_category == undefined)
						{
							_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
							_new_org_category.BindToDb( DefaultDb );
							_new_org_category.Save();
							_new_org_category.TopElem.code = "org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value;
							_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+"return $elem")).name;
							_new_org_category.TopElem.catalog_name = "education_method";
							_new_org_category.Save();
							org_category = _new_org_category;
							
							for (role in _education_methodTE.role_id)
							{
								role.Delete();
							}
							_education_methodTE.role_id.ObtainByValue(OptInt(org_category.DocID));
							
						}
						else
						{
							_role_exists = false;
							for (role in _education_methodTE.role_id)
							{
								if (OptInt(role) == OptInt(org_category.id))
								{
									_role_exists = true;
								}
							}
							if (_role_exists == false)
							{
								_education_methodTE.role_id.ObtainByValue(OptInt(org_category.id));
							}
						}
					}
					if (Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != "" && OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != undefined && curDocTE.custom_elems.ObtainChildByKey('educ_org').value != null)
					{
						org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value+"' and $elem/catalog_name='education_method' return $elem"));
		
						if (org_category == undefined)
						{
							_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
							_new_org_category.BindToDb( DefaultDb );
							_new_org_category.Save();
							_new_org_category.TopElem.code = "org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value;
							_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+" return $elem")).name;
							_new_org_category.TopElem.catalog_name = "education_method";
							_new_org_category.Save();
							org_category = _new_org_category;
							
							for (role in _education_methodTE.role_id)
							{
								role.Delete();
							}
							_education_methodTE.role_id.ObtainByValue(OptInt(org_category.DocID));
							
						}
						else
						{
							_role_exists = false;
							for (role in _education_methodTE.role_id)
							{
								if (OptInt(role) == OptInt(org_category.id))
								{
									_role_exists = true;
								}
							}
							if (_role_exists == false)
							{
								_education_methodTE.role_id.ObtainByValue(OptInt(org_category.id));
							}
						}

					}
					_education_methodDoc.Save();
					for (program in curDocTE.programs)
					{
						if (OptInt(program.id) == OptInt(_education_methodDoc.DocID))
						{

							new_program_in_cp = program;
							new_program_in_cp.name = p_name_new;
							new_program_in_cp.education_method_id=_education_methodDoc.DocID;
							new_program_in_cp.cost = p_cost_new;
							new_program_in_cp.cost_type = p_cost_type_new;
							new_program_in_cp.duration = p_duration_new;
							new_program_in_cp.person_num = p_person_num_new;
							new_program_in_cp.type = 'education_method';
							new_program_in_cp.object_id = _education_methodDoc.DocID;
							new_program_in_cp.required = 1;
						}
					}
					
					MESSAGE = "Изменения модуля успешно сохранены";

					
				}
				catch(err)
				{	
					ERROR = 1;
					MESSAGE = "Ошибка: " + err;
				}
				
				
				

			}

			if (Trim(action)  == "add_program")
			{

				try
				{				
					_education_methodDoc =  OpenDoc(UrlFromDocID(OptInt(p_id)));
					_education_methodTE =  _education_methodDoc.TopElem;
					if (_education_methodTE.course_id == "" || _education_methodTE.course_id == undefined || _education_methodTE.course_id == null )
					{
						_new_course_for_emDoc = OpenNewDoc( 'x-local://wtv/wtv_course.xmd' );
						_new_course_for_emDoc.BindToDb( DefaultDb );
						_new_course_for_emDoc.Save();
						_new_course_for_emDocTE = _new_course_for_emDoc.TopElem;
						_new_course_for_emDocTE.name = "Курс для модуля: " + _education_methodTE.name;
						_new_course_for_emDocTE.code =  "cr_"+ _new_course_for_emDoc.DocID;
						_new_course_for_emDocTE.status = "secret";
						_new_course_for_emDoc.Save();
						_education_methodTE.course_id = _new_course_for_emDoc.DocID;
						_education_methodDoc.Save();

					}
					if (String(Trim(_education_methodTE.custom_elems.ObtainChildByKey('mark_type').value)) == "" || String(Trim(_education_methodTE.custom_elems.ObtainChildByKey('mark_type').value)) == undefined || String(Trim(_education_methodTE.custom_elems.ObtainChildByKey('mark_type').value)) == null)
					{
						_education_methodTE.custom_elems.ObtainChildByKey('mark_type').value = "simple";
					}
					_education_methodDoc.Save();
					new_program_in_ep = curEPDocTE.education_methods.AddChild();
					new_program_in_ep.education_method_id = _education_methodDoc.DocID;
					curDoc.Save();
					MESSAGE = "Занятия успешно добавлены к модулю";
				}
				catch(err)
				{		
					MESSAGE = "Ошибка: " + err;
				}
				
				
				

			}



			if (Trim(action)  == "create_new_program")
			{

				try
				{
					
					_new_education_methodDoc =  OpenNewDoc( 'x-local://wtv/wtv_education_method.xmd' );
					_new_education_methodDoc.BindToDb( DefaultDb );
					_new_education_methodDoc.Save();
					_new_education_methodTE = _new_education_methodDoc.TopElem;
					_new_education_methodTE.name = p_name_new;
					_new_education_methodTE.type = 'course';
					_new_education_methodTE.code = "em_"+ _new_education_methodDoc.DocID;
					_new_education_methodTE.currency = "RUR";
					_new_education_methodTE.cost_type = p_cost_type_new;
					_new_education_methodTE.cost = p_cost_new;
					_new_education_methodTE.duration = p_duration_new;
					_new_education_methodTE.person_num = p_person_num_new;
					_new_education_methodTE.custom_elems.ObtainChildByKey('mark_type').value = "simple";
					if (Trim(curDocTE.custom_elems.ObtainChildByKey('event_form').value) == "Очная")
					{
						_new_education_methodTE.desc = Trim(p_fulltime_desc);
					}
					if (p_lectors_new != '' && p_lectors_new != null && p_lectors_new != undefined)
					{
						try
						{
							lectors_arr = p_lectors_new.split(",");
							
							for (lector in lectors_arr)
							{
								//lector_fullname = ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/id = "+OptInt(lector)+" return $elem")).fullname;
								_lector = _new_education_methodTE.lectors.AddChild();
								_lector.lector_id = OptInt(lector);
							}
						}
						catch(err)
						{

						}
					}
					if (Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != "" && OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != undefined && curDocTE.custom_elems.ObtainChildByKey('educ_org').value != null)
					{
						org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value+"' and $elem/catalog_name='education_method' return $elem"));
		
						if (org_category == undefined)
						{
							_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
							_new_org_category.BindToDb( DefaultDb );
							_new_org_category.Save();
							_new_org_category.TopElem.code = "org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value;
							_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+" return $elem")).name;
							_new_org_category.TopElem.catalog_name = "education_method";
							_new_org_category.Save();
							org_category = _new_org_category;
							
							for (role in _new_education_methodTE.role_id)
							{
								role.Delete();
							}
							_new_education_methodTE.role_id.ObtainByValue(OptInt(org_category.DocID));
							
						}
						else
						{
							_role_exists = false;
							for (role in _new_education_methodTE.role_id)
							{
								if (OptInt(role) == OptInt(org_category.id))
								{
									_role_exists = true;
								}
							}
							if (_role_exists == false)
							{
								_new_education_methodTE.role_id.ObtainByValue(OptInt(org_category.id));
							}
						}

					}
					_new_education_methodDoc.Save();
					if (_new_education_methodTE.course_id == "" || _new_education_methodTE.course_id == undefined || _new_education_methodTE.course_id == null )
					{
						_new_course_for_emDoc = OpenNewDoc( 'x-local://wtv/wtv_course.xmd' );
						_new_course_for_emDoc.BindToDb( DefaultDb );
						_new_course_for_emDoc.Save();
						_new_course_for_emDocTE = _new_course_for_emDoc.TopElem;
						_new_course_for_emDocTE.name = "Курс для модуля: " + _new_education_methodTE.name;
						_new_course_for_emDocTE.code =  "cr_"+ _new_course_for_emDoc.DocID;
						_new_course_for_emDocTE.status = "secret";
						_new_course_for_emDoc.Save();
						_new_education_methodTE.course_id = _new_course_for_emDoc.DocID;
						_new_education_methodDoc.Save();
					}
					if (Trim(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != "" && OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value) != undefined && curDocTE.custom_elems.ObtainChildByKey('educ_org').value != null)
					{
						org_category = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code='org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value+"' and $elem/catalog_name='course' return $elem"));
		
						if (org_category == undefined)
						{
							_new_org_category =  OpenNewDoc( 'x-local://qti/qti_role.xmd' );
							_new_org_category.BindToDb( DefaultDb );
							_new_org_category.Save();
							_new_org_category.TopElem.code = "org_"+curDocTE.custom_elems.ObtainChildByKey('educ_org').value;
							_new_org_category.TopElem.name = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id="+OptInt(curDocTE.custom_elems.ObtainChildByKey('educ_org').value)+"return $elem")).name;
							_new_org_category.TopElem.catalog_name = "course";
							_new_org_category.Save();
							org_category = _new_org_category;
							
							for (role in _new_course_for_emDocTE.role_id)
							{
								role.Delete();
							}
							_new_course_for_emDocTE.role_id.ObtainByValue(OptInt(org_category.DocID));
							
						}
						else
						{
							_role_exists = false;
							for (role in _new_course_for_emDocTE.role_id)
							{
								if (OptInt(role) == OptInt(org_category.id))
								{
									_role_exists = true;
								}
							}
							if (_role_exists == false)
							{
								_new_course_for_emDocTE.role_id.ObtainByValue(OptInt(org_category.id));
							}
						}
						_new_course_for_emDoc.Save();
					}
					new_program_in_ep = curEPDocTE.education_methods.AddChild();
					new_program_in_ep.education_method_id = _new_education_methodDoc.DocID;
					curEPDoc.Save();
					for (program in curDocTE.programs)
					{
						if (OptInt(program.object_id) == OptInt(curEPDoc.DocID))
						{
							try
							{
								OptInt(program.cost) += OptInt(_new_education_methodTE.cost);
								OptInt(program.duration) += OptInt(_new_education_methodTE.duration);
								OptInt(program.person_num) += OptInt(_new_education_methodTE.person_num);
							}
							catch(err)
							{
								
							}
						}
					}
					curDoc.Save();
					MESSAGE = "Новое занятие успешно создано";
				}
				catch(err)
				{		
					ERROR = 1;
					MESSAGE = "Ошибка: " + err;
				}
				
				
				

			}


			if (Trim(action) == "delete_p")
			{

				for (program in curEPDocTE.education_methods)
				{
					if (OptInt(program.education_method_id) == OptInt(cp_program_to_delete_id))
					{
						try
						{
							MESSAGE = "Занятие успешно удалено из модуля";
							program.Delete();
						}
						catch(err)
						{
							MESSAGE = "Ошибка: " + err;
						}

					}
				}
				
				
			}

			
			//собираем все цены учебных программ в наборе программ,а также количество участников в общую сумму и записываем эти данные в модульную программу
		
			/*dollar_course = 1;
			euro_course = 1;
			eng_fund_sterling_course = 1;
			grivna_course = 1;				
			for (program in curDocTE.programs)
			{
				duration = 0;
				person_num = 0;
				cost = 0;
				currency = 'RUR';
				education_methods_arr = XQuery("for $elem in education_program_education_methods where $elem/education_program_id = " + OptInt(program.object_id) + " return $elem");
				for (ep in education_methods_arr)
				{
					if (ep.cost_type == 'group')
					{
						if (ep.currency == '')
						{
							cost =  OptInt(cost) + OptInt(ep.cost);
						}
						if (ep.currency == 'RUR')
						{
							cost =  OptInt(cost) + OptInt(ep.cost);
						}
						if (ep.currency == 'USD')
						{
							cost = OptInt(cost) + OptInt(ep.cost) * dollar_course;
						}
						if (ep.currency == 'EUR')
						{
							cost = OptInt(cost) + OptInt(ep.cost) * euro_course;
						}
						if (ep.currency == 'GBR')
						{
							cost = OptInt(cost) + OptInt(ep.cost) * eng_fund_sterling_course;
						}
						if (ep.currency == 'UAH')
						{
							cost = OptInt(cost) + OptInt(ep.cost) * grivna_course;
						}
					}
					else if (ep.cost_type == 'person')
					{
						if (ep.currency == '')
						{
							cost =  OptInt(cost) + (OptInt(ep.cost) * OptInt(ep.person_num)) ;
						}			
						if (ep.currency == 'RUR')
						{
							cost =  OptInt(cost) + (OptInt(ep.cost) * OptInt(ep.person_num)) ;
						}
						if (ep.currency == 'USD')
						{
							cost = OptInt(cost) + ((OptInt(ep.cost) *  dollar_course )* OptInt(ep.person_num));
						}
						if (ep.currency == 'EUR')
						{
							cost = OptInt(cost) + ((OptInt(ep.cost) *  euro_course )* OptInt(ep.person_num));
						}
						if (ep.currency == 'GBR')
						{
							cost = OptInt(cost) + ((OptInt(ep.cost) *  eng_fund_sterling_course )* OptInt(ep.person_num));
						}
						if (ep.currency == 'UAH')
						{
							cost = OptInt(cost) + ((OptInt(ep.cost) *  grivna_course )* OptInt(ep.person_num));
						}
					}

					try {duration = OptInt(duration) + OptInt(ep.duration)}catch(err){}
					try {person_num = OptInt(person_num) + OptInt(ep.person_num)}catch(err){}
				
				}
				program.duration = duration;
				program.person_num = person_num;
				program.cost = cost;
			}*/
			
			curDoc.Save();

			try
			{
				curEPDoc.Save();
				if (Trim(action) == "edit_cp")
				{
					MESSAGE = "Изменения успешно сохранены"
				}
			}
			catch(err)
			{		
				MESSAGE = "Ошибка: " + err;
			}
			

		}
		catch(err)
		{
			ERROR = 1;
			MESSAGE = "ERROR"+err;
		}
		

	}
	else
	{

		ERROR = 1;
		MESSAGE = "ERROR: module_program_id is null"  + cp_id;
		
	}
}


