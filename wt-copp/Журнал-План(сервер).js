<%

var request_type =  Request.Method;
var response = {};


/*### Функции - начало ###*/

function getProgramtype(compoundProgramId) {	//Получить тип программы (true - очная, false - заочная)
	var result = true;
	var compoundProgramDoc = OpenDoc(UrlFromDocID(Int(compoundProgramId))).TopElem;
	var event_form = String(compoundProgramDoc.custom_elems.ObtainChildByKey('event_form').value);
	result = (event_form=='Очная'? true: false);
	return result;
}

function getLink (moduleObjectID, eventID) {
    var refLink = '';
    if (!moduleObjectID) return refLink;
    var sid = tools.get_sum_sid( moduleObjectID );
    var moduleDoc = OpenDoc(UrlFromDocID(Int(moduleObjectID))).TopElem;
    var moduleDocName = moduleDoc.Name;

if (moduleDocName == "course") {

    findCourse =  ArrayOptFirstElem(XQuery("for $elem in learnings where $elem/course_id = "+moduleObjectID+" and $elem/person_id = "+curUserID+" order by $elem/start_usage_date descending return $elem"));
    if(findCourse != undefined) {
        refLink = "/view_doc.html?mode=learning_proc&doc_id=&object_id="+findCourse.id+"&course_id="+moduleObjectID+"&sid="+sid;
    } else {
        findCourse = ArrayOptFirstElem(XQuery("for $elem in active_learnings where $elem/course_id = "+moduleObjectID+" and $elem/person_id = "+curUserID+" order by $elem/start_usage_date descending return $elem"));
        if(findCourse != undefined) {
            refLink = "/course_launch.html?course_id="+moduleObjectID+"&object_id="+findCourse.id+"&event_id="+eventID+"&sid="+sid;
        }
        else {
            tools.activate_course_to_person(curUserID, moduleObjectID, eventID);
            findCourse = ArrayOptFirstElem(XQuery("for $elem in active_learnings where $elem/course_id = "+moduleObjectID+" and $elem/person_id = "+curUserID+" order by $elem/start_usage_date descending return $elem"));
            if(findCourse != undefined) {
                setCourseInfo(findCourse)
                refLink = "/course_launch.html?course_id="+moduleObjectID+"&object_id="+findCourse.id+"&sid="+sid;
            }
            
        }
    }
    return refLink;
}

else if (moduleDocName == "assessment") {

    findTest =  ArrayOptFirstElem(XQuery("for $elem in test_learnings where $elem/assessment_id = "+moduleObjectID+" and ($elem/person_id = "+curUserID+" and $elem/event_id = "+eventID+") order by $elem/start_usage_date descending return $elem"));
    if(findTest != undefined) {
        refLink = "/view_doc.html?mode=test_learning_stat&object_id="+findTest.id;
    } else {
        findTest = ArrayOptFirstElem(XQuery("for $elem in active_test_learnings where $elem/assessment_id = "+moduleObjectID+" and ($elem/person_id = "+curUserID+" and $elem/event_id = "+eventID+") order by $elem/start_usage_date descending return $elem"));
        if(findTest != undefined) {
            refLink = "/test_launch.html?assessment_id="+moduleObjectID+"&object_id="+findTest.id;
        }
        else {
            tools.activate_test_to_person(curUserID, moduleObjectID, eventID);
            findTest = ArrayOptFirstElem(XQuery("for $elem in active_test_learnings where $elem/assessment_id = "+moduleObjectID+" and $elem/person_id = "+curUserID+" order by $elem/start_usage_date descending return $elem")); //тут event еще не прописан
            if(findTest != undefined) {
                refLink = "/test_launch.html?assessment_id="+moduleObjectID+"&object_id="+findTest.id;
            }
        }
    }
}
else if (moduleDocName == "forum") {

    refLink = "/view_doc.html?mode=forum&object_id="+moduleObjectID; /////Ссылка на форум

}
else if (moduleDocName == "event") {

    if(String(moduleDoc.type_id) == 'webinar') {
        refLink = "webinar_view_TODO";
    } else {
        refLink = "/view_doc.html?mode=event&object_id="+moduleObjectID;
    }

} else if (moduleDocName == "resource") {
    refLink = "/view_play_resource.html?object_id="+moduleObjectID+"&sid="+sid;
}
return refLink;
}



function openPlan(event_id) {   // получить структуру плана обучения
    var result = {};
    var eventPlan =  ArrayOptFirstElem( XQuery("for $elem in education_plans where $elem/event_id = " + event_id + " and $elem/person_id = " + curUserID + " return $elem") );
    var tePlanDoc = tools.open_doc(eventPlan.id) != undefined ? tools.open_doc(eventPlan.id).TopElem : null
    if (tePlanDoc == null) { response.error = 'Не найден план обучения'; return }
    
    // Находим массив форм обучения занятий
    var knowledge_classifier_id = ArrayOptFirstElem( XQuery("for $elem in knowledge_classifiers where $elem/code = 'form_education' return $elem/id") )
    var form_education = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = " + knowledge_classifier_id + " return $elem")
    
    // Находим массив типов обучения занятий
    knowledge_classifier_id = ArrayOptFirstElem( XQuery("for $elem in knowledge_classifiers where $elem/code = 'course_type' return $elem/id") )
    var type_education = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = " + knowledge_classifier_id + " return $elem")

    var compoundProgramId = eventPlan.compound_program_id;
    var structure = []; //сюда положится структура плана

    _modules = ArraySelect(tePlanDoc.programs, "This.type == 'folder'" );
    m_count = -1;
    
    response.classes = []
    for (_module in _modules) {
        m_count ++
        obj_module = tools.open_doc(compoundProgramId).TopElem.programs.Child(m_count)
        teModule = tools.open_doc(obj_module.object_id) != undefined ? tools.open_doc(obj_module.object_id).TopElem : null
        res_module = {
            id: String(_module.id), 
            name: String(_module.name), 
            object_id: String(_module.object_id), 
            children: [], 
            state_id: 0, 
            duration: 0, 
            score: 0,
            desc: String(teModule != null ? teModule.desc : '')
        }

        _classes = ArraySelect( tePlanDoc.programs, "This.parent_progpam_id == " + _module.id );
        for (_class in _classes) {
            _classDocTE = tools.open_doc(_class.education_method_id) != undefined ? tools.open_doc(_class.education_method_id).TopElem : null
            
            res_module.duration += _classDocTE.duration;
            res_class = {
                id: String(_class.id), 
                name: String(_class.name), 
                object_id: String(_class.object_id), 
                mark: String(_class.mark), 
                parent_id: String(_class.parent_progpam_id), 
                children: [], 
                state_id: 0, 
                duration: String(_classDocTE.duration)
            }

            // Создаём два условия: 1) Полное - находим курсы с зашитыми мероприятием и группой 2) Если не находим по 1 условию, то ищем первый попавшийся с пустыми полями мероприятия и группы
            condition = "where $elem/person_id = " + eventPlan.person_id + " and $elem/course_id = " + _class.object_id
            condition_full = condition + " and $elem/event_id = " + event_id

            active_class = ArrayOptFirstElem( ArrayUnion(
                XQuery( "for $elem in active_learnings " + condition_full + " return $elem"),
                XQuery( "for $elem in learnings " + condition_full + " return $elem")
            ) )
            if (active_class == undefined) {
                active_class = ArrayOptFirstElem( ArrayUnion(
                    XQuery( "for $elem in active_learnings " + condition + " return $elem"),
                    XQuery( "for $elem in learnings " + condition + " return $elem")
                ) )
                if (active_class == undefined) continue
                coursePlanDoc = tools.open_doc(active_class.id)
                // зашиваем в курс мероприятие и группу
                coursePlanDoc.TopElem.event_id = event_id
                coursePlanDoc.TopElem.group_id = tePlanDoc.group_id
                coursePlanDoc.Save()
            }



            coursePlanDoc = tools.open_doc(active_class.id)
            coursePlanDocTE = coursePlanDoc != undefined ? coursePlanDoc.TopElem : null
            courseDocTE = tools.open_doc(active_class.course_id).TopElem
            form = ArrayOptFind( ArrayIntersect(form_education, courseDocTE.knowledge_parts, "id", "knowledge_part_id"), "name == 'Дистанционное'" )
            res_class.form_education = DataType(form) == "object" && form != null ? String(form.name) : 'Очное'
            type = ArrayOptFind( ArrayIntersect(type_education, courseDocTE.knowledge_parts, "id", "knowledge_part_id"), "name == 'Балл'" )
            res_class.type_education = DataType(type) == "object" && type != null ? String(type.name) : 'Зачет'
            
            for (part in coursePlanDocTE.parts) {
                course_part = tools.open_doc(active_class.course_id).TopElem.parts.ObtainChildByKey(part.code)
                res_elem = {
                    name: String(part.name),
                    code: String(part.code),
                    object_id: OptInt(part.object_id),
                    course_id: Int(active_class.course_id),
                    state_id: Int(part.GetOptProperty('state_id', 0)),
                    score: String(part.score),
                    desc: String(course_part.GetOptProperty('desc', '')),
                    type: String(part.type)
                }
                // Заполнение элеменов (3-й уровень вложенности)
                if (part.type == 'resource') {
                    teRes = tools.open_doc(part.object_id) != undefined ? tools.open_doc(part.object_id).TopElem : null
                    if (teRes == null) continue
                    // Проверяем хранится ли файл в файловой системе или в БД
                    file_source_id = ArrayOptFirstElem( XQuery( "for $elem in file_sources where $elem/code = 'file_system_upload' or $elem/code = 'file_system' return $elem/id" ) )
                    res_elem.object_name = String(teRes.name)
                    if (teRes.file_source == file_source_id) {
                        res_elem.link = StrReplace(teRes.file_path, 'x-local://wt/web/', '')
                    } else {
                        res_elem.link = "/view_play_resource.html?object_id="+active_class.id+"&info=1&sub_id="+part.code+"&sid="+tools.get_sum_sid( active_class.id )
                    }
                }
                if (part.type == 'inline') res_elem.link = String(course_part.url)
                if (part.type == 'learning_task') {
                    res_elem.files = getFiles(part.object_id)
                    res_elem.attempts = []
                    task_results = XQuery("for $elem in learning_task_results where $elem/learning_task_id = " + part.object_id + " and $elem/person_id = " + curUserID + " return $elem")
                    for (task in task_results) {
                        teTask = tools.open_doc(task.id).TopElem
                        res_elem.attempts.push({
                            mark: String( task.mark ? task.mark : '-' ),
                            start_date: String( task.start_date ),
                            finish_date: String( task.finish_date ? task.finish_date : '-' ),
                            comment: String( teTask.comment ? teTask.comment : '-' )
                        })
                    }
                }

                if (part.type == 'lesson') {
                    findCourse = undefined; // Возможно эта строка уже не нужна
                    res_elem.type_name = "Электронный курс"; // Возможно эта строка уже не нужна
                    course_moduleDocUrl =  String(OpenDoc(UrlFromDocID(OptInt(part.course_module_id))).TopElem.url);
                    assignedElCourse =  ArrayOptFirstElem(XQuery("for $elem in courses where $elem/base_url = "+XQueryLiteral(course_moduleDocUrl)+" return $elem"));
                    if (assignedElCourse != undefined) {
                        findCourse =  ArrayOptFirstElem(XQuery("for $elem in learnings where $elem/course_id = "+assignedElCourse.id+" and $elem/person_id = "+curUserID+" order by $elem/start_usage_date descending return $elem"));
                        if(findCourse != undefined) {
                            res_elem.link = "/view_doc.html?mode=learning_proc&doc_id=&object_id="+findCourse.id+"&course_id="+assignedElCourse.id+"&sid="+tools.get_sum_sid( assignedElCourse.id );
                        } else {
                            findCourse = ArrayOptFirstElem(XQuery("for $elem in active_learnings where $elem/course_id = "+assignedElCourse.id+" and $elem/person_id = "+curUserID+" order by $elem/start_usage_date descending return $elem"));
                            if(findCourse != undefined) {
                                res_elem.link = "/course_launch.html?course_id="+assignedElCourse.id+"&object_id="+findCourse.id+"&event_id="+event_id+"&sid="+tools.get_sum_sid(assignedElCourse.id );
                            }
                            else {
                                tools.activate_course_to_person(curUserID, assignedElCourse.id, event_id);
                                findCourse = ArrayOptFirstElem(XQuery("for $elem in active_learnings where $elem/course_id = "+assignedElCourse.id+" and $elem/person_id = "+curUserID+" order by $elem/start_usage_date descending return $elem"));
                                if(findCourse != undefined) {
                                    setCourseInfo(findCourse)
                                    res_elem.link = "/course_launch.html?course_id="+assignedElCourse.id+"&object_id="+findCourse.id+"&sid="+tools.get_sum_sid(assignedElCourse.id );
                                }
                                
                            }
                        }
                    } else {
                            res_elem.link = "/course_launch.html?course_id="+_class.object_id+"&object_id="+active_class.id+"&event_id="+event_id+"&sid="+tools.get_sum_sid( _class.object_id );
                    }

                    // Если найдена запись о курсе - берём статус и очки
                    if (findCourse != undefined) { 
                    res_elem.ccourse_id = String(findCourse.id); 
                    res_elem.state_id = String(findCourse.state_id); 
                    res_elem.score = findCourse.state_id == 1 ? "10" : String(findCourse.score);
                    }

                } else if (String(part.type)=='test') {
                    findTest = undefined;
                    assignedTest =  ArrayOptFirstElem(XQuery("for $elem in assessments where $elem/id = "+XQueryLiteral(part.assessment_id)+" return $elem"));
                    if (assignedTest != undefined) {
                        findTest =  ArrayOptFirstElem(XQuery("for $elem in test_learnings where $elem/assessment_id = "+assignedTest.id+" and ($elem/person_id = "+curUserID+" and $elem/event_id = "+event_id+") order by $elem/start_usage_date descending return $elem"));
                        if(findTest != undefined) {
                            if ( findTest.state_id != 3) {
                                res_elem.link = "/view_doc.html?mode=test_learning_stat&object_id="+findTest.id+"&sid="+tools.get_sum_sid( assignedTest.id );
                            } else {
                                findTest = ArrayOptFirstElem(XQuery("for $elem in active_test_learnings where $elem/assessment_id = "+assignedTest.id+" and $elem/person_id = "+curUserID+" order by $elem/start_usage_date descending return $elem"));
                                if (findTest == undefined) {
                                    tools.activate_test_to_person(curUserID, assignedTest.id, event_id)
                                    findTest = ArrayOptFirstElem(XQuery("for $elem in active_test_learnings where $elem/assessment_id = "+assignedTest.id+" and $elem/person_id = "+curUserID+" order by $elem/start_usage_date descending return $elem"));
                                }
                                res_elem.link = "/test_launch.html?assessment_id="+assignedTest.id+"&object_id="+findTest.id+"&sid="+tools.get_sum_sid( assignedTest.id )
                                findTest.state_id = 3
                            }
                        } else {
                            findTest = ArrayOptFirstElem(XQuery("for $elem in active_test_learnings where $elem/assessment_id = "+assignedTest.id+" and ($elem/person_id = "+curUserID+" and $elem/event_id = "+event_id+") order by $elem/start_usage_date descending return $elem"));
                            if(findTest != undefined) {
                                res_elem.link = "/test_launch.html?assessment_id="+assignedTest.id+"&object_id="+findTest.id+"&sid="+tools.get_sum_sid( assignedTest.id );
                            }
                            else {
                                tools.activate_test_to_person(curUserID, assignedTest.id, event_id);
                                findTest = ArrayOptFirstElem(XQuery("for $elem in active_test_learnings where $elem/assessment_id = "+assignedTest.id+" and $elem/person_id = "+curUserID+" order by $elem/start_usage_date descending return $elem")); //тут event еще не прописан
                                if(findTest != undefined) {
                                    res_elem.link = "/test_launch.html?assessment_id="+assignedTest.id+"&object_id="+findTest.id+"&sid="+tools.get_sum_sid( assignedTest.id );
                                }
                            }
                        }
                    } else {
                            res_elem.link = "/course_launch.html?course_id="+_class.object_id+"&object_id="+active_class.id+"&event_id="+event_id+"&sid="+tools.get_sum_sid( _class.object_id  );
                    }

                    // Если найдена запись о тесте - берём статус и очки
                    if (findTest != undefined) { 
                        res_elem.ccourse_id = String(findTest.id); 
                        res_elem.state_id = OptInt(findTest.state_id); 
                        res_elem.score = findTest.state_id == 1 ? "10" : String(findTest.score);
                        res_elem.max_score = String(findTest.max_score)

                        // Следующий код заполняет данные в незаконченном курсе по элементу тест, т.к. после завершения тест автоматически не проставляет данные в курс
                        if (ArrayCount(coursePlanDoc.TopElem.parts) == 1 && coursePlanDocTE.state_id < 2) {
                            res_class.course = coursePlanDocTE
                            if (findTest.state_id > 1) {
                                part.start_usage_date = findTest.start_usage_date
                                part.last_usage_date = findTest.last_usage_date
                                part.state_id = findTest.state_id
                                part.cur_state_id = findTest.state_id
                                part.score = findTest.score
                                part.cur_score = findTest.score
                                part.max_score = findTest.max_score
                                coursePlanDocTE.state_id = 2
                                coursePlanDoc.Save()
                            }
                        }
                    }
                } else if (String(part.type)=='inline') {
                            //res_elem.link = String(part.url); // Нужно достать из парта url..
                }

                if (res_elem.score == 0 && res_elem.state_id == 1) res_elem.score = 10;
                res_class.children.push(res_elem);
                res_class.state_id=OptInt(active_class.state_id);
            }

            if (ArraySelect(res_class.children, 'state_id == "1"').length > 0) res_class.state_id = 1
            if (ArraySelect(res_class.children, 'state_id > 1').length == ArrayCount(res_class.children)) res_class.state_id = 2
            res_module.children.push(res_class)
        }
        structure.push(res_module);
    }
    result = structure;

	return result;
}

function setCourseElemStatus(object, state, userID) {    //set it to 2 (finished)

    //only заочная form for now
    var this_active_course = ArrayOptFind(
        ArrayUnion(
            XQuery("for $elem in active_learnings where $elem/person_id = " + OptInt(userID ? userID : curUserID) + " return $elem"),
            XQuery("for $elem in learnings where $elem/person_id = " + OptInt(userID ? userID : curUserID) + " return $elem")
        ), 
        'This.course_id == ' + String(object.course_id)
    )

    if (this_active_course != undefined) {
        courseDoc = tools.open_doc(this_active_course.id)
        courseDocTE = courseDoc.TopElem
        this_part =  ArrayOptFind(courseDocTE.parts, 'This.PrimaryKey == "' + object.code + '"')
        response.parts = this_part
        response.part_code = String(object.code)
        
        if (this_part != undefined) {
            this_part.state_id = state ? state : 2
            courseDoc.Save()
            response.success = "Статус установлен"
        } else {
            response.error = 'Элемент занятия не найден'; return
        }

    } else {
        response.error = 'Занятие не найдено'; return
    }

}

function createTaskResult(form, event_id) {
    task = tools.read_object(form.GetProperty("task"))
    file = form.GetProperty("file")
    response.element = task
    if (task.answer == "" && file == null) throw "Необходим комментарий или файл"
    plan_id = ArrayOptFirstElem( XQuery("for $elem in education_plans where $elem/event_id = "+event_id+" and $elem/person_id = "+curUserID+" return $elem/id") )
    group_id = ArrayOptFirstElem( XQuery("for $elem in education_plans where $elem/event_id = "+event_id+" and $elem/person_id = "+curUserID+" return $elem/group_id") )

    fileDoc = tools.new_doc_by_name('learning_task_result')
	teFileDoc = fileDoc.TopElem
    teFileDoc.answer = String(task.answer)
    fileDoc.BindToDb(DefaultDb)
    teFileDoc.learning_task_id = task.object_id
    teFileDoc.person_id = curUserID
    teFileDoc.person_fullname = curUser.fullname
    teFileDoc.education_plan_id = plan_id
    teFileDoc.status_id = 'evaluation'
    teFileDoc.start_date = Date()
    newChild = teFileDoc.files.AddChild()
    newFile = createNewFile(file, group_id)
    response.file = newFile
    newChild.file_id = newFile.id
    
    fileDoc.Save()
    setCourseElemStatus(task, 1)
    response.data = "success"
}

function createNewFile(file, group_id) {
	// try {
	var obj = {}
	
	xLocalFile = 'x-local://wt/web/upload/group_' + group_id + '/'
	fileSourceID = ArrayOptFirstElem( XQuery( "for $elem in file_sources where $elem/code = 'file_system_upload' return $elem/id" ) )

	_bytes = StrLen( file )
	_fileName = String(file.FileName)
	dots = _fileName.split(".")
	_fileType =  dots[dots.length - 1]

	var fileDoc = tools.new_doc_by_name('resource')
	teFileDoc = fileDoc.TopElem
	fileDoc.BindToDb(DefaultDb)
	xLocalFile = xLocalFile + fileDoc.DocID + '.' + _fileType

	PutUrlData( xLocalFile, file )

	teFileDoc.name = _fileName
	teFileDoc.size = _bytes
	teFileDoc.code = 'res_' + fileDoc.DocID
	teFileDoc.type = 'file' // TODO types
	// teFileDoc.comment = object.desc // Добавить когда-нибудь комментарий
	teFileDoc.allow_download = 1
	teFileDoc.allow_unauthorized_download = 1
	teFileDoc.person_id = curUserID
	teFileDoc.file_source = fileSourceID
	teFileDoc.file_url = xLocalFile
	teFileDoc.file_path = xLocalFile
	pasteElemToRole(group_id, "resource", teFileDoc)
	fileDoc.Save()
	

	obj.id = OptInt(fileDoc.DocID)
	obj.name = String(_fileName)
	obj.url = StrReplace(xLocalFile, 'x-local://wt/web/', '')
	obj.size = OptInt(_bytes)
	obj.type = String(_fileType)

	return obj
	// } catch(err) { response.error = String(err) }
}

function getFiles(obj_id) {
    teDoc = tools.open_doc(obj_id) != undefined ? tools.open_doc(obj_id).TopElem : null
    arr = []
    if (teDoc == null) return arr

    for (file in teDoc.files) {
		_Res = ArrayOptFirstElem( XQuery("for $elem in resources where $elem/id = " + file.file_id + " return $elem") )
		teRes = tools.open_doc(file.file_id) != undefined ? tools.open_doc(file.file_id).TopElem : null
        if (teRes == null) continue
        arr.push({
			"id": String(teRes.id),
			"name": String(teRes.name),
			"type": StrLowerCase( UrlPathSuffix( teRes.file_url ) ),
			"size": String(teRes.size),
			"url": StrReplace(teRes.file_url, 'x-local://wt/web/', ''),
			"lastModifiedDate": String(_Res.modification_date)
		})
    }

    return arr
}

function pasteElemToRole (group_id, catalog, teDoc) {
	teGroup = ArrayOptFirstElem(XQuery("for $elem in groups where $elem/id = " + group_id + " return $elem"))
	if(ArrayCount(XQuery("for $elem in roles where $elem/catalog_name = '" + catalog + "' and $elem/code = 'group_" + group_id + "' return $elem"))) {
		roleID = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = '" + catalog + "' and $elem/code = 'group_" + group_id + "' return $elem/id"))
		teDoc.role_id.Clear()
		teDoc.role_id.Add().Value = roleID
	} else {
        var newCat = OpenNewDoc('x-local://qti/qti_role.xmd')
        parent_role_id = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = '" + catalog + "' and $elem/code = 'learning_task_resources' return $elem/id"))
		newCat.TopElem.name = teGroup.name
		newCat.TopElem.code = 'group_' + group_id
        newCat.TopElem.catalog_name = catalog
        newCat.TopElem.parent_role_id = parent_role_id
		newCat.BindToDb(DefaultDb)
		newCat.Save()
		teDoc.role_id.Add().Value = newCat.DocID
	}
}

/*### Функции - конец###*/

/*### Обработка запроса - начало###*/
try {
    //response.query = Request.Query;
    
	if (request_type == 'GET') {

        if(Request.Query.HasProperty("event_id")) {	//Получение информации по плану обучения текущего пользователя через мероприятие
            response.data = openPlan(Request.Query.event_id);
        }  else {				
            response.error = 'Ошибка запроса - недостаточно данных';
        }

	} else if (request_type == 'POST') {
		var courseElem = tools.read_object(Request.Body);    //element on which user clicked
		if (Request.Query.HasProperty("set_status")) {
			setCourseElemStatus(courseElem);
        }
        if (Request.Query.HasProperty("learning_task")) {
            createTaskResult(Request.Form, Request.Query.event_id)
        }
	}
	else {
		response.error = 'Ошибка запроса';
    }
    
} catch (e) {	
	response.error = String(e);
}

Response.Write(tools.object_to_text(response, 'json'));
/*### Обработка запроса - конец###*/
%>