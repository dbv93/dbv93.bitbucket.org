<%
	function getOrgs () {
		var result = [];
		var orgsQuery = XQuery("for $elem in orgs where $elem/code != 'copp' order by $elem/name return $elem");

		for (org in orgsQuery) {
		    result.push({
			id: Int(org.id),
			name: String(org.name),
			code: String(org.code)
		    });
		}
		return result;
	}


	function openCollaborator (collab_id) {

		var result = {};
		var teCollDoc = OpenDoc(UrlFromDocID(Int(collab_id))).TopElem;

		result.id = Int(teCollDoc.id);

		result.lastname= String(teCollDoc.lastname);
		result.firstname =String(teCollDoc.firstname);
		result.middlename = String(teCollDoc.middlename);

		result.sex = String(teCollDoc.sex);
		result.birth_date = String(teCollDoc.birth_date);
		result.email = String(teCollDoc.email);
		result.phone = String(teCollDoc.phone);
		result.position = {id: Int(teCollDoc.position_id), name: String(teCollDoc.position_name)};
		//result.subdiv =  {id: Int(teCollDoc.position_parent_id), name: String(teCollDoc.position_parent_name)};	//TODO: редактирование этих полей
		result.org = {id: Int(teCollDoc.org_id), name: String(teCollDoc.org_name)};

		return result;//teCollDoc;

	}

	function getResponsable (teOrgDoc) {

		if (teOrgDoc.ChildExists('func_managers')&&ArrayOptFirstElem(ArrayDirect(teOrgDoc.func_managers))!=undefined) {

		    return openCollaborator (ArrayOptFirstElem(ArrayDirect(teOrgDoc.func_managers)).person_id);

		} else {

		    return {
			id: null,
			
			lastname: '',
			firstname: '',
			middlename: '',

			sex: '',
			birth_date: '',
			email: '',
			phone: '',

			position: {id: null, name: ''},
			subdiv:  {id: null, name: ''},
			org: {id: null, name: ''}
		    }

		}

	}

	function openOrg (org_id) {

		var result = {};
		var teOrgDoc = OpenDoc(UrlFromDocID(Int(org_id))).TopElem;

		result.id = Int(teOrgDoc.id);
		result.code = String(teOrgDoc.code);
		result.name = String(teOrgDoc.name);
		result.disp_name = String(teOrgDoc.disp_name);
		result.postal_address = String(teOrgDoc.postal_address);
		result.web =  String(teOrgDoc.web);
		result.responsable = getResponsable(teOrgDoc);

		//result.legal_address = String(teOrgDoc.essentials.legal_address);

		return result;//teCollDoc;

	}

	function createPosition (collab_id, org_id) {
		var new_doc = tools.new_doc_by_name('position');
		new_doc.BindToDb(DefaultDb);	

		new_doc.TopElem.name = 'представитель';
		new_doc.TopElem.basic_collaborator_id = Int(collab_id);
		new_doc.TopElem.org_id = Int(org_id);

		new_doc.Save();
		return new_doc.DocID;
	}	

	function createOrModifyOrg (data) {

		var new_doc;
		var resp_doc;

		if (data.HasProperty('id')) {

		    new_doc = OpenDoc(UrlFromDocID(Int(data.id)));
		    resp_doc =  OpenDoc(UrlFromDocID(Int(data.responsable.id)));

		} else {

			new_doc = tools.new_doc_by_name('org');
			new_doc.BindToDb(DefaultDb);

			resp_doc = tools.new_doc_by_name('collaborator');
			resp_doc.BindToDb(DefaultDb);

		}


		//new_doc.TopElem.login = String(data.login);
		//new_doc.TopElem.password = String(data.password);

		new_doc.TopElem.name = String(data.name);
		new_doc.TopElem.disp_name = String(data.disp_name);
		new_doc.TopElem.code = String(data.code);

		new_doc.TopElem.postal_address = String(data.postal_address);
		new_doc.TopElem.web = String(data.web);

		resp_doc.TopElem.lastname= String(data.responsable.lastname);
		resp_doc.TopElem.firstname =String(data.responsable.firstname);
		resp_doc.TopElem.middlename = String(data.responsable.middlename);
		resp_doc.TopElem.email = String(data.responsable.email);
		resp_doc.TopElem.phone = String(data.responsable.phone);

		if (!data.HasProperty('id')) { 

			resp_doc.TopElem.position_id = createPosition(resp_doc.DocID, new_doc.DocID);
			var new_func_manager = new_doc.TopElem.func_managers.AddChild();
			new_func_manager.person_id  = resp_doc.DocID;
			tools.common_filling( 'collaborator', new_func_manager, resp_doc.DocID, resp_doc.TopElem);

		}

		resp_doc.Save();
		new_doc.Save();
		OpenDoc(UrlFromDocID(Int(resp_doc.TopElem.position_id))).Save();

		return new_doc;

	}   
	    
	function deleteOrg (id) {

		var teOrgDoc = OpenDoc(UrlFromDocID(Int(id))).TopElem;
		DeleteDoc(UrlFromDocID(Int(id)));					//delete org

		var respObj = ArrayOptFirstElem(ArrayDirect(teOrgDoc.func_managers));
	
		if (respObj != undefined) {
			DeleteDoc(UrlFromDocID(Int(respObj.person_id)));		//delete responasable
			DeleteDoc(UrlFromDocID(Int(respObj.person_position_id)));	//delete responasble position
		}

	}

	var response = {};

	try {       

		if(Request.Method == "GET") {

		    if (Request.Query.HasProperty('id')) {
			response.orgInfo = openOrg(Request.Query.id);		           
		    } else {
			response.orgs = getOrgs();
		    }

		} else if (Request.Method == "POST") {


			//TODO: add org logo
		    var orgInfo = tools.read_object(Request.Body);
			response.body = orgInfo;
		    if (Request.Query.HasProperty('delete')) {
			deleteOrg(orgInfo.id);
		    } else {
			createOrModifyOrg(orgInfo);
		    }
		}

	} catch (err) {response.error = String(err);}

	Response.Write(tools.object_to_text(response, 'json'));
%>