<%

var accept_image = ".jpg,.jpeg,.png";
var accept_file = ".doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip";
function getObjs() {
  var result = [];
  var objsQuery = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6753954396609189673 return $elem");
  for (obj in objsQuery) {
    teObjDoc = OpenDoc(UrlFromDocID(Int(obj.id))).TopElem;
    user_creatorName = OpenDoc(UrlFromDocID(Int(teObjDoc.doc_info.creation.user_id))).TopElem.fullname;
    create_date = StrXmlDate(Date(teObjDoc.doc_info.creation.date), false)
    result.push({
      id: Int(obj.id),
      name: String(obj.name),
      create_date: String(create_date),
      user_creator: String(user_creatorName),
      code: String(obj.code)
    });
  }
  return result;
}

function getCompetences() {
  var result = [];
  var objsQuery = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6753954396609189672 return $elem");
  for (obj in objsQuery) {
    result.push({
      knowledge_part_id: Int(obj.id),
      knowledge_part_name: String(obj.name)
    });
  }
  return result;
}


function getKnowledgeLevels() {
  var result = [];
  var objsQuery = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6753954396609189731 return $elem");
  for (obj in objsQuery) {
    result.push({
      knowledge_part_id: Int(obj.id),
      knowledge_part_name: String(obj.name)
    });
  }
  return result;
}




function getImg(id) {
  fileExist = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = " + XQueryLiteral(id) + " return $elem")) != undefined;
  if (fileExist) {
    imgDoc = OpenDoc(UrlFromDocID(Int(id))).TopElem;
    obj = {};
    obj.id = String(id);
    obj.name = String(imgDoc.name);
    obj.url = "/download_file.html?file_id=" + String(id);
    return obj;
  }
}



function openObj(obj_id) {

  var result = {};
  var teObjDoc = OpenDoc(UrlFromDocID(Int(obj_id))).TopElem;

  result.id = Int(teObjDoc.id);
  result.name = String(teObjDoc.name);
  result.code = String(teObjDoc.code);
  result.comment = String(teObjDoc.comment);
  result.text_area = String(teObjDoc.text_area);
  result.image = teObjDoc.resource_id ? getImg(teObjDoc.resource_id) : [];
  result.competences = [];
  result.knowledgelevels = [];

  for (_competence in teObjDoc.knowledge_parts) {
    if (OpenDoc(UrlFromDocID(Int(_competence.knowledge_part_id))).TopElem.knowledge_classifier_id == 6753954396609189672) {
      result.competences.push(_competence)
    }
  }
  for (_knowledgelevel in teObjDoc.knowledge_parts) {
    if (OpenDoc(UrlFromDocID(Int(_knowledgelevel.knowledge_part_id))).TopElem.knowledge_classifier_id == 6753954396609189731) {
      result.knowledgelevels.push(_knowledgelevel)
    }
  }
  // result.status = (teObjDoc.completed ? 'Завершен' : 'Проводится');
  // result.questions = [];
  // for (question in teObjDoc.questions) {
  //   result.questions.push({ id: Int(question.id), title: String(question.title), type: String(question.type), entries: ArrayDirect(question.entries) });
  // }
  return result;

}


function modifyArray (docObj, newarray) {

  var i;

  docObj.TopElem.knowledge_parts.Clear();
  response.newarr = newarray
  for (i = 0; i<ArrayCount(newarray); i++) {

    newItem = docObj.TopElem.knowledge_parts.AddChild();
    newItem.knowledge_part_id = newarray[i].knowledge_part_id;
    newItem.knowledge_part_name = newarray[i].knowledge_part_name;
  }
}

function createOrModifyObj(data) {

  var new_doc;

  if (data.HasProperty('id')) {

    new_doc = OpenDoc(UrlFromDocID(Int(data.id)));

  } else {
    new_doc = tools.new_doc_by_name('knowledge_part');
    new_doc.BindToDb(DefaultDb);
    new_doc.TopElem.knowledge_classifier_id = "0x5DBADDBD5F141729"
  }

  new_doc.TopElem.name = String(data.name);
  new_doc.TopElem.comment = String(data.comment);
  new_doc.TopElem.text_area = String(data.text_area);
  modifyArray(new_doc, ArrayUnion(data.competences, data.knowledgelevels))
  new_doc.TopElem.parent_object_id = 6760201305451088461;
  // modifyQuestions(new_doc, data.questions);

  new_doc.Save();

  return new_doc.DocID;

}

function deleteObj(id) {

  //var teOrgDoc = OpenDoc(UrlFromDocID(Int(id))).TopElem;
  DeleteDoc(UrlFromDocID(Int(id)));					//delete poll

}

var response = {};

try {

  if (Request.Method == "GET") {

    if (Request.Query.HasProperty('id')) {
      response.objInfo = openObj(Request.Query.id);
    } else {
      response.data = getObjs();
      response.competences = getCompetences();
      response.knowledgelevels = getKnowledgeLevels();
    }

  } else if (Request.Method == "POST") {

    var objInfo = tools.read_object(Request.Body);
    response.body = objInfo;
    if (Request.Query.HasProperty('delete')) {
      deleteObj(objInfo.id);
    } else {
      response.id = createOrModifyObj(objInfo)
    }
  }

} catch (err) { response.error = String(err); }

Response.Write(tools.object_to_text(response, 'json'));
%>