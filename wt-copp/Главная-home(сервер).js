<%
	//------------------------------------------------------------------------------Функции-начало---------------------------------------------------------------------//

	//-------------------------------------------------------------в-порядке-следования-на-главной----------------------------------------------------------------//

	function getCategories () { //типа группы доступа, пока не понятно по тз
		var result = [];
		var queryRes = XQuery("for $elem in groups where $elem/is_educ = false() order by $elem/name return $elem");
		for (_querObj in queryRes) {
	
			_resObj = {
				id: Int(_querObj.id),
				name: String(_querObj.name)
			};

			result.push(_resObj);
		}
		return result;
	}


	function getEvents () {	//события
		var result = [];
		var queryRes = XQuery("for $elem in events order by $elem/name return $elem"); 

		function getEventTypeName(type) {

			switch(type) {
				case 'assessment': 
					return 'Тестирование';
				break;
				case 'case':
					return 'Кейс';
				break;
				case 'compound_program':
					return 'Модульная программа';
				break;
				case 'compound_program_elem':
					return 'Элемент модульной программы';
				break;
				case 'dist_test':
					return 'Дистанционное обучение';
				break;
				case 'education_method':
					return 'Учебная программа';
				break;
				case 'education_method_from_program':
					return 'Элемент из набора программ';
				break;
				case 'one_time':
					return 'Разовое мероприятие';
				break;
				case 'webinar':
					return 'Вебинар';
				break;
				default:
					return 'Разовое мероприятие';
			}
		}



		function getDate(date) {
			date = new Date(date);
			return {
				year: Year(date),
				month: Month(date),
				day: Day(date),
				hour: Hour(date),
				minute: Minute(date),
				stringDate: StrLongDate(date),
				ISOdate: StrXmlDate (date, false)
			};
		}


		for (_querObj in queryRes) {
	
			_resObj = {
				id: Int(_querObj.id),
				name: String(_querObj.name),
				start_date: getDate(_querObj.start_date),
				duration: (String(_querObj.start_date)!=''&&String(_querObj.finish_date)!='' ? String(DateDiff(_querObj.finish_date, _querObj.start_date)/3600): ''),
				type: String(getEventTypeName(_querObj.type_id))
			};

			result.push(_resObj);
		}
		return result;
	}

	function getProfessions () {	//профессии - хз что такое востребованные, мне в гугл запрос посылать?
		var result = [];
		var queryRes = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6753954396609189673 return $elem");
		for (_querObj in queryRes) {
	
			_resObj = {
				id: Int(_querObj.id),
				name: String(_querObj.name)
			};

			result.push(_resObj);
		}
		return result;
	}

	function getCoppInfo () {
		var result = {};

		coppOrg = OpenDoc(UrlFromDocID(6748398834638268537)).TopElem;
		coppPlace = OpenDoc(UrlFromDocID(6756890869092806695)).TopElem;

		_resObj = {
			email: String(coppOrg.email),
			phone: String(coppOrg.phone),
			address:  String(coppPlace.address),
			schedule: String(coppPlace.comment)
		}
		result = _resObj;
		return result;
	}

	function getStatNumbers () {
		var result = {};

		var countOrgs = ArrayCount(XQuery("for $o in orgs, $e in education_orgs where $o/name = $e/name return $o"))

		_resObj = {
			orgs: ArrayCount(XQuery("for $o in orgs, $e in education_orgs where $o/name = $e/name return $o")),
			comps: ArrayCount(XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6753954396609189673 return $elem")),
			progs: ArrayCount(XQuery("for $elem in compound_programs return $elem")),
			learn: ArrayCount(XQuery("for $elem in education_plans return $elem"))
		};
		result = _resObj;
		return result;
	}


	//------------------------------------------------------------------------------Функции-конец----------------------------------------------------------------------//


	//------------------------------------------------------------------Обработка запроса-начало--------------------------------------------------------------------//
	var response = {};

	try {
		if (Request.Method=='POST') {	

		}

		else if (Request.Method=='GET') {
	
			/*
			попытки что-то распараллелить
			var thread = new Thread;
			thread.EvalCode( 'getCategories ()' );
			response.thread = thread;
			thread.IsRunning
			
			Выводы: скорее всего, в EvalCode можно передавать только глобальные функции (типа tools.) Далее возникнет необходимость отслеживать завершение потоков - мб можно на коллбеках как-то написать?
			В общем, придется покопаться если хотим как-то ускорить сервер

			*/

			//response.categories = getCategories ();
			response.events = getEvents ();
			response.professions = getProfessions ();
			response.coppInfo = getCoppInfo ();
			response.statNumbers = getStatNumbers ();
			
		}
	} catch (err) {response.error = String(err)}

	Response.Write(tools.object_to_text(response, 'json')); 
	//--------------------------------------------------------------------Обработка запроса-конец--------------------------------------------------------------------//
%>

