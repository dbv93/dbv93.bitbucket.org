<%
    function getOrgs () {
        var result = []
        var objsQuery = XQuery("for $elem in orgs order by $elem/name return $elem");
        for (obj in objsQuery) {
            result.push({
                id: String(obj.id),
                name: String(obj.name)
            })  
        }
        return result
    }

    function getCollaborators () {
        var result = []
        var objsQuery = XQuery("for $elem in collaborators where $elem/fullname != '' order by $elem/fullname return $elem");
        for (obj in objsQuery) {
            result.push({
                id: String(obj.id),
                fullname: String(obj.fullname)
            })  
        }
        return result
    }

    function getQuestionTypes () {
        return [{
            type: "multiple_choice", title: "Единственный выбор"
        },{
            type: "multiple_response", title: "Множественный выбор"
        },{
            type: "order", title: "Ранжирование"
        },{
            type: "gap_fill", title: "Текстовый ввод"
        },{
            type: "numerical_fill_in_blank", title: "Цифровой ввод"
        },{
            type: "match_item", title: "Соответствие"
        }]
	}
	
	function getTestStatuses () {
		return [{
			status: "publish", title: "Опубликован"
		},{
			status: "project", title: "Проект"
		},{
			status: "secret", title: "Скрыт"
		},{
			status: "archive", title: "Архив"
		}]
	}

	function getOrgIdFromFolder(teDoc) {
		var result = undefined
		var role_id =  ArrayOptFirstElem(ArrayExtract(teDoc.role_id, 'This'))
		if (role_id != undefined) {
			result = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/id = "+role_id+" return $elem/code"))
			if (result != undefined) result = StrReplace(String(result), 'org_','')
		}
		return result
	}

    function getOrgItems (org_id) {
        var result = []
		if (org_id != undefined) {
			var role_id = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = 'item' and $elem/code = 'org_" + org_id + "' return $elem/id"));
			var objsQuery = XQuery("for $elem in items where MatchSome( $elem/role_id, (" + role_id + ") ) order by $elem/name return $elem");
		} else {
			var objsQuery = XQuery("for $elem in items order by $elem/name return $elem");
		}
        for (obj in objsQuery) {
            result.push({
                id: String(obj.id),
				title: String(obj.title),
				type_id: String(obj.type_id)
            })  
        }
        return result
    }

	function getObjs (org_id) {	//Общий GET
		var result = [];
		if (org_id != undefined) {
			var role_id = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = 'assessment' and $elem/code = 'org_" + org_id + "' return $elem/id"));
			var objsQuery = XQuery("for $elem in assessments where MatchSome( $elem/role_id, (" + role_id + ") ) order by $elem/title return $elem");
		} else {
            var objsQuery = XQuery("for $elem in assessments order by $elem/title return $elem");
		}
		for (obj in objsQuery) {

		    teObjDoc = OpenDoc(UrlFromDocID(Int(obj.id))).TopElem;

		    creatorName = creationDate = 'Не задано';

		    countTestActivities = ArrayCount ( ArrayUnion (
                XQuery("for $elem in active_test_learnings where $elem/assessment_id = "+XQueryLiteral(obj.id)+" return $elem"),
                XQuery("for $elem in test_learnings where $elem/assessment_id = "+XQueryLiteral(obj.id)+" return $elem")
            ))

		    if (String(teObjDoc.doc_info.creation.user_login) != '') {
			    creatorObj = ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/login = "+XQueryLiteral(teObjDoc.doc_info.creation.user_login)+" return $elem"));
			    if (creatorObj != undefined) creatorName = String(creatorObj.fullname);
		    }
		    if (String(teObjDoc.doc_info.creation.date) != '') {
			creationDate = String(teObjDoc.doc_info.creation.date)
		    }


		    result.push({

			id: String(obj.id),
			title: String(obj.title),
			status: String(obj.status),
			create_date: creationDate,
			test_moderator: creatorName,
			participants_num: countTestActivities

		    });
		}
		return result;
    }
    
	function openObj (obj_id) {
		var result = {};
		var teObjDoc = OpenDoc(UrlFromDocID(OptInt(obj_id))).TopElem;
		result.id = String(teObjDoc.id);
		result.title = String(teObjDoc.title);
        result.status = String(teObjDoc.status);
		result.sections = [];
		result.dev_org = getOrgIdFromFolder(teObjDoc)
		result.publish_url = String(teObjDoc.publish_url);
		for (section in teObjDoc.sections) { 
			_items = [];
			for (item in section.items) {
                // try {
                //     teItemDoc = OpenDoc(UrlFromDocID(OptInt(item.id))).TopElem
                // } catch (err) {continue}
                // answers = [];
                // for (answer in teItemDoc.answers) {
                //     answers.push({
                //         id: String(answer.id),
                //         text: String(answer.text),
                //         is_correct_answer: tools_web.is_true (answer.is_correct_answer, false)
                //     })
                // }
				_items.push( {id: Int(item.id), title: String(item.title)} );
			}
			result.sections.push({id: Int(section.id), title: String(section.title), items: _items});
		}
		return result;

	}

	function openItem (id) {
		var result = {}
		teItemDoc = tools.open_doc(id) != undefined ? tools.open_doc(id).TopElem : null
		if (teItemDoc != null) {
			answers = [];
			for (answer in teItemDoc.answers) {
				answer_obj = {
					id: String(answer.id),
					text: String(answer.text),
					is_correct_answer: tools_web.is_true (answer.is_correct_answer, false)
				}
				if (ArrayCount(answer.conditions)) {
					_conditions = []
					for (_condition in answer.conditions) {
						_conditions.push({value: String(_condition.value)})
					}
					answer_obj.conditions = _conditions
				} else if (ArrayCount(answer.values)) {
					_values = []
					for (_value in answer.values) {
						_values.push({text: String(_value.text)})
					}
					answer_obj.values = _values
				}
				answers.push(answer_obj)
			}
			result = {id: Int(teItemDoc.id), title: String(teItemDoc.title), answers: answers, type_id: String(teItemDoc.type_id)}
		}
		return result
	}

	function createOrModifyItem (data) {
		itemDoc = tools.open_doc(data.id) != undefined ? tools.open_doc(data.id) : null
		if (itemDoc == null) {
			itemDoc = tools.new_doc_by_name('item')
			itemDoc.BindToDb(DefaultDb)
		}
		if (data.HasProperty('dev_org')) pasteElemToRole(Int(data.dev_org), 'item', itemDoc.TopElem)

		itemDoc.TopElem.title = data.title
		itemDoc.TopElem.question_text = data.title
		itemDoc.TopElem.type_id = data.type_id
		itemDoc.TopElem.answers.Clear();

		for (answer in data.answers) {
			newAnswerItem = itemDoc.TopElem.answers.AddChild();
			newAnswerItem.id = answer.id
			newAnswerItem.text = answer.text
			newAnswerItem.is_correct_answer =  answer.is_correct_answer
			if( String(data.type_id) == 'gap_fill' ||  String(data.type_id) == 'numerical_fill_in_blank') {
				newCondition = newAnswerItem.conditions.AddChild();
				newCondition.grading_option_id = '='
				newCondition.sentence_option_id = 'equal'
				newCondition.value = answer.conditions[0].value
				newCondition.case_sensitive = 0
			} else if ( String(data.type_id) == 'match_item' ) {
				newValue = newAnswerItem.values.AddChild();
				newValue.text = answer.values[0].text
			}
		}


		itemDoc.Save()
		return {id: String(itemDoc.DocID) , title: String(itemDoc.TopElem.title), type_id: String(itemDoc.TopElem.type_id)};
	}

	function modifySections (docObj, sectionsArray) {

        docObj.TopElem.sections.Clear();

		for (section in sectionsArray) {

			newSectionItem = docObj.TopElem.sections.AddChild();
			newSectionItem.title = section.title;
			for (item in section.items) {
				newItem = newSectionItem.items.AddChild();
				newItem.id = item.id
				newItem.title = item.title
			}
        }   
        docObj.Save()
	}

	function createOrModifyObj (data) {

		var new_doc;

		if (data.HasProperty('id') && !StrContains(data.id, '_')) {
			new_doc = tools.open_doc(data.id) != undefined ? tools.open_doc(data.id) : null
			if (new_doc != null) {
				modifySections (new_doc, data.sections)
				qti_tools.pulish_assessment(new_doc.DocID)
			}
		} else {
			new_doc = tools.new_doc_by_name('assessment');
			new_doc.BindToDb(DefaultDb);
			if (data.HasProperty('dev_org')) pasteElemToRole(Int(data.dev_org), 'assessment', new_doc.TopElem)
		}

		new_doc.TopElem.title = String(data.title)
		new_doc.Save()

		return { id: String(new_doc.DocID) }

	}   

	function pasteElemToRole (org_id, catalog, teDoc) {
		teOrg = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id = " + org_id + " return $elem"));
		if(ArrayCount(XQuery("for $elem in roles where $elem/catalog_name = '" + catalog + "' and $elem/code = 'org_" + teOrg.id + "' return $elem"))) {
			roleID = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = '" + catalog + "' and $elem/code = 'org_" + teOrg.id + "' return $elem/id"));
			teDoc.role_id.Clear();
			teDoc.role_id.Add().Value = roleID;
		} else {
			var newCat = OpenNewDoc('x-local://qti/qti_role.xmd');
			newCat.TopElem.name = teOrg.name;
			newCat.TopElem.code = 'org_' + teOrg.id;
			newCat.TopElem.catalog_name = catalog;
			newCat.BindToDb(DefaultDb);
			newCat.Save();
			teDoc.role_id.Add().Value = newCat.DocID;
		}
	}
	    
	function deleteObj (id) {

		DeleteDoc(UrlFromDocID(Int(id)));					//delete obj

	}

	var response = {};

	try {       

		if(Request.Method == "GET") {

		    if (Request.Query.HasProperty('id')) {
				response.data = openObj(Request.Query.id);
				response.lists = {
					items: getOrgItems (undefined)	//TODO pass org_id
				}
			} else if (Request.Query.HasProperty('question')) {
			 response.data = openItem(Int(Request.Query.question));           
		    } else if (Request.Query.HasProperty('org_id')) {	//TODO
			    response.data = getObjs(Request.Query.org_id);
		    } else {
			if (ArrayOptFirstElem(XQuery("for $elem in group_collaborators where $elem/collaborator_id = "+XQueryLiteral(Int(curUserID))+" and $elem/group_id = 6761406065601834020 return $elem"))!= undefined) {
				response.canEdit = false;
			} else { response.canEdit = true; }
			response.data = getObjs();
			response.lists = {
				orgs: getOrgs(),
                collaborators: getCollaborators(),
                question_types: getQuestionTypes(),
                test_statuses: getTestStatuses()
			}
		    }

		} else if (Request.Method == "POST") {

		    var objInfo = tools.read_object(Request.Body);
		        //response.body = objInfo;
		    if (Request.Query.HasProperty('delete')) {
			    deleteObj(objInfo.id);
		    } else if (Request.Query.HasProperty('org_id')) {
			   response.data = createOrModifyObj(objInfo);
		    } else if (Request.Query.HasProperty('question')) {
			   response.data = createOrModifyItem(objInfo);
			} else {
			   response.data = createOrModifyObj(objInfo);
		    }
		}

	} catch (err) {response.error = String(err);}

	Response.Write(tools.object_to_text(response, 'json'));
%>