<%
	function getSex (sexString) {
		if (sexString == 'w') return 'Женский';
		if (sexString == 'm') return 'Мужской';
		return '';
	}
	function getObjs () {	//Общий GET
		var userObj = ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/id = "+XQueryLiteral(curUserID)+" return $elem"));
		var userOrgId = String(userObj.org_id);
		var orgVacancyDoc = ArrayOptFirstElem(XQuery("for $elem in documents where $elem/code = "+XQueryLiteral(userOrgId)+" return $elem"));
		var result = {};
		result.objs = [];
		var competence = [];
		var competenceAll = [];
		if (orgVacancyDoc != undefined) {
			objsQuery =  XQuery("for $elem in documents where $elem/parent_document_id = " + XQueryLiteral(Int(orgVacancyDoc.id))+ " return $elem");
			for (obj in objsQuery) {
				competenceAll = [];
				competenceArr = String(obj.knowledge_parts).split(";");
				for (competenceId in competenceArr) {
					competenceQuery = ArrayOptFirstElem(XQuery("for $elem in knowledge_parts where $elem/id = " + XQueryLiteral(Int(competenceId))+ " return $elem"));
					competenceAll.push({
						id: Int(competenceId),
						name: String(competenceQuery.name)
					});
				}
				result.objs.push({
					id: Int(obj.id),
					name: String(obj.name),
					organization: String(userObj.org_name),
					date: StrDate(obj.create_date, false),
					competenceNames: ArrayMerge(competenceAll, "This.name", ", ")
				});
			}
		}
		result.competences = [];
		var competencesQuery = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = " + XQueryLiteral(6753954396609189672)+ " return $elem");
		for (competenceObj in competencesQuery) {
			result.competences.push({
				id: Int(competenceObj.id),
				name: String(competenceObj.name)
			});
		}
		return result;
	}

	function getObjsCopp () {	//Общий GET для цоповца
		var result = {};
		result.objs = [];
		var competence = [];
		var competenceAll = [];
		var vacanciesOrgs = XQuery("for $elem in documents where $elem/parent_document_id = 6755889390303450216 return $elem");
		for (vacancyOrg in vacanciesOrgs) {
			objsQuery = XQuery("for $elem in documents where $elem/parent_document_id = " + XQueryLiteral(Int(vacancyOrg.id))+ " return $elem");
			for (obj in objsQuery) {
				competenceAll = [];
				competenceArr = String(obj.knowledge_parts).split(";");
				for (competenceId in competenceArr) {
					competenceQuery = ArrayOptFirstElem(XQuery("for $elem in knowledge_parts where $elem/id = " + XQueryLiteral(Int(competenceId))+ " return $elem"));
					if (competenceQuery || competenceQuery!=undefined) {
						competenceAll.push({
							id: Int(competenceId),
							name: String(competenceQuery.name)
						});
					}
				}
				result.objs.push({
					id: Int(obj.id),
					name: String(obj.name),
					organization: String(vacancyOrg.name),
					date: StrDate(obj.create_date, false),
					competenceNames: ArrayMerge(competenceAll, "This.name", ", ")
				});
			}
	
		}
		result.competences = [];
		var competencesQuery = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = " + XQueryLiteral(6753954396609189672)+ " return $elem");
		for (competenceObj in competencesQuery) {
			result.competences.push({
				id: Int(competenceObj.id),
				name: String(competenceObj.name)
			});
		}
		return result;
	}

	function getImg(id) {
	  fileExist = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = " + XQueryLiteral(id) + " return $elem")) != undefined;
	  if (fileExist) {
	    imgDoc = OpenDoc(UrlFromDocID(Int(id))).TopElem;
	    obj = {};
	    obj.id = String(id);
	    obj.name = String(imgDoc.name);
	    obj.url = "/download_file.html?file_id=" + String(id);
	    return obj;
	  }
	}


	function openObj (obj_id) {

		var result = {};
		var teObjDoc = OpenDoc(UrlFromDocID(Int(obj_id))).TopElem;

		result.id = Int(teObjDoc.id);
		result.name = String(teObjDoc.name);
		result.small_desc = String(teObjDoc.comment);
		result.description = String(teObjDoc.text_area);
		result.competence = [];
		result.image = String(teObjDoc.resource_id)!='' ? getImg(Int(teObjDoc.resource_id)) : [];	//если объект то говно
		for (knowledge_part in teObjDoc.knowledge_parts) {
			result.competence.push({id: String(knowledge_part.knowledge_part_id), name: String(knowledge_part.knowledge_part_name)});
		}
		result.candidates = [];
		allCollabs = XQuery("for $elem in collaborators where $elem/knowledge_parts != '' return $elem");
		for (collab in allCollabs) {
			userClassifier = [];
			intersectArray = [];
			queryKnowledge = [];
			userFiles = [];

			queryKnowledge = String(collab.knowledge_parts).split(";");
			intersectArray = ArrayIntersect(result.competence, queryKnowledge, 'id', 'This');
			if (ArrayCount(intersectArray)) {

				collabDoc = OpenDoc(UrlFromDocID(Int(collab.id))).TopElem;
				if (collabDoc.custom_elems.ChildByKeyExists("portfolio") && (String(collabDoc.custom_elems.GetChildByKey("portfolio").value) == "true") ) {
					for (knowledge_part in collabDoc.knowledge_parts) {
						userClassifier.push({
							"id": Int(knowledge_part.knowledge_part_id), 
							"name": String(knowledge_part.knowledge_part_name)
						})
					}
					if (collabDoc.files.ChildExists("file")) {
						for (file in collabDoc.files) {
							file_doc = OpenDoc(UrlFromDocID(Int(file.file_id))).TopElem;
							userFiles.push({
								"id": Int(file_doc.id), 
								"comment": String(file_doc.comment),
								"url": "/download_file.html?file_id="+String(file_doc.id)
							})
						}
					}
					result.candidates.push({
						"id": String(collabDoc.id),
						"fullname": String(collab.fullname),
						"sex": getSex(String(collabDoc.sex)),
						"birthdate": StrXmlDate(collabDoc.birth_date),
						"email": String(collabDoc.email),
						"about": String(collabDoc.comment),
						"pict_url": String(collabDoc.pict_url),
						"userclassifier": userClassifier,
						"files": userFiles,
					})
				}

			}

		}
		
		return result;

	}


	function createOrModifyObj (data) {

		var new_doc;

		if (data.HasProperty('id')) {

		    new_doc = OpenDoc(UrlFromDocID(Int(data.id)));

		} else {
			new_doc = tools.new_doc_by_name('document');
			new_doc.BindToDb(DefaultDb);
			new_doc.Save();
			userOrgId = Int(ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/id = "+XQueryLiteral(curUserID)+" return $elem")).org_id);
			var orgVacancy = ArrayOptFirstElem(XQuery("for $elem in documents where $elem/code = "+XQueryLiteral(String(userOrgId))+" return $elem"));
			if (orgVacancy != undefined) {
				var orgVacancyId = Int(orgVacancy.id);
			} else {
				new_doc_org = tools.new_doc_by_name('document');
				new_doc_org.BindToDb(DefaultDb);
				new_doc_org.Save();
				new_doc_org.TopElem.parent_document_id = Int(6755889390303450216);
				new_doc_org.TopElem.code = userOrgId;
				new_doc_org.TopElem.name = String(ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id = "+XQueryLiteral(userOrgId)+" return $elem")).disp_name);
				new_doc_org.Save();
				var orgVacancyId = Int(new_doc_org.TopElem.id);
			}
			new_doc.TopElem.parent_document_id = orgVacancyId;
		}

		new_doc.TopElem.name = String(data.name);
		new_doc.TopElem.comment = String(data.small_desc);
		new_doc.TopElem.text_area = String(data.description);
		new_doc.TopElem.knowledge_parts.Clear(); //Удаляем старые
		for (var i = 0; i<ArrayCount(data.competence); i++) {
			//knowledgePartID = Int(data.competence[i].id);
			newcompetence = new_doc.TopElem.knowledge_parts.AddChild(); //Добавляем новую
			newcompetence.knowledge_part_id = Int(data.competence[i].id);
			//knowledgePartName = String(ArrayOptFirstElem(XQuery("for $elem in knowledge_parts where $elem/id = " + XQueryLiteral(Int(knowledgePartID))+ " return $elem")).name);
			newcompetence.knowledge_part_name = String(data.competence[i].name);
		}

		new_doc.Save();

		return new_doc;

	}   
	    
	function deleteObj (id) {

		DeleteDoc(UrlFromDocID(Int(id)));					

	}

	var response = {};

	try {       

		//  проверяем роль человека
		var curRole = XQuery("for $elem in group_collaborators where $elem/code='copp' and $elem/collaborator_id="+XQueryLiteral(curUserID)+" return $elem/group_id");
		if (ArrayCount(curRole) > 1) { curRole = curRole.join(','); } else { curRole = String(curRole[0]); }
		curRole = StrContains(curRole, "6748398834638268528") ? "copp" : StrContains(curRole, "6761406065601834022") ? "employer" : undefined;


		if(Request.Method == "GET") {

		    if (Request.Query.HasProperty('id')) {
			response.objInfo = openObj(Request.Query.id);		           
		    } else {
			if (Int(curUserID) == 6748398834638268527) { //если методист цопп
				response.data = getObjsCopp();
			} else {
				response.data = getObjs();
			}
		    }

		} else if (Request.Method == "POST") {

		    var objInfo = tools.read_object(Request.Body);
		    if (Request.Query.HasProperty('delete')) {
			deleteObj(objInfo.id);
		    } else {
			if (String(ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/id = "+XQueryLiteral(curUserID)+" return $elem")).org_id) == "") {
				response.error = "Для размещения вакансии Вы должны состоять в какой-либо организации."; 
			} else {
				response.id = createOrModifyObj(objInfo).DocID;
			}
		    }
		}

	} catch (err) {response.error = String(err);}

	Response.Write(tools.object_to_text(response, 'json'));
%>