<%

var request_type =  Request.Method;
var response = {};

function getStringStatus(status_id) {

	switch (status_id) {
		case 'plan':						
			return 'Планируется';
		break;
		case 'active':							
			return 'Проводится';
		case 'cancel':							
			return 'Отменено';
		break;
		case 'close':						
			return 'Завершено';
		break;
		default:
			return 'Планируется';
	}
}

function get_knowledge_parts(code_name) {
	var knowledge_classifier = ArrayOptFirstElem(XQuery("for $elem in knowledge_classifiers where $elem/code = '" + code_name + "' return $elem"))
	var knowledge_classifier_id = knowledge_classifier.id
	var knowledge_parts = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = " + knowledge_classifier_id + " return $elem")
	return knowledge_parts
}

function getEvents () {

	var events_array = [];


	if (isLector()) {
		var lector_id = OptInt(ArrayOptFirstElem(XQuery("for $elem in lectors where $elem/person_id = "+OptInt(curUserID)+" return $elem")).id);
		response.debug_lector_id = lector_id;
		var collab_events =  XQuery("for $elem in event_lectors where $elem/lector_id = "+XQueryLiteral(lector_id)+" and $elem/type_id = 'compound_program' order by $elem/name return $elem");
		response.debug = ArrayCount(collab_events) ;
	} else {
		var collab_events = XQuery("for $elem in event_collaborators where $elem/collaborator_id = "+curUserID+" and $elem/type_id = 'compound_program' order by $elem/name return $elem");
	}
	for (event in collab_events) {
		try {
		events_array.push({
			id: Int(event.event_id), 
			name: String(event.name), 
			type: getProgramtype(event.event_id.ForeignElem.compound_program_id),
			progName: String(event.event_id.ForeignElem.compound_program_id.ForeignElem.name),
			status: getStringStatus(String(event.status_id)), 
			startDate: String(event.start_date).substr(0,10),
			finishDate: String(event.finish_date).substr(0,10)
		});
		} catch(err) { response.error = String(err) }
	}
	return events_array;
}

function getProgramtype(compoundProgramId) {	//Получить тип программы (true - очная, false - заочная)
	var result = true;
	var compoundProgramDoc = OpenDoc(UrlFromDocID(Int(compoundProgramId))).TopElem;
	var event_form = String(compoundProgramDoc.custom_elems.ObtainChildByKey('event_form').value);
	result = (event_form=='Очная'? true: false);
	return result;
}

function getElements(teCourse, module_id) {
	newParts = []

	for (part in teCourse.parts) {
		// newElem = tools.new_doc_by_name('course').TopElem.parts.AddChild()
		// newElem.AssignElem(part)
		newElem = {
			"id": String(part.code),
			"name": String(part.name),
			"code": String(part.code),
			"object_id": String(part.object_id),
			"type": String(part.type)
		}
		if (module_id) {
			newElem.module_id = String(module_id)
			newElem.course_id = String(teCourse.course_id)
			newElem.score = String(part.score)
			newElem.max_score = String(part.max_score)
			newElem.state_id = String(part.state_id)
			newElem.type = String(part.type)
			newElem.form_evaluation = "Автоматически"

			if (part.type == 'learning_task') {
				newElem.attempts = []
				newElem.form_evaluation = "Вручную"
				newElem.max_score = String(1)
				task_results = XQuery("for $elem in learning_task_results where $elem/learning_task_id = " + part.object_id + " and $elem/person_id = " + teCourse.person_id + " return $elem/id")
				task_results = ArrayCount(task_results) ? task_results : null
				if (task_results != null) {
					result = {}
					teTaskResult = tools.open_doc(task_results[ArrayCount(task_results) - 1]).TopElem
					result.answer = String(teTaskResult.answer)
					result.start_date = String( teTaskResult.start_date )
					result.finish_date = String( teTaskResult.finish_date ? teTaskResult.finish_date : '-' )
					result.comment = String(teTaskResult.comment)
					result.score = String(teTaskResult.mark)
					result.max_score = String(1)
					result.files = []
					for (file in teTaskResult.files) {
						_Res = ArrayOptFirstElem( XQuery("for $elem in resources where $elem/id = " + file.file_id + " return $elem") )
						teRes = tools.open_doc(file.file_id) != undefined ? tools.open_doc(file.file_id).TopElem : null
						if (teRes == null) continue
						result.files.push({
							"id": String(teRes.id),
							"name": String(teRes.name),
							"type": StrLowerCase( UrlPathSuffix( teRes.file_url ) ),
							"size": String(teRes.size),
							"url": StrReplace(teRes.file_url, 'x-local://wt/web/', ''),
							"lastModifiedDate": String(_Res.modification_date)
						})
					}
					newElem.state_id = String( teTaskResult.status_id == 'success' ? 4 : teTaskResult.status_id == 'evaluate' ? 2 : 1 )
					newElem.attempts.push(tools.object_to_text(result, 'json')) // Превышена максимальная вложенность функции tools.object_to_text равная 11 ступеням
				}
			}
			
                        else   if (part.type == 'lesson') {
				    findCourse = undefined;
                                    assignedElCourse =  ArrayOptFirstElem(XQuery("for $elem in courses where $elem/base_url = "+XQueryLiteral(course_moduleDocUrl)+" return $elem"));
                                    if (assignedElCourse != undefined) {
                                        findCourse =  ArrayOptFirstElem(XQuery("for $elem in learnings where $elem/course_id = "+assignedElCourse.id+" and $elem/person_id = "+teCourse.person_id+" order by $elem/start_usage_date descending return $elem"));
                                        if(findCourse != undefined) {
                                            //res_elem.link = "/view_doc.html?mode=learning_proc&doc_id=&object_id="+findCourse.id+"&course_id="+assignedElCourse.id+"&sid="+tools.get_sum_sid( assignedElCourse.id );
                                        } else {
                                            findCourse = ArrayOptFirstElem(XQuery("for $elem in active_learnings where $elem/course_id = "+assignedElCourse.id+" and $elem/person_id = "+teCourse.person_id+" order by $elem/start_usage_date descending return $elem"));
                                            if(findCourse != undefined) {
                                               // res_elem.link = "/course_launch.html?course_id="+assignedElCourse.id+"&object_id="+findCourse.id+"&event_id="+event_id+"&sid="+tools.get_sum_sid(assignedElCourse.id );
                                            }
                                            else {
												tools.activate_course_to_person(curUserID, assignedElCourse.id, event_id);
                                                findCourse = ArrayOptFirstElem(XQuery("for $elem in active_learnings where $elem/course_id = "+assignedElCourse.id+" and $elem/person_id = "+teCourse.person_id+" order by $elem/start_usage_date descending return $elem"));
                                                if(findCourse != undefined) {
													setCourseInfo(findCourse)
                                                   // res_elem.link = "/course_launch.html?course_id="+assignedElCourse.id+"&object_id="+findCourse.id+"&sid="+tools.get_sum_sid(assignedElCourse.id );
                                                }
                                                
                                            }
                                        }
                                    } else {
                                         //res_elem.link = "/course_launch.html?course_id="+_class.object_id+"&object_id="+active_class.id+"&event_id="+event_id+"&sid="+tools.get_sum_sid( _class.object_id );
                                    }

			// Если найдена запись о курсе - берём статус и очки
				    if (findCourse != undefined) { 
					newElem.ccourse_id = String(findCourse.id); 
					newElem.state_id = String(findCourse.state_id); 
					newElem.score = String( findCourse.score > 0 ? (findCourse.score / 20) : 0 )
					newElem.result = String( findCourse.score )
				    }

                               } else if (String(part.type)=='test') {
                                    findTest = undefined;
                                    assignedTest =  ArrayOptFirstElem(XQuery("for $elem in assessments where $elem/id = "+XQueryLiteral(part.assessment_id)+" return $elem"));
                                    if (assignedTest != undefined) {
                                        findTest =  ArrayOptFirstElem(XQuery("for $elem in test_learnings where $elem/assessment_id = "+assignedTest.id+" and ($elem/person_id = "+teCourse.person_id+" and $elem/event_id = "+event_id+") order by $elem/start_usage_date descending return $elem"));
                                        if(findTest != undefined) {
                                            //res_elem.link = "/view_doc.html?mode=test_learning_stat&object_id="+findTest.id+"&sid="+tools.get_sum_sid( assignedTest.id );
                                        } else {
                                            findTest = ArrayOptFirstElem(XQuery("for $elem in active_test_learnings where $elem/assessment_id = "+assignedTest.id+" and ($elem/person_id = "+teCourse.person_id+" and $elem/event_id = "+event_id+") order by $elem/start_usage_date descending return $elem"));
                                            if(findTest != undefined) {
                                                //res_elem.link = "/test_launch.html?assessment_id="+assignedTest.id+"&object_id="+findTest.id+"&sid="+tools.get_sum_sid( assignedTest.id );
                                            }
                                            else {
                                                tools.activate_test_to_person(curUserID, assignedTest.id, event_id);
                                                findTest = ArrayOptFirstElem(XQuery("for $elem in active_test_learnings where $elem/assessment_id = "+assignedTest.id+" and $elem/person_id = "+teCourse.person_id+" order by $elem/start_usage_date descending return $elem")); //тут event еще не прописан
                                                if(findTest != undefined) {
                                                    //res_elem.link = "/test_launch.html?assessment_id="+assignedTest.id+"&object_id="+findTest.id+"&sid="+tools.get_sum_sid( assignedTest.id );
                                                }
                                            }
                                        }
                                    } else {
                                         //res_elem.link = "/course_launch.html?course_id="+_class.object_id+"&object_id="+active_class.id+"&event_id="+event_id+"&sid="+tools.get_sum_sid( _class.object_id  );
                                    }

			// Если найдена запись о курсе - берём статус и очки
				    if (findTest != undefined) { 
					newElem.ccourse_id = String(findTest.id); 
					newElem.state_id = String(findTest.state_id); 
					newElem.max_score = String(part.max_score);
					if (part.max_score == 1) 
						if (findTest.score >= 1) newElem.score = String(1)
						else newElem.score = String(0)
					else newElem.score = String( findTest.score > 0 && findTest.max_score > 0 ? (findTest.score / findTest.max_score) * 5 : 0 )
					newElem.result = String( findTest.score > 0 && findTest.max_score > 0 ? (findTest.score / findTest.max_score) * 100 : 0 )
					newElem._max_score = String(findTest.max_score)
					newElem._score = String(findTest.score)
				    }
			}

		}
		newParts.push( newElem )
	}

	return newParts
}

function getProgram(cmpID) {
	try {
	var structure = []
	var programDoc = tools.open_doc(cmpID)
	var teProgramDoc = programDoc != undefined ? programDoc.TopElem : null
	if (teProgramDoc == null) return structure

	for (program in teProgramDoc.programs) {
		newProg = {
			"id": String(program.education_program_id),
			"name": String(program.name),
			"courses": []
		}
		// newProg = tools.new_doc_by_name('compound_program').TopElem.programs.AddChild() // создать новую копию объекта
		// newProg.AssignElem(program) // полное слияние объекта
		// tools.assign_elems( newProg, program, ["id", "name", "object_id", "cost", "cost_type", "duration"] ) // функция сливает два массива объектов по указанным ключам
		// tools.assign_elems_exclude( newProg, program, ["custom_elems"] )

		teModule = tools.open_doc(newProg.id) != undefined ? tools.open_doc(newProg.id).TopElem : null
		if (teModule == null) continue

		for (method in teModule.education_methods) {
			teMethod = tools.open_doc(method.education_method_id) != undefined ? tools.open_doc(method.education_method_id).TopElem : null
			if (teMethod == null) continue

			teCourse = tools.open_doc(teMethod.course_id) != undefined ? tools.open_doc(teMethod.course_id).TopElem : null
			if (teCourse == null) continue

			newCourse = {
				"id": String(teCourse.id),
				"module_id": String(teMethod.id),
				"name": String(teCourse.name),
				"elements": getElements(teCourse)
			}

			newProg.courses.push(newCourse)
		}

		structure.push(newProg)
	}

	return structure
	} catch (err) {
		response.error = String(err)
		return []
	}
}

function getScores(plan_id) {	//получить оценки по плану
	try {
	var result = []
	var _plan = ArrayOptFirstElem( XQuery("for $elem in education_plans where $elem/id = " + plan_id + " return $elem") )
	var person_id = _plan.person_id
	var group_id = _plan.group_id
	var event_id = _plan.event_id
	var compound_program_id = _plan.compound_program_id

	var form_education = get_knowledge_parts('form_education')
	var course_type = get_knowledge_parts('course_type')
	var form_evaluation = get_knowledge_parts('form_evaluation')
	var type_evaluation = get_knowledge_parts('type_evaluation')

	tePlan = tools.open_doc(_plan.id) != undefined ? tools.open_doc(_plan.id).TopElem : null
	teCMP = tools.open_doc(compound_program_id) != undefined ? tools.open_doc(compound_program_id).TopElem : null

	person = []

	for (program in teCMP.programs) {

		planChild = ArraySelect(tePlan.programs, "parent_progpam_id == null")[program.ChildIndex]
		module = {
			"id": String(program.education_program_id),
			"real_name": String(program.name),
			"name": String(planChild.name),
			"score": String(planChild.mark != '' ? planChild.mark : 0),
			"state_id": String(planChild.state_id),
			"type": "module",
			"courses": []
		}

		teModule = tools.open_doc(module.id) != undefined ? tools.open_doc(module.id).TopElem : null
		if (teModule == null) continue
		
		for (method in teModule.education_methods) {

			teMethod = tools.open_doc(method.education_method_id) != undefined ? tools.open_doc(method.education_method_id).TopElem : null
			teCourse = tools.open_doc(teMethod.course_id) != undefined ? tools.open_doc(teMethod.course_id).TopElem : null

            form = ArrayOptFirstElem( ArrayIntersect(form_education, teCourse.knowledge_parts, "id", "knowledge_part_id") )
            form = DataType(form) == "object" && form != null ? String(form.name) : 'Очное'
            type = ArrayOptFirstElem( ArrayIntersect(course_type, teCourse.knowledge_parts, "id", "knowledge_part_id") )
			type = DataType(type) == "object" && type != null ? String(type.name) : 'Лекция'
            form_eval = ArrayOptFirstElem( ArrayIntersect(form_evaluation, teCourse.knowledge_parts, "id", "knowledge_part_id") )
			form_eval = DataType(form_eval) == "object" && form_eval != null ? String(form_eval.name) : 'Автоматически'
            type_eval = ArrayOptFirstElem( ArrayIntersect(type_evaluation, teCourse.knowledge_parts, "id", "knowledge_part_id") )
			type_eval = DataType(type_eval) == "object" && type_eval != null ? String(type_eval.name) : 'Зачет'
			
			courseChild = ArrayOptFind(tePlan.programs, "education_method_id == " + method.education_method_id)
			plan_course = ArrayUnion(
				XQuery("for $elem in active_learnings where $elem/person_id = " + person_id + " and $elem/course_id = " + teCourse.id + " return $elem"),
				XQuery("for $elem in learnings where $elem/person_id = " + person_id + " and $elem/course_id = " + teCourse.id + " return $elem")
			) // TODO: Также необходимо искать по группе и мероприятию, иначе могут возникнуть конфликты в будущем

			teCoursePlan = ArrayCount(plan_course) ? tools.open_doc(plan_course[0].id).TopElem : []
			courseElem = {
				"id": String(teCourse.id),
				"real_name": String(teCourse.name),
				"name": String(courseChild.name),
				"score": String(courseChild.mark),
				"state_id": String(courseChild.state_id),
				"type": "course",
				"form_education": form,
				"course_type": type,
				"form_evaluation": form_eval,
				"type_evalution": type_eval,
				"elements" : getElements(teCoursePlan, module.id)
			}

			module.courses.push(courseElem)
		}

		person.push(module)
	}
	return person

} catch(err) { response.progress = String(err) }
}

function getJournalListeners(event_id) {
	var result = []
	if (isLector()) {
		var eventPlans = XQuery("for $elem in education_plans where $elem/event_id = " + event_id + " return $elem");
	} else {
		var eventPlans = XQuery("for $elem in education_plans where $elem/event_id = " + event_id + " and $elem/person_id = " + curUserID + " return $elem");
	}
	for (_plan in eventPlans) {
		result.push({
			id: Int(_plan.person_id),
			plan_id: Int(_plan.id),
			name: String(_plan.person_fullname),
			progress: getScores(_plan.id)
		});
	}
	return result;
}

function openEvent(event_id) {
	var result = {}
	var compoundProgramId = ArrayOptFirstElem( XQuery("for $elem in events where $elem/id = " + event_id + " return $elem") ).compound_program_id
	if (compoundProgramId != undefined && compoundProgramId != '') {
		result.structure = getProgram(compoundProgramId)
		result.listeners = getJournalListeners(event_id)
	} else {
		response.error = 'Не найдена модульная программа'
	}
	return result
}

function setScoreForElem(item) {
	var _edPlan = ArrayOptFirstElem( XQuery("for $elem in education_plans where $elem/id = " + item.plan_id + " return $elem") );
	var person_id = _edPlan.person_id;
	var compoundProgramId = _edPlan.compound_program_id;
	if (person_id != undefined && person_id!= '') {

		if (!getProgramtype(compoundProgramId)) { //заочная - оцениваем элементы курса
			var this_active_course = ArrayUnion(
				XQuery("for $elem in active_learnings where $elem/person_id = " + person_id + " and $elem/course_id = " + item.course_id + " return $elem"),
				XQuery("for $elem in learnings where $elem/person_id = " + person_id + " and $elem/course_id = " + item.course_id + " return $elem")
			) // TODO: Также необходимо искать по группе и мероприятию, иначе могут возникнуть конфликты в будущем

			if (this_active_course != undefined) {
				courseDoc = OpenDoc(UrlFromDocID(OptInt(this_active_course[0].id)));
				courseDocTE = courseDoc.TopElem;

				this_part =  ArrayOptFind(courseDocTE.parts, 'code == "' + item.code + '"')
				response.part = this_part

				if (this_part != undefined) {
					this_part.score = item.mark;
					if (item.type == "learning_task") {
						task_result = ArrayOptFirstElem( XQuery("for $elem in learning_task_results where $elem/learning_task_id = " + this_part.object_id + " and $elem/person_id = " + person_id + " return $elem/id") ) // and $elem/status_id = 'evaluation'
						task_doc = tools.open_doc(task_result)
						task_doc.TopElem.comment = item.comment
						task_doc.TopElem.status_id = 'success'
						task_doc.TopElem.mark = item.mark
						task_doc.TopElem.finish_date = Date()
						task_doc.Save()
					}
					courseDoc.Save();
				} else {
					response.error = 'Элемент занятия не найден'; return
				}

			} else {
				response.error = 'Занятие не найдено'; return
			}
		} else {	//очная - оцениваем получаем курсы в плане
			_planDoc = tools.open_doc(item.plan_id)
			_planDocTe = _planDoc != undefined ? _planDoc.TopElem : null

			_plan_course = ArrayFind( ArraySelect(_planDocTe.programs, "This.type == 'course'" ), "This.object_id == " + String(item.id) )

			if (_plan_course != undefined) {
				_plan_course.mark = item.mark
				_planDoc.Save();
			} else {
				response.error = 'Занятие не найдено'; return
			}
		
		}

	} else {
		response.error = 'Не найден план'; return
	}
}

function isLector() {
	return ArrayOptFirstElem(XQuery("for $elem in lectors where $elem/person_id = "+OptInt(curUserID)+" return $elem"))!=undefined;
}

/*### Функции - конец###*/

/*### Обработка запроса - начало###*/
try {
	response.query = Request.Query;
	if (request_type === 'GET') {
			if(Request.Query.HasProperty("event_id")) {	//Получение информации по одному мероприятию
				response.data = openEvent(Request.Query.event_id)
			}  else {									//Получение списка всех мероприятий
				response.events = getEvents();
			}
			response.isLector = isLector();
	} else if (request_type === 'POST') {	//выставление оценки
		var newScoreData = tools.read_object(Request.Body);
		if (newScoreData.HasProperty('score')&&newScoreData.HasProperty('object_id')&&newScoreData.HasProperty('plan_id')&&newScoreData.HasProperty('course_id')&&String(newScoreData.score)!=''&&String(newScoreData.plan_id)!=''&&String(newScoreData.course_id)!='') {
			setScoreForElem(newScoreData);
		} else {
			response.error = 'Неверные значения отправляемых данных';
		}
	}
	else {
		response.error = 'Ошибка запроса';
	}
} catch (e) {	
	response.error = String(e);
}

Response.Write(tools.object_to_text(response, 'json'));
/*### Обработка запроса - конец###*/
%>