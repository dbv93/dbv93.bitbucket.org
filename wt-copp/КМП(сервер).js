<%
function ExtractKeysFromArray (array, keys) {
	if (!IsArray(array) || !IsArray(keys)) return []
	var destArray = new Array
	for (item in array) {
		if (!DataType( item ) == "object") continue
		elem = {}
		for (key in keys) {
			newKey = item.GetOptProperty(key, "key not found in object")
			elem[key] = DataType( newKey ) == "object" && newKey !== null || IsArray(newKey) ? newKey : String(newKey)
		}
		destArray.push(elem)
	}
	return destArray
}

function get_knowledge_parts (code_name) {
	var knowledge_classifier = ArrayOptFirstElem(XQuery("for $elem in knowledge_classifiers where $elem/code = '" + code_name + "' order by $elem/name ascending return $elem"))
	var knowledge_classifier_id = knowledge_classifier.id
	var knowledge_parts_arr = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = " + knowledge_classifier_id + " order by $elem/name ascending return $elem")
	var knowledge_parts = []
	for(part in knowledge_parts_arr) {
		knowledge_parts.push({
			id: String(part.id),
			name: String(part.name),
			code: String(part.code)
		})
	}
	return knowledge_parts
}

function getFile (id, org_id) {
	try {
		org = org_id ? org_id : ""
		_resources = ArrayOptFirstElem( XQuery("for $elem in resources where $elem/id = " + id + " and contains($elem/file_path, 'org_" + org + "') return $elem") )
		if (_resources == undefined) return
		_Res = tools.open_doc(_resources.id)
		teRes = _Res.TopElem
		return {
			"id": String(teRes.id),
			"name": String(teRes.name),
			"type": StrLowerCase( UrlPathSuffix( teRes.file_url ) ),
			"size": String(teRes.size),
			"url": StrReplace(teRes.file_url, 'x-local://wt/web/', ''),
			"lastModifiedDate": String(_resources.modification_date)
		}
	} catch (err) {
		response.error = String(err)
	}
}

function getResources (org_id) {
// Этот кусочек кода показывает файлы в конкретной папке файловой системы
/*
	var arrRes = new Array()
	var sDirectoryPath = 'C:\\\\copp-volgograd\\WebTutorServer\\wt\\web\\upload\\org_' + org_id + '\\'
		arrFiles = ReadDirectoryByPath( sDirectoryPath )
		for (sFileName in arrFiles) {
			if(PathIsDirectory(sFileName)) continue
			sUID = StrReplace(sFileName, sDirectoryPath + "\\", "")
			oItem = new Object()
			oItem.name = FileName(sFileName)
			//oItem.size = StrLen(sFileName) // мб работает
			oItem.uid = sUID
			arrRes.push(oItem)
		}
	return arrRes
*/
	var arrResources = []
	_resources = XQuery("for $elem in resources where contains($elem/file_path, 'org_" + org_id + "') return $elem")
	for (res in _resources) {
		_Res = tools.open_doc(res.id)
		teRes = _Res.TopElem
		arrResources.push({
			"id": String(teRes.id),
			"name": String(teRes.name),
			"type": StrLowerCase( UrlPathSuffix( teRes.file_url ) ),
			"size": String(teRes.size),
			"url": StrReplace(teRes.file_url, 'x-local://wt/web/', ''),
			"lastModifiedDate": String(res.modification_date)
		})
	}
	return arrResources
	
}

function getPolls (org_id) {
	// return ArrayDirect(XQuery("for $elem in polls return $elem"))
	role_id = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = 'poll' and $elem/code = 'org_" + org_id + "' return $elem/id"))
	return role_id ? XQuery("for $elem in polls where MatchSome( $elem/role_id, (" + role_id + ") ) return $elem") : []
}

function getTasks (org_id) {
	role_id = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = 'learning_task' and $elem/code = 'org_" + org_id + "' return $elem/id"))
	arr = []
	Tasks = role_id ? XQuery("for $elem in learning_tasks where MatchSome( $elem/role_id, (" + role_id + ") ) return $elem") : []
	response.tasks = Tasks
	for (task in Tasks) {
		teDoc = tools.open_doc(task.id) != undefined ? tools.open_doc(task.id).TopElem : null
		if (teDoc != null) {
			files = []
			for (file in teDoc.files) {
				resource = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = " + XQueryLiteral(file.file_id) + " return $elem"))
				if (resource != null) {
					url = resource.file_path ? StrReplace(resource.file_path, 'x-local://wt/web/', '') : 'download_file.html?file_id=' + resource.id
					files.push({
						"id": String(resource.id),
						"name": String(resource.name),
						"type": StrLowerCase( UrlPathSuffix( resource.name ) ),
						"size": String(resource.size),
						"url": url,
						"lastModifiedDate": String(resource.modification_date)
					})
				}
			}
			arr.push({
				"id": Int(task.id),
				"name": String(task.name),
				"files": files,
				"desc": String(teDoc.desc)
			})
		}
	}
	return arr
}

function getTests (org_id) {
	role_id = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = 'assessment' and $elem/code = 'org_" + org_id + "' return $elem/id"))
	arr = []
	tests = ArrayDirect(XQuery("for $elem in assessments return $elem")) // TODO Убрать, когда будут создаваться тесты
	for (test in tests) {
		teDoc = tools.open_doc(test.id) != undefined ? tools.open_doc(test.id).TopElem : null
		arr.push({
			"id": Int(test.id),
			"name": String(test.title),
			"desc": String(teDoc != null ? teDoc.desc : ''),
			"files": [],
			"comment": String(teDoc != null ? teDoc.comment : '')
		})
	}
	return arr
	// return role_id ? XQuery("for $elem in assessments where MatchSome( $elem/role_id, (" + role_id + ") ) return $elem") : []
}

function getModules (org_id) {
	role_id = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = 'education_program' and $elem/code = 'org_" + org_id + "' return $elem/id"))
	return role_id ? XQuery("for $elem in education_programs where MatchSome( $elem/role_id, (" + role_id + ") ) return $elem") : []
}

function getLongreads (org_id) {
	lgs = []
	parent_document_id = ArrayOptFirstElem(XQuery("for $elem in documents where $elem/code = 'org_" + org_id + "' return $elem/id"))
	arr = parent_document_id ? XQuery("for $elem in documents where $elem/parent_document_id = " + XQueryLiteral(parent_document_id) + " return $elem") : []
	for (lg in arr) {
		teDoc = tools.open_doc(lg.id) != undefined ? tools.open_doc(lg.id).TopElem : null
		lgs.push({
			"id": Int(lg.id),
			"name": String(lg.name),
			"desc": String(teDoc != null ? teDoc.text_area : ''),
			"files": [],
			"comment": String(teDoc != null ? teDoc.comment : '')
		})
	}
	return lgs
}

function getPrograms () {
	var result = []
	var programs = XQuery("for $elem in compound_programs order by $elem/name descending return $elem")
	for (prog in programs) {
		progDoc = tools.open_doc(prog.id)
		teProgDoc = progDoc.TopElem
		progRoles = tools.read_object(tools.object_to_text(teProgDoc.role_id, 'json')).GetOptProperty('role_id')
		moderatorID = progDoc.TopElem.custom_elems.ObtainChildByKey('cp_moderator').value
		result.push({
			id: OptInt(prog.id),
			name: String(prog.name),
			create_date: String(progDoc.TopElem.doc_info.creation.date),
			status: String(progDoc.TopElem.custom_elems.ObtainChildByKey('cp_status').value),
			cp_moderator: String(tools.open_doc(moderatorID) != undefined ? tools.open_doc(moderatorID).TopElem.fullname : ''),
			'roles': progRoles
		})
	}
	
	return result
}

function getProgramStructure (id) {
	var progDoc = tools.open_doc(id)
	var teProgDoc = progDoc.TopElem

	// var role = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = 'compound_program' and $elem/code = 'org_" + teOrg.id + "' return $elem/id"))
	structure = tools.read_object(teProgDoc.custom_elems.ObtainChildByKey('structure').value)
	program = {}
	program.id = String(teProgDoc.id)
	program.name = String(teProgDoc.name)
	program.dev_org = openDocCustomElems("dev_org")
	program.cp_moderator = openDocCustomElems("cp_moderator")
	program.cp_status = String(progDoc.TopElem.custom_elems.ObtainChildByKey('cp_status').value)
	program.test_program = structure.GetOptProperty('test_program', false)
	program.hidden = program.test_program ? true : String(progDoc.TopElem.custom_elems.ObtainChildByKey('hidden').value)
	program.educ_org = openDocCustomElems("educ_org")
	program.status_log = String(progDoc.TopElem.custom_elems.ObtainChildByKey('status_log').value)
	program.competence = getKnowledgeParts('competence', teProgDoc.knowledge_parts, true)
	program.profession = getKnowledgeParts('profession', teProgDoc.knowledge_parts, true)
	program.issued_document = getKnowledgeParts('issued_documents', teProgDoc.knowledge_parts)
	program.form_education = getKnowledgeParts('form_education', teProgDoc.knowledge_parts)
	program.listeners_category = getKnowledgeParts('listeners_category', teProgDoc.knowledge_parts)
	program.knowledge_level_cl = getKnowledgeParts('knowledge_level_cl', teProgDoc.knowledge_parts)
	program.cp_extra_classification_cl = getKnowledgeParts('cp_extra_classification_cl', teProgDoc.knowledge_parts)
	program.desc = String(teProgDoc.desc)
	program.image = tools.open_doc(teProgDoc.resource_id) != undefined && program.dev_org != '' ? getFile(teProgDoc.resource_id, program.dev_org.id) : null
	program.regulations = structure.GetOptProperty('regulations', []) // TODO
	program.results = structure.GetOptProperty('results', []) // TODO
	program.persons = structure.GetOptProperty('persons', []) // TODO
	program.resources = structure.GetOptProperty('resources', {}) // TODO
	program.program = getStructure(teProgDoc) // TODO
	program.duration = ArraySum(program.program, 'duration')
	program.cost = ArraySum(program.program, 'cost')
	// program.additional_info = progDoc.TopElem.custom_elems.ObtainChildByKey('additional_info').value
	

	response.lists = {}
	response.lists.resources = program.dev_org != '' ? getResources(program.dev_org.id) : []
	response.lists.polls = program.dev_org != '' ? getPolls(program.dev_org.id) : []
	response.lists.learning_tasks = program.dev_org != '' ? getTasks(program.dev_org.id) : []
	response.lists.tests = program.dev_org != '' ? getTests(program.dev_org.id) : []
	response.lists.modules = program.dev_org != '' ? getModules(program.dev_org.id) : [] // TODO сделать, чтобы можно было брать модули только из утверждённых программ без возможности редактирования или выполнять копирование (полное?)
	response.lists.longreads = program.dev_org != '' ? getLongreads(program.dev_org.id) : []

	return program
}

function openDocCustomElems (value) {
	doc = tools.open_doc(teProgDoc.custom_elems.ObtainChildByKey(value).value)
	return doc != undefined ? doc.TopElem : ''
}

function getKnowledgeParts (cat, parts, isArray) {
	var obj = null
	var _parts = ArrayMerge(parts, 'This.PrimaryKey', ', ')
	var classifier_id = ArrayOptFirstElem(XQuery("for $elem in knowledge_classifiers where $elem/code = " + XQueryLiteral(cat) + " return $elem/id"))
	obj = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = " + OptInt(classifier_id) + " and contains(" + XQueryLiteral(_parts) + ", $elem/id) return $elem")
	if (!isArray) obj = ArrayOptFirstElem(obj)
	return obj
}

function getProgramResults () {
	obj = {
		profession: '',
		knowledges: [],
		skills: [],
		capability: []
	}
	return obj
}

function getProgramResources () {
	obj = {
		technical: [],
		education: []
	}
	return obj
}

function getStructure (teProgDoc) {
	var module = {}
	var item = {}
	var element = {}
	var resource = {}
	var arr = [
		// {
		//   id: '_1',
		//   name: 'Модуль 1',
		//   items: [],
		//   cost: 0,
		//   duration: 0,
		//   hidden: false
		// }
	]
	if (ArrayCount(teProgDoc.programs)) { arr = [] }
	for (part in teProgDoc.programs) {
		module = {}
		module.id = String(part.education_program_id)
		module.name = String(part.name)
		module.cost = String(part.cost) != '' ? OptInt(part.cost) : 0
		module.cost_type = String(part.cost_type)
		module.duration = String(part.duration) != '' ? OptInt(part.duration) : 0
		module.type = String(part.type)
		module.items = []
		arr.push(module)
		teModuleDoc = tools.open_doc(module.id) != undefined ? tools.open_doc(module.id).TopElem : null
		if (teModuleDoc != null) {
			for (met in teModuleDoc.education_methods) {
				item = {}
				teEduMet = tools.open_doc(met.education_method_id) != undefined ? tools.open_doc(met.education_method_id).TopElem : null
				if (teEduMet != null) {
					teCourseDoc = tools.open_doc(teEduMet.course_id) != undefined ? tools.open_doc(teEduMet.course_id).TopElem : null
					if (teCourseDoc != null) {
						item.id = String(teEduMet.id)
						item.course_id = String(teCourseDoc.id)
						item.name = String(teCourseDoc.name)
						item.cost = OptInt(teEduMet.cost)
						item.currency = String(teEduMet.currency)
						item.cost_type = String(teEduMet.cost_type)
						item.duration = OptInt(teEduMet.duration)
						item.person_num = String(teEduMet.person_num)
						item.desc = String(teCourseDoc.desc)
						item.is_open = String(teEduMet.is_open)
						item.form_education = getKnowledgeParts('form_education', teCourseDoc.knowledge_parts)
						item.form_education = item.form_education != null ? item.form_education : ArrayOptFind(get_knowledge_parts('form_education'), 'code == "default"')
						item.form_evaluation = getKnowledgeParts('form_evaluation', teCourseDoc.knowledge_parts)
						item.type = getKnowledgeParts('course_type', teCourseDoc.knowledge_parts)
						item.type_evaluation = getKnowledgeParts('type_evaluation', teCourseDoc.knowledge_parts)
						item.elements = []
						if (ArrayCount(teCourseDoc.parts)) {
							for (elem in teCourseDoc.parts) {
								element = {}
								element.object = {}
								element.object.files = []
								element.id = String(elem.code)
								element.code = String(elem.code)
								element.name = String(elem.name)
								element.type = String(elem.type)
								element.desc = String(elem.desc)
								element.is_visible = String(elem.is_visible)
								if (elem.type == 'resource') {
									teDoc = tools.open_doc(elem.object_id) != undefined ? tools.open_doc(elem.object_id).TopElem : null
									resource = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = " + XQueryLiteral(elem.object_id) + " return $elem"))
									if (teDoc != null && resource != null) {
										url = resource.file_path ? StrReplace(resource.file_path, 'x-local://wt/web/', '') : 'download_file.html?file_id=' + resource.id
										element.object.files.push({
											"id": String(resource.id),
											"name": String(resource.name),
											"type": StrLowerCase( UrlPathSuffix( resource.name ) ),
											"size": String(resource.size),
											"url": url,
											"lastModifiedDate": String(resource.modification_date)
										})
										element.object.id = String(teDoc.id)
										element.object.name = String(teDoc.name)
										element.object.desc = String(teDoc.comment)
									}
								}
								if (elem.type == 'learning_task') {
									teDoc = tools.open_doc(elem.object_id) != undefined ? tools.open_doc(elem.object_id).TopElem : null
									if (teDoc != null) {
										for(file in teDoc.files) {
											teFile = tools.open_doc(file.file_id) != undefined ? tools.open_doc(file.file_id).TopElem : null
											resource = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = " + XQueryLiteral(file.file_id) + " return $elem"))
											if (teFile != null && resource != null) {
												url = resource.file_path ? StrReplace(resource.file_path, 'x-local://wt/web/', '') : 'download_file.html?file_id=' + resource.id
												element.object.files.push({
													"id": String(resource.id),
													"name": String(resource.name),
													"type": StrLowerCase( UrlPathSuffix( resource.name ) ),
													"size": String(resource.size),
													"url": url,
													"lastModifiedDate": String(resource.modification_date)
												})
											}
										}
										element.object.id = String(teDoc.id)
										element.object.name = String(teDoc.name)
										element.object.comment = String(teDoc.comment)
										element.object.desc = String(teDoc.desc)
									}
								}
								if (elem.type == 'test') {
									teDoc = tools.open_doc(elem.object_id) != undefined ? tools.open_doc(elem.object_id).TopElem : null
									if (teDoc != null) {
										element.object.desc = String(teDoc.desc)
										element.object.id = String(elem.object_id)
									}
								}
								if (elem.type == 'inline') {
									element.link = String(elem.url)
									element.object.desc = String(elem.desc) // убрать
								}
								if (elem.type == 'longread') {
									teDoc = tools.open_doc(elem.object_id) != undefined ? tools.open_doc(elem.object_id).TopElem : null
									element.link = String(elem.url)
									if (teDoc != null) {
										element.object.desc = String(teDoc.text_area)
										element.object.id = String(elem.object_id)
									}
								}
								// if (element.object.HasProperty("id")) item.elements.push(element)
								item.elements.push(element)
							}
						}
						module.items.push(item)
					}
				}
			}
		}
	}
	return arr
}

function getLists () {
	var lists = {}
	lists.form_education = get_knowledge_parts('form_education')
	lists.course_type = ArraySort ( get_knowledge_parts('course_type'), 'id', '+' )
	lists.listeners_category = get_knowledge_parts('listeners_category')
	lists.issued_documents = get_knowledge_parts('issued_documents') // в структуре есть категории, типа их надо убрать
	lists.knowledge_level_cl = get_knowledge_parts('knowledge_level_cl')
	lists.cp_extra_classification_cl = get_knowledge_parts('cp_extra_classification_cl')
	lists.competencies = ExtractKeysFromArray( XQuery("for $elem in knowledge_parts where $elem/parent_object_id = 6760201305451088462 order by $elem/name ascending return $elem"), ["id", "code", "name"] )
	lists.professions = ExtractKeysFromArray( XQuery("for $elem in knowledge_parts where $elem/parent_object_id = 6760201305451088461 order by $elem/name ascending return $elem"), ["id", "code", "name"] )
	lists.orgs = ExtractKeysFromArray( XQuery("for $elem in orgs order by $elem/name ascending return $elem"), ["id", "code", "name"] )
	lists.collaborators = ExtractKeysFromArray( XQuery("for $elem in collaborators order by $elem/name ascending return $elem"), ["id", "fullname"] )
	lists.education_programs = ExtractKeysFromArray( XQuery("for $elem in education_programs order by $elem/name ascending return $elem"), ["id", "code", "name"] )
	lists.form_evaluation = get_knowledge_parts('form_evaluation')
	lists.type_evaluation = get_knowledge_parts('type_evaluation')
	return lists
}

function addKnowledgePart (teDoc, obj) {
	if (obj == null || DataType(obj) != 'object') return
	kp = teDoc.knowledge_parts.AddChild()
	kp.knowledge_part_name = obj.name
	kp.knowledge_part_id = obj.id
}

function setProgramStructure (structure) {
	var progDoc = tools.open_doc(structure.id)
	var teProgDoc = progDoc.TopElem
	status = teProgDoc.custom_elems.ObtainChildByKey('cp_status').value
	if (status != 'Проект') { response.error = "Ошибка! Вы можете изменять программу только в режиме проекта."; return }
	teProgDoc.name = Trim(structure.name)
	if(structure.educ_org != '' && structure.educ_org.HasProperty("id")) { 
		teProgDoc.custom_elems.ObtainChildByKey('educ_org').value = String(structure.educ_org.id) 
	}
	teProgDoc.custom_elems.ObtainChildByKey('hidden').value = structure.GetOptProperty("test_program", false) ? true : String(structure.hidden)
	// teProgDoc.custom_elems.ObtainChildByKey('additional_info').value =
	// tools.object_to_text('', 'json')
	teProgDoc.desc = Trim(structure.desc);
	try { 
		teProgDoc.resource_id = structure.image != null ? structure.image.GetOptProperty("id", "") : ""
	} catch (err) {
		// Nichts!
	}
	
	// if(structure.dev_org.HasProperty("id") && structure.dev_org != '' && structure.dev_org.HasProperty("id")) { 
	// 	teProgDoc.custom_elems.ObtainChildByKey('dev_org').value = String(structure.dev_org.id) 
	// 	pasteElemToRole(structure.dev_org.id, 'compound_program', teProgDoc)
	// } else { teProgDoc.role_id.Clear() }
	teProgDoc.knowledge_parts.Clear()
	addKnowledgePart(teProgDoc, structure.cp_extra_classification_cl)
	addKnowledgePart(teProgDoc, structure.listeners_category)
	addKnowledgePart(teProgDoc, structure.issued_document)
	addKnowledgePart(teProgDoc, structure.knowledge_level_cl)
	for (item in structure.competence) addKnowledgePart(teProgDoc, item) // addKnowledgePart(ArrayOptFirstElem(XQuery("for $elem in knowledge_parts where $elem/id = " + item + " return $elem")))
	for (item in structure.profession) addKnowledgePart(teProgDoc, item) // addKnowledgePart(ArrayOptFirstElem(XQuery("for $elem in knowledge_parts where $elem/id = " + item + " return $elem")))

	if(ArrayCount(structure.program)) setProgramModules(structure)
	teProgDoc.custom_elems.ObtainChildByKey('structure').value = tools.object_to_text(structure, 'json')
	progDoc.Save()

	return getProgramStructure(structure.id)
}

function setProgramModules (structure) {
	var moduleChild = null
	var ModuleDoc = null
	var teModuleDoc = null
	var MethodDoc = null
	var teMethodDoc = null
	var CourseDoc = null
	var teCourseDoc = null
	teProgDoc.programs.Clear()

	for (module in structure.program) {
		ModuleDoc = tools.open_doc(module.id)
		teModuleDoc = ModuleDoc != undefined ? ModuleDoc.TopElem : null

		if (teModuleDoc == null) {
			ModuleDoc = tools.new_doc_by_name('education_program')
			ModuleDoc.BindToDb(DefaultDb)
			ModuleDoc.Save()
			teModuleDoc = ModuleDoc.TopElem

			module.id = ModuleDoc.DocID
		}
		
		teModuleDoc.education_methods.Clear()
		
		teModuleDoc.name = module.name
		teModuleDoc.code = "cmp_pr_" + teProgDoc.id
		if (structure.dev_org != '' && structure.dev_org.HasProperty("id")) pasteElemToRole(structure.dev_org.id, 'education_program', teModuleDoc)
		else teModuleDoc.role_id.Clear()
		// teModuleDoc.custom_elems.push( {"name": "status", "value": "Проект"} ) К сожалению в Учебных наборах нет кастомных полей о_О и документооборота... жэсть

		moduleChild = teProgDoc.programs.AddChild()
		newCode ++
		// moduleChild.code = newCode
		moduleChild.type = "education_program"
		moduleChild.name = module.name
		moduleChild.object_id = ModuleDoc.DocID
		moduleChild.education_program_id = ModuleDoc.DocID
		// moduleChild.cost = 0
		// moduleChild.cost_type = 'person'
		// moduleChild.duration = 0

		for (item in module.items) {
			MethodDoc = tools.open_doc(item.id)
			teMethodDoc = MethodDoc != undefined ? MethodDoc.TopElem : null

			if (teMethodDoc == null) {
				MethodDoc = tools.new_doc_by_name('education_method')
				MethodDoc.BindToDb(DefaultDb)
				MethodDoc.Save()
				teMethodDoc = MethodDoc.TopElem
			}

			methodChild = teModuleDoc.education_methods.AddChild()
			methodChild.education_method_id = MethodDoc.DocID

			teMethodDoc.name = item.name
			teMethodDoc.code = "em_" + MethodDoc.DocID

			if (structure.dev_org != '' && structure.dev_org.HasProperty("id")) pasteElemToRole(structure.dev_org.id, 'education_method', teMethodDoc)
			else teMethodDoc.role_id.Clear()

			CourseDoc = tools.open_doc(item.course_id)
			teCourseDoc = CourseDoc != undefined ? CourseDoc.TopElem : null

			if (teCourseDoc == null) {
	
				CourseDoc = tools.new_doc_by_name('course')
				CourseDoc.BindToDb(DefaultDb)
				CourseDoc.Save()
				teCourseDoc = CourseDoc.TopElem

			}

			teMethodDoc.course_id = CourseDoc.DocID
			teMethodDoc.type = "course"
			teCourseDoc.parts.Clear()

			teCourseDoc.name = item.name
			teCourseDoc.code = "cr_" + CourseDoc.DocID
			teCourseDoc.desc = item.desc
			teCourseDoc.knowledge_parts.Clear()
			addKnowledgePart(teCourseDoc, item.form_education)
			addKnowledgePart(teCourseDoc, item.form_evaluation)
			addKnowledgePart(teCourseDoc, item.type)
			addKnowledgePart(teCourseDoc, item.type_evaluation)

			if (structure.dev_org != '' && structure.dev_org.HasProperty("id")) pasteElemToRole(structure.dev_org.id, 'course', teCourseDoc)
			else teCourseDoc.role_id.Clear()

			for (i = 0; i < item.elements.length; i++) {
				elem = item.elements[i]
				pasteElemToCourse(CourseDoc, elem, i)
			}
			
			MethodDoc.Save()
			CourseDoc.Save()
		}
		if (ModuleDoc != null) ModuleDoc.Save()
	}

	teProgDoc.custom_elems.ObtainChildByKey('structure').value = tools.object_to_text(structure, 'json')

	return "success"
}

function pasteElemToCourse (doc, elem, i) {
	newChild = doc.TopElem.parts.AddChild()

	if (StrContains(elem.id, '_')) {
		newCode ++
		newChild.code = newCode // 'PART_' + (newChild.ChildIndex + 1)
	} else {
		newChild.code = elem.code
	}

	newChild.name = Trim(elem.name)
	newChild.desc = Trim(elem.desc)
	newChild.type = elem.type
	
	newChild.is_visible = true
	//newChild.set_status_side = 'course'
	//newChild.score_factor = 1
	//newChild.attempts_num = 1

	if (elem.type == 'resource') {
		newChild.object_id = elem.object.files[0].id
	}
	if (elem.type == 'learning_task') {
		newChild.object_id = elem.object.id
	}
	if (elem.type == 'inline') {
		newChild.url = Trim(elem.link)
	}
	if (elem.type == 'poll') {
		// newChild.object_id = elem.object.id
	}
	if (elem.type == 'longread') {
		newChild.object_id = elem.object.id
		newChild.url = "view_doc_short.html?doc_id=" + elem.object.id
	}
	if (elem.type == 'test') {
		newChild.assessment_id = elem.object.id
		newChild.object_id = elem.object.id
	}
}

function pasteElemToRole (org_id, catalog, teDoc) {
	teOrg = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id = " + org_id + " return $elem"))
	if(ArrayCount(XQuery("for $elem in roles where $elem/catalog_name = '" + catalog + "' and $elem/code = 'org_" + teOrg.id + "' return $elem"))) {
		roleID = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/catalog_name = '" + catalog + "' and $elem/code = 'org_" + teOrg.id + "' return $elem/id"))
		teDoc.role_id.Clear()
		teDoc.role_id.Add().Value = roleID
	} else {
		var newCat = OpenNewDoc('x-local://qti/qti_role.xmd')
		newCat.TopElem.name = teOrg.name
		newCat.TopElem.code = 'org_' + teOrg.id
		newCat.TopElem.catalog_name = catalog
		newCat.BindToDb(DefaultDb)
		newCat.Save()
		teDoc.role_id.Add().Value = newCat.DocID
	}
}

function pasteDocumentToRole (org_id, teDoc) {
	teOrg = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/id = " + org_id + " return $elem"))
	roleLongreadsID = ArrayOptFirstElem(XQuery("for $elem in documents where $elem/code = 'cmp_longreads' return $elem/id"))
	if(ArrayCount(XQuery("for $elem in documents where $elem/code = 'org_" + org_id + "' return $elem"))) {
		roleID = ArrayOptFirstElem(XQuery("for $elem in documents where $elem/code = 'org_" + org_id + "' return $elem/id"))
		teDoc.parent_document_id = roleID
	} else {
		var newCat = tools.new_doc_by_name('document')
		newCat.TopElem.name = teOrg.name
		newCat.TopElem.code = 'org_' + teOrg.id
		newCat.TopElem.parent_document_id = roleLongreadsID
		newCat.BindToDb(DefaultDb)
		newCat.Save()
		teDoc.parent_document_id = newCat.DocID
	}
}

function setStatus (id, status) {
	if (tools.open_doc(id) == undefined) {response.error = 'Не найден документ модульной программы ' + id + '. Обратитесь к администратору'; return }
	progDoc = tools.open_doc(id)
	if (progDoc.TopElem.custom_elems.ObtainChildByKey('cp_status').value == status.status)  {response.error = 'Программа уже имеет этот статус'; return }
	str = '/ Дата<splt> ' + Date() 
		+ '<brk> Кто менял<splt> ' + teUser.fullname 
		+ '<brk> Сменил со статуса<splt> ' + progDoc.TopElem.custom_elems.ObtainChildByKey('cp_status').value 
		+ '<brk> Поменял на статус<splt> ' +  status.status
		+ '<brk> Комментарий <splt> ' + status.comment

	progDoc.TopElem.custom_elems.ObtainChildByKey('status_log').value = 
	progDoc.TopElem.custom_elems.ObtainChildByKey('status_log').value + str
	progDoc.TopElem.custom_elems.ObtainChildByKey('cp_status').value = status.status

	progDoc.Save()
	return "success"
}

function createProgram (structure) {
	var progDoc = tools.new_doc_by_name('compound_program')
	progDoc.BindToDb(DefaultDb)
	var teProgDoc = progDoc.TopElem
	teProgDoc.code = "cmp_pr_" + progDoc.DocID
	teProgDoc.name = structure.name
	structure.id = progDoc.DocID
	if (structure.HasProperty("dev_org") && structure.dev_org != '' && structure.dev_org.HasProperty("id")) pasteElemToRole(structure.dev_org.id, "compound_program", teProgDoc)
	else { response.error = "Сначала укажите организацию разработчика"; return }
	teProgDoc.custom_elems.ObtainChildByKey('cp_moderator').value = structure.cp_moderator.id
	teProgDoc.custom_elems.ObtainChildByKey('dev_org').value = structure.dev_org.id
	teProgDoc.custom_elems.ObtainChildByKey('cp_status').value = "Проект"
	teProgDoc.custom_elems.ObtainChildByKey('structure').value = tools.object_to_text(structure, 'json')

	progDoc.Save()
	return { id: String(progDoc.DocID), structure: tools.object_to_text(structure, 'json') }
}

function createLearningTask (structure, org_id) {
	if (org_id == '' || org_id == undefined) { response.error = "Выберите организатора разработчика"; return }
	var newDoc = tools.new_doc_by_name('learning_task')
	newDoc.BindToDb(DefaultDb)
	var teNewDoc = newDoc.TopElem
	teNewDoc.code = 'lt_' + newDoc.DocID
	teNewDoc.name = Trim(structure.name)
	teNewDoc.desc = Trim(structure.desc)
	// teNewDoc.name = Trim(structure.object.name)
	// teNewDoc.desc = Trim(structure.object.desc)
	// teNewDoc.yourself_start = true
	for (file in structure.object.files) {
		newChild = teNewDoc.files.AddChild()
		newChild.file_id = file.id
	}
	pasteElemToRole(org_id, "learning_task", teNewDoc)
	newDoc.Save()

	return teNewDoc
}

function createLongread (structure, org_id) {
	if (org_id == '' || org_id == undefined) { response.error = "Выберите организатора разработчика"; return }
	var newDoc = tools.new_doc_by_name('document')
	newDoc.BindToDb(DefaultDb)
	var teNewDoc = newDoc.TopElem
	teNewDoc.code = 'lg_' + newDoc.DocID
	teNewDoc.name = Trim(structure.name)
	teNewDoc.text_area = Trim(structure.desc)
	// teNewDoc.name = Trim(structure.object.name)
	// teNewDoc.text_area = Trim(structure.object.desc)
	for (file in structure.object.files) {
		newChild = teNewDoc.files.AddChild()
		newChild.file_id = file.id
	}
	pasteDocumentToRole(org_id, teNewDoc)
	newDoc.Save()

	return teNewDoc
}

function createNewFiles (form) { // TODO
	arr = []
	files = form.HasProperty("files") ? form.GetProperty("files") : []
	response.files = IsArray(files) ? ArrayCount(files) : 'не массив'

	return "success"
}

function createNewFile (form) {
	try {
	var obj = {}
	object = tools.read_object(form.GetProperty("object"))
	if (!form.HasProperty("dev_org")) { response.error = "Сначала укажите организацию разработчика"; return }
	dev_org = form.GetProperty("dev_org")
	
	_data = form.GetProperty("file")
	xLocalFile = 'x-local://wt/web/upload/org_' + dev_org + '/'
	fileSourceID = ArrayOptFirstElem( XQuery( "for $elem in file_sources where $elem/code = 'file_system_upload' return $elem/id" ) )

	_bytes = StrLen( _data )
	_fileName = String(_data.FileName)
	dots = _fileName.split(".")
	_fileType =  dots[dots.length - 1]

	//if (Request.Query.HasProperty('scorm')) {
	if (_fileType != 'zip') {
	var fileDoc = tools.new_doc_by_name('resource')
	teFileDoc = fileDoc.TopElem
	fileDoc.BindToDb(DefaultDb)
	xLocalFile = xLocalFile + fileDoc.DocID + '.' + _fileType

	PutUrlData( xLocalFile, _data )

	teFileDoc.name = _fileName
	teFileDoc.size = _bytes
	teFileDoc.code = 'cp_' + fileDoc.DocID
	teFileDoc.type = 'file' // TODO types
	// teFileDoc.comment = object.desc // Добавить когда-нибудь комментарий
	teFileDoc.allow_download = 1
	teFileDoc.allow_unauthorized_download = 1
	teFileDoc.person_id = curUserID
	teFileDoc.file_source = fileSourceID
	teFileDoc.file_url = xLocalFile
	teFileDoc.file_path = xLocalFile
	pasteElemToRole(dev_org, "resource", teFileDoc)
	fileDoc.Save()

	// teFileDoc.custom_elems.ObtainChildByKey('doc_type').value = tools.object_to_text(options.doc_type, 'json')
	// teFileDoc.custom_elems.ObtainChildByKey('mark_condition').value = tools.object_to_text(options.mark_condition, 'json')
	// teFileDoc.custom_elems.ObtainChildByKey('mark_type').value = tools.object_to_text(options.mark_type, 'json')
	} else {
		
	}
	


	obj.id = OptInt(fileDoc.DocID)
	obj.name = String(_fileName)
	obj.url = StrReplace(xLocalFile, 'x-local://wt/web/', '')
	obj.size = OptInt(_bytes)
	obj.type = String(_fileType)

	return obj
	} catch(err) { response.error = String(err) }
}

function removeFile (fileID) { 
	
	return "success" 
}

try {
    var teUser = tools.open_doc(curUserID).TopElem
    var teOrg = teUser.org_id != '' ? tools.open_doc(teUser.org_id).TopElem : null
    var response = {}
	var additionalInfo = ['regulations', 'results', 'persons', 'resources'];
	var newCode = Int(DateToRawSeconds(CurDate) + StrInt(CurDateMilliseconds, 3))

    if (Request.Method == 'PUT') {
	// if (teUser.org_id == '') { response.error = "ошибка создания, пользователь должен состоять в организации" return }
		if (Request.Query.HasProperty('new')) {
			var structure = tools.read_object(Request.Body)	//Структура программы в формате JSON
			response.data = createProgram(structure)
		}
		if (Request.Query.HasProperty('org_id') && Request.Query.HasProperty('learning_task')) {
			var structure = tools.read_object(Request.Body)
			response.data = createLearningTask(structure, Request.Query.org_id)
		}
		if (Request.Query.HasProperty('org_id') && Request.Query.HasProperty('longread')) {
			var structure = tools.read_object(Request.Body)
			response.data = createLongread(structure, Request.Query.org_id)
		}
	}

    if (Request.Method == 'POST') {
		if (Request.Query.HasProperty('id')) {
			var structure = tools.read_object(Request.Body)	//Структура программы в формате JSON
			response.data = setProgramStructure(structure)
		}
		if (Request.Query.HasProperty('removefile')) response.data = removeFile(Request.Query.removefile)
		if (Request.Query.HasProperty('file')) response.data = createNewFile(Request.Form)
		if (Request.Query.HasProperty('files')) response.data = createNewFiles(Request.Form)
		if (Request.Query.HasProperty('status')) response.data = setStatus(Request.Query.status, tools.read_object(Request.Body))
    }

    if (Request.Method == 'GET') {
        if (Request.Query.HasProperty('id')) response.data = getProgramStructure(Request.Query.id)
        if (Request.Query.HasProperty('programs')) response.data = getPrograms()
        if (Request.Query.HasProperty('lists')) response.data = getLists()
    }

} catch (err) {response.error = String(err)}

Response.Write(tools.object_to_text(response, 'json'))
%>