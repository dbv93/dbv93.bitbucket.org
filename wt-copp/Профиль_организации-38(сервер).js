<%



function editSameEntityByCode(org_id, name) {
	var samePlace = ArrayOptFirstElem(XQuery("for $elem in places where $elem/code = "+XQueryLiteral(String(org_id))+" return $elem"));
	if (samePlace != undefined) {
		samePlaceDoc  = OpenDoc(UrlFromDocID(Int(samePlace.id)));
		samePlaceDoc.TopElem.name = String(name);
		samePlaceDoc.Save();
	}
	var sameEdOrg = ArrayOptFirstElem(XQuery("for $elem in education_orgs where $elem/code = "+XQueryLiteral(String(org_id))+" return $elem"));
	if (sameEdOrg != undefined) {
		sameEdOrgDoc  = OpenDoc(UrlFromDocID(Int(sameEdOrg.id)));
		sameEdOrgDoc.TopElem.name = String(name);
		sameEdOrgDoc.Save();
	}
	var sameFolder =  ArrayOptFirstElem(XQuery("for $elem in roles where $elem/code = 'org_"+String(org_id)+"' return $elem"));
	if (sameFolder != undefined) {
		sameFolderDoc  = OpenDoc(UrlFromDocID(Int(sameFolder.id)));
		sameFolderDoc.TopElem.name = String(name);
		sameFolderDoc.Save();
	}
}


function openCollaborator(collab_id) {

  var result = {};
  var teCollDoc = OpenDoc(UrlFromDocID(Int(collab_id))).TopElem;

  result.id = Int(teCollDoc.id);

  result.lastname = String(teCollDoc.lastname);
  result.firstname = String(teCollDoc.firstname);
  result.middlename = String(teCollDoc.middlename);

  result.sex = String(teCollDoc.sex);
  result.birth_date = String(teCollDoc.birth_date);
  result.email = String(teCollDoc.email);
  result.phone = String(teCollDoc.phone);
  try {
	result.position = { id: Int(teCollDoc.position_id), name: String(teCollDoc.position_name) };
  } catch (err) { result.position = { id: null, name: '' };}
  //result.subdiv =  {id: Int(teCollDoc.position_parent_id), name: String(teCollDoc.position_parent_name)};	//TODO: редактирование этих полей
  try {
	result.org = { id: Int(teCollDoc.org_id), name: String(teCollDoc.org_name) };
  } catch (err) { result.org = { id: null, name: '' };}

  return result;//teCollDoc;

}

function getResponsable(teOrgDoc) {

  if (teOrgDoc.ChildExists('func_managers') && ArrayOptFirstElem(ArrayDirect(teOrgDoc.func_managers)) != undefined) {

    return openCollaborator(ArrayOptFirstElem(ArrayDirect(teOrgDoc.func_managers)).person_id);

  } else {

    return {
      id: null,

      lastname: '',
      firstname: '',
      middlename: '',

      sex: '',
      birth_date: '',
      email: '',
      phone: '',

      position: { id: null, name: '' },
      subdiv: { id: null, name: '' },
      org: { id: null, name: '' }
    }

  }

}

function getImg(id) {
  fileExist = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = " + XQueryLiteral(id) + " return $elem")) != undefined;
  if (fileExist) {
    imgDoc = OpenDoc(UrlFromDocID(Int(id))).TopElem;
    obj = {};
    obj.id = String(id);
    obj.name = String(imgDoc.name);
    obj.url = "/download_file.html?file_id=" + String(id);
    return obj;
  }
}
function openOrg(org_id) {

  var result = {};
  try {
	var teOrgDoc = OpenDoc(UrlFromDocID(Int(org_id))).TopElem;
  } catch (e) {
    response.error = 'Вы не состоите ни в одной организации, либо она была удалена. Обратитесь к администратору.';
    return;
  }

  result.id = Int(teOrgDoc.id);
  result.code = String(teOrgDoc.code);
  result.name = String(teOrgDoc.name);
  result.disp_name = String(teOrgDoc.disp_name);
  result.postal_address = String(teOrgDoc.postal_address);
  result.web = String(teOrgDoc.web);
  result.desc = String(teOrgDoc.desc); //teOrgDoc.HasProperty("desc") ? String(teOrgDoc.desc) : "";
  result.responsable = getResponsable(teOrgDoc);

  result.image = teOrgDoc.resource_id ? getImg(teOrgDoc.resource_id) : [];
  result.legal_address = String(teOrgDoc.custom_elems.ObtainChildByKey('legal_address').value);
  result.files = [];
  if (teOrgDoc.files.ChildExists("file")) {
	for (file in teOrgDoc.files) {
		file_doc = OpenDoc(UrlFromDocID(Int(file.file_id))).TopElem;
		result.files.push({
			"id": Int(file_doc.id), 
			"name": String(file_doc.name),
			"url": "/download_file.html?file_id="+String(file_doc.id),
			"create_date": String(StrDate(file_doc.doc_info.creation.date, false)),
		})
	}
  }

  return result;//teCollDoc;

}

function createPosition(collab_id, org_id) {
  var new_doc = tools.new_doc_by_name('position');
  new_doc.BindToDb(DefaultDb);

  new_doc.TopElem.name = 'представитель';
  new_doc.TopElem.basic_collaborator_id = Int(collab_id);
  new_doc.TopElem.org_id = Int(org_id);

  new_doc.Save();
  return new_doc.DocID;
}

function createOrModifyOrg(data) {

  var new_doc;
  var resp_doc;

  if (data.id != '') {

    new_doc = OpenDoc(UrlFromDocID(Int(data.id)));

  } else { 

    new_doc = tools.new_doc_by_name('org');
    new_doc.BindToDb(DefaultDb);

  }

  if (data.responsable.HasProperty('id')&&String(data.responsable.id) != '') {

	resp_doc = OpenDoc(UrlFromDocID(Int(data.responsable.id))); 

  } else {
      
	    resp_doc = tools.new_doc_by_name('collaborator');
	    resp_doc.BindToDb(DefaultDb);

	    resp_doc.TopElem.login = String(data.responsable.login);
	    resp_doc.TopElem.password = String(data.responsable.password);

	    resp_doc.TopElem.lastname = String(data.responsable.lastname);
	    resp_doc.TopElem.firstname = String(data.responsable.firstname);
	    resp_doc.TopElem.middlename = String(data.responsable.middlename);
	    resp_doc.TopElem.email = String(data.responsable.email);
	    resp_doc.TopElem.phone = String(data.responsable.phone);
	    resp_doc.TopElem.position_id = createPosition(resp_doc.DocID, new_doc.DocID);

   }

  new_doc.TopElem.name = String(data.name);
  new_doc.TopElem.disp_name = String(data.disp_name);
  new_doc.TopElem.code = String(data.code);

  new_doc.TopElem.postal_address = String(data.postal_address);
  new_doc.TopElem.web = String(data.web);
  new_doc.TopElem.desc = String(data.desc);

	if (data.HasProperty('legal_address') && String(data.legal_address) != '') {
		new_doc.TopElem.custom_elems.ObtainChildByKey('legal_address').value = String(data.legal_address);
/*
	  if (new_doc.TopElem.ChildExists('essentials') && ArrayOptFirstElem(ArrayDirect(new_doc.TopElem.essentials)) != undefined) {
		ArrayOptFirstElem(ArrayDirect(new_doc.TopElem.essentials)).legal_address = String(data.legal_address);
	  } else {
		new_doc.TopElem.essentials.Add().Value = 'wd';
		//ArrayOptFirstElem(ArrayDirect(new_doc.TopElem.essentials)).legal_address = String(data.legal_address);
	  }
*/
	} else {
		new_doc.TopElem.custom_elems.ObtainChildByKey('legal_address').value ='';
/*
		new_doc.TopElem.essentials.Clear();
*/
	}


  if (data.id != '')	{ new_doc.TopElem.func_managers.Clear();	 } else { //манипуляции с функ.руководителем
	
		new_doc.TopElem.place_id = createPlace(new_doc);
	        if (data.HasProperty('is_educ')&&data.is_educ)   createEdOrg(new_doc)	//создавать обучающюю организацию - да/нет
  }

   var new_func_manager = new_doc.TopElem.func_managers.AddChild();
   new_func_manager.person_id = resp_doc.DocID;
   tools.common_filling('collaborator', new_func_manager, resp_doc.DocID, resp_doc.TopElem);

   editSameEntityByCode(new_doc.DocID,  String(data.name)); //изменение связанных объектов

  resp_doc.Save();
  new_doc.Save();

  try {
	OpenDoc(UrlFromDocID(Int(resp_doc.TopElem.position_id))).Save();
  } catch(no_positon) {}

  alert ('**********COPP_LOG********** (mode=org_profile): Пользователь  «' +curUser.fullname+'» (' +curUserID+') изменил параметры организации «' +new_doc.TopElem.disp_name+'» (' +new_doc.DocID+'). Дата: '+StrXmlDate(Date()));

  return new_doc.DocID;

}

    function getFileType(file_extension) {
        var result = "file";    //default
        switch (file_extension) {
          case 'swf': result = "swf"; break;
          case 'flv': result = "flv"; break;
          case 'jpg': case 'jpeg': case 'png': result = "img"; break;
          case 'ppt': case 'pptx': result = "ppt"; break;
          case 'doc': case 'docx': result = "doc"; break;
          case 'xls': case 'xlsx': result = "excel"; break;
          case 'pdf': result = "pdf"; break;     
          case 'wav': case 'mp3': result = "audio"; break;        
          case 'avi': case 'mp4': case 'wmv': result = "video"; break;           
        }
        return result;
      }
      

	function uploadFile(form) {

		var file = form.file;
		var nameOfFile = form.file_name;
		bytes = StrLen( file.FileName );
		fileName = String( file.FileName );
		dots = fileName.split(".");
		fileType = StrLowerCase(String(dots[dots.length-1]));
	    
		newDoc = tools.new_doc_by_name('resource');
		newDoc.BindToDb(DefaultDb);
		localFile = fileName;
		xLocalFile = String("x-local://wt_data/attachments/" + newDoc.TopElem.id + "." + fileType);
		PutUrlData( xLocalFile, file );
	    
		if (FilePathExists( UrlToFilePath( xLocalFile ) )) {
		fileSize = UrlFileSize( xLocalFile );
		} else {
		fileSize = bytes;
		}
		newDoc.TopElem.file_url = xLocalFile;
		newDoc.TopElem.size = fileSize;
		newDoc.TopElem.name = String(nameOfFile);
		newDoc.TopElem.file_name = fileName;
		newDoc.TopElem.type = getFileType(fileType);
		newDoc.Save();

		orgDoc = OpenDoc(UrlFromDocID(Int(curUser.org_id)));
		newFile = orgDoc.TopElem.files.AddChild();
		newFile.file_id = newDoc.DocID;
		orgDoc.Save();

		    
	}


var response = {};

try {

  if (Request.Method == "GET") {

      response.orgInfo = openOrg(Int(curUser.org_id));

  } else if (Request.Method == "POST") {


	if (Request.Query.HasProperty('addfile') && Request.Form.HasProperty("file")) {
		uploadFile(Request.Form);
	} else {

    var orgInfo = tools.read_object(Request.Body);
    response.body = orgInfo;
      response.id = createOrModifyOrg(orgInfo);
      response.info = orgInfo;
  }
  }

} catch (err) { response.error = String(err); }

Response.Write(tools.object_to_text(response, 'json'));
%>