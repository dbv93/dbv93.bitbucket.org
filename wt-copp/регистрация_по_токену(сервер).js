<%
	var lifespan = 30; //Дефолтная продолжительность жизни токена

	//------------------------------------------------------------------------------Функции-начало---------------------------------------------------------------------//

	function checkTokenFresh (token) {

		return (DateDiff(new Date(), token.registration_date) < token.lifespan*24*60*60);	//тут уже произвольная продолжительность жизни
	}

	function checkLoginUnique (login) {
		return ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/login = "+XQueryLiteral(login)+"  and $elem/web_banned = false() return $elem")) == undefined;
	}
	
	function confirmCollaborator (id) {
		var collDoc = OpenDoc(UrlFromDocID(Int(id)));
		collDoc.TopElem.access.web_banned = false;
		collDoc.Save();
	}

	function createNewCollaborator (token) {
		var new_doc = tools.new_doc_by_name('collaborator');
		var fullnameArr = String(token.user_fullname).split(' ');
		new_doc.TopElem.lastname=fullnameArr[0];
		new_doc.TopElem.firstname=fullnameArr[1];
		new_doc.TopElem.middlename=fullnameArr[2];

		//new_doc.TopElem.fullname =
		new_doc.TopElem.login = String(token.user_login);
		new_doc.TopElem.password = String(token.user_password);
		new_doc.TopElem.email = String(token.user_login);
		new_doc.TopElem.access.web_banned = true;
		new_doc.BindToDb(DefaultDb);

		new_doc.Save();

		return new_doc.DocID;		//WT умеет отрправлять уведомления только сотрудникам в БД
	}							//Придется сначала создавать сотрудника...

	function fullfillToken(token) {
		tokenDoc = OpenDoc(UrlFromDocID(Int(token.id)));
		tokenDoc.TopElem.fullfilled = true;
		tokenDoc.Save();
	}

	function createNewToken(registrationForm) {
		var new_doc = OpenNewDoc('x-local://udt/udt_cc_user_token.xmd');

		//alert(tools.object_to_text(new_doc.TopElem, 'json'));
		new_doc.TopElem.fullfilled = false;
		new_doc.TopElem.user_fullname = String(registrationForm.fullname);
		new_doc.TopElem.user_login = String(registrationForm.login);
		new_doc.TopElem.user_password = String(registrationForm.password);
		new_doc.TopElem.registration_date = new Date();
		new_doc.TopElem.person_id = createNewCollaborator(new_doc.TopElem);
		new_doc.TopElem.lifespan = lifespan;

		new_doc.BindToDb(DefaultDb);
		new_doc.Save();

		tools.create_notification("copp_registr", new_doc.DocID);
	}

	//------------------------------------------------------------------------------Функции-конец----------------------------------------------------------------------//


	//------------------------------------------------------------------Обработка запроса-начало--------------------------------------------------------------------//
	var response = {};

	if (Request.Method=='POST') {	

		if (Request.Query.HasProperty('token_id')) {	
			try {
				reqToken = ArrayOptFirstElem(XQuery("for $elem in cc_user_tokens where $elem/id = "+String(Request.Query.token_id)+" and $elem/fullfilled = false() return $elem"));
				if (reqToken != undefined) {

					var isTokenFresh = checkTokenFresh(reqToken);

					if (isTokenFresh) {
						fullfillToken(reqToken);
						confirmCollaborator(reqToken.person_id);
					} else {
						response.error = 'Токен устарел';
					}
				} else { 
					response.error = 'Токен не найден';
				}
			} catch (err) {response.error = 'Ошибка при подтверждении токена: '+String(err)}
		} else {	//Создание токена по форме
			try {
				formData = tools.read_object(Request.Body);
				var isLoginUnique = checkLoginUnique(formData.login);
				if (isLoginUnique) { 
					createNewToken(formData);
				} else {
					response.error = 'Такой логин уже есть';
				}
			} catch (err) {response.error = 'Ошибка при создании токена: '+String(err)}
		}
		//if (response.HasProperty('error')) throw '{error: '+String(response.error)+'}';

	}

	else if (Request.Method=='GET') {
		if (Request.Query.HasProperty('newemail')) {
			response.isUnique = checkLoginUnique (String(Request.Query.newemail));
		}
	}

	Response.Write(tools.object_to_text(response, 'json')); 
	//--------------------------------------------------------------------Обработка запроса-конец--------------------------------------------------------------------//
%>

