<%
	//------------------------------------------------------------------------------Функции-начало---------------------------------------------------------------------//

	function getCompetencies () {
		objs = [];
		for (item in XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6753954396609189672 return $elem")) {
			objs.push({ id: String(item.id), name: String(item.name), type: "competence" });
		}
		return objs;
	}


	function getProfessions () {
		objs = [];
		for (item in XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6753954396609189673 return $elem")) {
			objs.push({ id: String(item.id), name: String(item.name), type: "profession" });
		}
		return objs;
	}

	function getListenersCategories () {
		objs = [];
		for (item in XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6759588246871560947 return $elem")) {
			objs.push({ id: String(item.id), name: String(item.name), type: "listeners" });
		}
		return objs;
	}

	function getImg(id) {
	  fileExist = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = " + XQueryLiteral(id) + " return $elem")) != undefined;
	  if (fileExist) {
	    imgDoc = OpenDoc(UrlFromDocID(Int(id))).TopElem;
	    obj = {};
	    obj.id = String(id);
	    obj.name = String(imgDoc.name);
	    obj.url = "/download_file.html?file_id=" + String(id);
	    return obj;
	  }
	}

	function getMPmodules (teProgDoc) {
		var result = [];
		var programs = teProgDoc.programs;
		var modules = ArraySelect(programs, "This.type=='education_program'");

		for(module in modules) {
			result.push({
				name: String(module.name),
				comment: String(module.comment),
				duration: String(module.duration),
				type: "education_program"
			});
		}
		return result;
	}

	function getOrgType(teOrgDoc) {
		var result = 'Не образовательная организация';
		if (ArrayOptFirstElem(XQuery("for $elem in education_orgs where ( ($elem/code != '' and $elem/code = " + XQueryLiteral(String(teOrgDoc.code)) + ") or ($elem/name != '' and $elem/name = " + XQueryLiteral(String(teOrgDoc.name)) + ") ) return $elem")) != undefined) result = 'Образовательная организация';
		return result;
	}

	function getKnowledgeParts(teProgDoc) {
		var result = [];
		for (_knowledge_part in teProgDoc.knowledge_parts) {
				result.push({id: String(_knowledge_part.knowledge_part_id), name: String(_knowledge_part.knowledge_part_name)});
		}
		return result;
	}

	function getOrgFromRole(teProgDoc) {
		var rolesArr = teProgDoc.role_id;
		for (role in rolesArr) {
		    roleBD = ArrayOptFirstElem(XQuery("for $elem in roles where $elem/id = "+String(role)+" return $elem"));
		    if (roleBD != undefined) {
			return { "id": StrReplace(String(roleBD.code), "org_", ""), "name": String(roleBD.name) };
		    }
		}
		return {};
	}

	function getCompetences (teProgDoc, param) {

		var result = [];
		var classifier_id = '';
		switch (String(param)) {
			case 'comp': classifier_id = '6753954396609189672'; break;
			case 'listener': classifier_id = '6759588246871560947'; break;
			case 'kind': classifier_id = '6771076880541175630'; break;
			case 'prof': classifier_id = '6753954396609189673'; break;
		}
		for (_knowledge_part in teProgDoc.knowledge_parts) {
				isCompetence = ArrayOptFirstElem(XQuery("for $elem in knowledge_parts where $elem/id = "+_knowledge_part.knowledge_part_id+"and $elem/knowledge_classifier_id = "+Int(classifier_id)+" return $elem")) != undefined;
				if(isCompetence) result.push({id: String(_knowledge_part.knowledge_part_id), name: String(_knowledge_part.knowledge_part_name)});
		}
		return result;
	}


	function getHours(teProgDoc) {
		var result = 0;
		var modules = ArraySelect(teProgDoc.programs, "This.type=='education_program'");
			for(module in modules) {
		    if (OptInt(module.duration)!=undefined) result+=OptInt(module.duration);
		}   
		return result;
	}

	function checkEnroll(id) {
		if (Request.Session.HasProperty('cur_user_id') && Request.Session.cur_user_id != null && Request.Session.cur_user_id != undefined)
		return ArrayOptFirstElem(XQuery("for $elem in requests where $elem/object_id = "+XQueryLiteral(OptInt(id))+" and $elem/type = 'compound_program' and $elem/person_id = " +XQueryLiteral(OptInt(Request.Session.GetOptProperty('cur_user_id') ))+ " return $elem")) != undefined
		return false
	}

	function openProg(id) {

	var result = {};
	var teProgDoc = OpenDoc(UrlFromDocID(Int(id))).TopElem;

	result.id = Int(teProgDoc.id);
	result.name = String(teProgDoc.name);
	result.desc = String(teProgDoc.desc);
	result.type = "compound_program";
	result.link = '/view_doc.html?mode=cp_request_create&request_type_id=6755889390303450248&object_id='+String(teProgDoc.id);
	result.prog_structure = getMPmodules(teProgDoc);
	result.knowledge_parts = getKnowledgeParts(teProgDoc);
	result.listeners_category = getCompetences(teProgDoc, 'listener');
	result.program_kind = getCompetences(teProgDoc, 'kind');
	result.professions = getCompetences(teProgDoc, 'prof');
	result.competences = getCompetences(teProgDoc, 'comp');
	result.competencies = getCompetences(teProgDoc, 'comp');
	result.org = getOrgFromRole(teProgDoc);
	result.hours = getHours(teProgDoc);
	result.is_enrolled = checkEnroll(id);
	result.create_date = StrXmlDate(Date(teProgDoc.doc_info.creation.date), false); //String(teProgDoc.doc_info.creation.date).slice(0, 10);
    
	creatorName = '';


	if (String(teProgDoc.doc_info.creation.user_login) != '') {
		creatorObj = ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/login = "+XQueryLiteral(teProgDoc.doc_info.creation.user_login)+" return $elem"));
		if (creatorObj != undefined) creatorName = String(creatorObj.fullname);
	}

	result.author = creatorName;

	result.event_form = String(teProgDoc.custom_elems.ObtainChildByKey("event_form").value);

	result.image = teProgDoc.resource_id ? getImg(teProgDoc.resource_id) : [];


	  return result;

	}

	function getRoles () {	//модульные программы
		curRole = ArrayDirect(XQuery("for $elem in group_collaborators where $elem/code='copp' and $elem/collaborator_id="+XQueryLiteral(Request.Session.cur_user_id)+" return $elem/group_id"));
		if (ArrayCount(curRole) != 0) {
			if (ArrayCount(curRole) > 1) { curRole = curRole.join(','); } else { curRole = String(curRole[0]); }
			curRole = StrContains(curRole, "6748398834638268528") ? "copp" : StrContains(curRole, "6761406065601834020") ? "copp" : StrContains(curRole, "6761406065601834022") ? "employer" : StrContains(curRole, "6761406065601834021") ? "org_represent" : undefined;
		} else {curRole = undefined}
		return curRole;
	}

	function getPrograms () {	//модульные программы

		var result = [];
		var queryRes = [];

		if (!Request.Query.HasProperty("test")) 
			queryRes = XQuery("for $elem in compound_programs where $elem/code!='' and $elem/name!='' and doc-contains($elem/id,'wt_data','[cp_status=Принята]')=true() and doc-contains($elem/id,'wt_data','[hidden=true]')=false() order by $elem/name return $elem"); 
		else 
			queryRes = XQuery("for $elem in compound_programs where $elem/name='' or doc-contains($elem/id,'wt_data','[hidden=true]')=true() order by $elem/name return $elem")

		for (_querObj in queryRes) {

			try {
				result.push(openProg(_querObj.id));
			} catch (err) { response.error = String(err) }

		}
		return result;
	}

	function getModules () {	//наборы программ

        var result = [];
        
        var education_programs = XQuery("for $elem in education_programs order by $elem/name return $elem");
        var education_methods_all_arr = XQuery("education_methods");

        for (ed_program in education_programs) {
            education_program_education_methods_arr = XQuery("for $elem in education_program_education_methods where $elem/education_program_id="+OptInt(ed_program.id)+" return $elem");
            education_methods_arr = ArrayIntersect(education_methods_all_arr, education_program_education_methods_arr,"This.id", "This.education_method_id");
            
            ed_program_knowledge_parts = [];
            for (ed_method in education_methods_arr) {
                if(String(ed_method.knowledge_parts)!='') {
                    ed_program_knowledge_parts= ArrayUnion(ed_program_knowledge_parts, String(ed_method.knowledge_parts).split(';'))
                }
            }
            
            if (ArrayCount(ed_program_knowledge_parts)>0) {
                ed_program_knowledge_parts = ArrayIntersect(ArrayUnion(getCompetencies(), getProfessions()), ArraySelectDistinct(ed_program_knowledge_parts,'This'),"This.id", "This");
            }

            teProgDoc = OpenDoc(UrlFromDocID(ed_program.id)).TopElem;

            result.push(
                {
                    id: OptInt(ed_program.id),
                    name: String(ed_program.name),
                    hours: ArraySum(education_methods_arr, 'duration'),
                    cost: ArraySum(education_methods_arr, 'cost'),
                    knowledge_parts: ed_program_knowledge_parts,
                    desc: String(teProgDoc.desc)
                }
            );
        }

		return result;
	}

	function getEducOrgs() {
        var result = [];
        var educ_orgs = XQuery("for $o in orgs, $e in education_orgs where $o/id = $e/code return $o");
        for (_ed_org in educ_orgs) {
            //try { 
            //    _ed_orgDoc = OpenDoc(UrlFromDocID(Int(_ed_org.id))).TopElem;
            //} catch (err) { continue; }
            //if (String(_ed_orgDoc.postal_address) != '') {
            // result.push(String(_ed_orgDoc.postal_address));
            //}
		result.push({
			id: Int(_ed_org.id),
			name: String(_ed_org.name)
		});
        }
        return result;
	}

    function getEducationForms() {
        var result = [];
        var objsQuery = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6773685341833798064 return $elem");
        for (obj in objsQuery) {
          result.push({
            id: Int(obj.id),
            name: String(obj.name),
            code: String(obj.code)
          });
        }
        return result;
      }

    function getOkvedCatalog() {
	result = [];
        var objsQuery = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6773685341833798079 return $elem");
        for (obj in objsQuery) {
          result.push({
            id: Int(obj.id),
            name: String(obj.name),
            code: String(obj.code)
          });
        }
	return result;
    }

    function getProgramTypes() {
	result = [
		{ id: 1, name: String("Повышение квалификации") },
		{ id: 2, name: String("Переподготовка") }
	];
	return result;
    }

	function getFavorites () {
		var teCollDoc = OpenDoc(UrlFromDocID(Int(Request.Session.cur_user_id))).TopElem;
		if (teCollDoc.custom_elems.ChildByKeyExists("favorite")) {
			var favorite = String(teCollDoc.custom_elems.GetChildByKey("favorite").value);
		} else {
			var favorite = '';
		}
		return favorite;
	}

	//------------------------------------------------------------------------------Функции-конец----------------------------------------------------------------------//


	//------------------------------------------------------------------Обработка запроса-начало--------------------------------------------------------------------//
	var response = {};

	try {

		if (Request.Method=='POST') {	

		}

		else if (Request.Method=='GET') {
			if (Request.Query.HasProperty('id')) {
				response.data = openProg(Request.Query.id);
			} else {
				response.data = getPrograms();
				response.competencies = getCompetencies();
				response.professions = getProfessions();
				response.modules = getModules();
				response.educOrgs = getEducOrgs();
				response.curDate = StrXmlDate(new Date(),true);			
				response.educationForms = getEducationForms();
				response.listenersCategory = getListenersCategories();

			if (Request.Session.HasProperty('cur_user_id') && Request.Session.cur_user_id != null && Request.Session.cur_user_id != undefined) {
				response.groupRequest = getRoles() == "employer";
				response.favorites = getFavorites();
			if (response.groupRequest) {
				response.okved = getOkvedCatalog();
				response.programTypes = getProgramTypes();
			}
			response.test = getRoles();
			}
			}
			
		}

	} catch (err) {response.error = String(err)}

	Response.Write(tools.object_to_text(response, 'json')); 
	//--------------------------------------------------------------------Обработка запроса-конец--------------------------------------------------------------------//
%>