<%
	function getObjs () {	//Общий GET
		var result = [];
		var objsQuery = XQuery("for $elem in polls order by $elem/name return $elem");

		for (obj in objsQuery) {

		    teObjDoc = OpenDoc(UrlFromDocID(Int(obj.id))).TopElem;

		    creatorName = 'Не задано';

		    pollResults = XQuery("for $elem in poll_results where $elem/poll_id = "+XQueryLiteral(obj.id)+" and $elem/status = '2' return $elem");

		    if (String(teObjDoc.doc_info.creation.user_login) != '') {
			creatorObj = ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/login = "+XQueryLiteral(teObjDoc.doc_info.creation.user_login)+" return $elem"));
			if (creatorObj != undefined) creatorName = String(creatorObj.fullname);
		    }

		    result.push({

			id: Int(obj.id),
			name: String(obj.name),
			status: (obj.completed ? 'Завершен': 'Проводится'),
			start_date: StrXmlDate(teObjDoc.start_date, false),
			end_date: StrXmlDate(teObjDoc.end_date, false),
			creator: creatorName,
			participants_num: ArrayCount(pollResults)

		    });
		}
		return result;
    }
    

    function getPollResultStats(tePollDoc) {
        var result = [];
        for (question in tePollDoc.questions) {//карточка опроса, не результата
            result_obj = {
                question_id: Int(question.id),
                question: String(question.title),
                type: String(question.type), 
                answers: []
            }
            for (entry in question.entries) {
                if (entry.ChildExists('value')&&String(entry.value)!='') {
                    entry_value = String(entry.value);
                    result_obj.answers.push({id: Int(entry.id), answer: String(entry_value), counter: Int(0)})
                }
            }
            result.push(result_obj);
        }
        var pollResults = XQuery("for $elem in poll_results where $elem/poll_id = "+XQueryLiteral(Int(tePollDoc.id))+" and $elem/status = '2' return $elem");
        for (poll_res in pollResults) {
            pollResDoc = OpenDoc(UrlFromDocID(Int(poll_res.id))).TopElem;
            for (questionEntry in pollResDoc.questions) {
                resultQuestionElem = ArrayOptFind ( result, 'question_id=='+String(questionEntry.id) );
                if(resultQuestionElem == undefined) continue;
                switch (String(resultQuestionElem.type)) {
                    case 'choice': case 'combo':
                        for (entry in resultQuestionElem.answers) {
                            if (String(entry.id) == String(questionEntry.value))
                            entry.counter++;
                        }
                    break;
                    case 'select': 
                        for (entry in resultQuestionElem.answers) {
                            entriesArr = String(questionEntry.value).split(';');
                            for (entry_value in entriesArr) {
                                if (String(entry.id) == String(entry_value))
                                entry.counter++;
                            }
                        }
                    break;
                    case 'string': case 'text': case 'number':
                        resultQuestionElem.answers.push({id: null, answer: String(questionEntry.value), counter: null});
                    break;
                }
        
            }
        }
        return result;

    }

	function openObj (obj_id) {

		var result = {};
		var teObjDoc = OpenDoc(UrlFromDocID(Int(obj_id))).TopElem;

		result.id = Int(teObjDoc.id);
		result.name = String(teObjDoc.name);
		result.start_date = (String(teObjDoc.start_date).length ? StrXmlDate(teObjDoc.start_date, false) : '');
		result.end_date = (String(teObjDoc.end_date).length ? StrXmlDate(teObjDoc.end_date, false) : '');
		result.status = (teObjDoc.completed ? 'Завершен': 'Проводится');
		result.questions = [];
		result.usersOfPoll =  (teObjDoc.is_anonymous ? 'all': 'registered');
		for (question in teObjDoc.questions) {
			_entries = [];
			for (_entry in question.entries) {
				_entries.push( {id: Int(_entry.id), value: String(_entry.value)} );
			}
			result.questions.push({id: Int(question.id), title: String(question.title), type: String(question.type), entries: _entries});
		}
		var pollsRes = getPollResultStats(teObjDoc);
		// for (answer in pollsRes) {
        //     thisQuestion = ArrayOptFind(result.questions, 'id=='+String(answer.id));
		// 	if (thisQuestion != undefined) {
		// 		answer.question_title = String(thisQuestion.title);
		// 	}
		// }
		result.pollsRes = pollsRes;
		//result.shit = getPollResultStats(obj_id);
		result.finished = ArrayCount(XQuery("for $elem in poll_results where $elem/poll_id = "+XQueryLiteral(Int(obj_id))+" and $elem/status = '2' return $elem"));
		result.multiPages = ArrayCount(ArraySelect(teObjDoc.items,"This.type=='pagebreak'"))>0;
		return result;

	}


	function modifyQuestions (docObj, questionsArray, multiPages) {

		var i;
		var j;

		docObj.TopElem.questions.Clear();

		for (i = 0; i<ArrayCount(questionsArray); i++) {

			newQuestionItem = docObj.TopElem.questions.AddChild();
			newQuestionItem.id = questionsArray[i].id;
			newQuestionItem.title = questionsArray[i].title;
			newQuestionItem.type = questionsArray[i].type;


			for (j = 0; j<ArrayCount(questionsArray[i].entries); j++) {

				newEntryItem = newQuestionItem.entries.AddChild();
				newEntryItem.id = questionsArray[i].entries[j].id;
				newEntryItem.value = questionsArray[i].entries[j].value;
			}
		}

		docObj.TopElem.items.Clear();

		if (multiPages) {
			for (i = 0; i<ArrayCount(questionsArray); i++) {
				newSectionItem = docObj.TopElem.items.AddChild();
				newSectionItem.type = 'question';
				newSectionItem.question_id = questionsArray[i].id;
				if (i!=ArrayCount(questionsArray)-1&&ArrayCount(questionsArray)>1) {	//pagebreak
					newSectionItem = docObj.TopElem.items.AddChild();
					newSectionItem.type = 'pagebreak';
				}
			}
		}
	}

	function createOrModifyObj (data) {

		var new_doc;

		if (data.HasProperty('id')) {

		    new_doc = OpenDoc(UrlFromDocID(Int(data.id)));

		} else {
			new_doc = tools.new_doc_by_name('poll');
			new_doc.BindToDb(DefaultDb);
			new_doc.TopElem.show_report = true;
			new_doc.TopElem.is_one_time = true;
		}

		new_doc.TopElem.name = String(data.name);

		new_doc.TopElem.start_date = Date(data.start_date);
		new_doc.TopElem.end_date = Date(data.end_date);

		if ( String(data.usersOfPoll) == 'all' ) {
			new_doc.TopElem.is_anonymous = true;
			new_doc.TopElem.access.enable_anonymous_access = true;
		} else if ( String(data.usersOfPoll) == 'registered') {
			new_doc.TopElem.is_anonymous = false;
			new_doc.TopElem.access.enable_anonymous_access = false;
		}

		var multiPages = false;
		if (data.HasProperty("multiPages")) {
			multiPages = tools_web.is_true (data.multiPages, false);
		}

		modifyQuestions (new_doc, data.questions, multiPages);

		new_doc.Save();

		return new_doc;

	}   
	    
	function deleteObj (id) {

		DeleteDoc(UrlFromDocID(Int(id)));					//delete obj

		var pollResults = XQuery("for $elem in poll_results where $elem/poll_id = "+XQueryLiteral(Int(id))+" return $elem");

		for  (_pollRes in pollResults) {
			DeleteDoc(UrlFromDocID(Int(_pollRes.id)));					//delete obj
		}

	}

	var response = {};

	try {

		if(Request.Method == "GET") {

		    if (Request.Query.HasProperty('id')) {
			response.data = openObj(Request.Query.id);		           
		    } else {
			if (ArrayOptFirstElem(XQuery("for $elem in group_collaborators where $elem/collaborator_id = "+XQueryLiteral(Int(curUserID))+" and $elem/group_id = 6761406065601834020 return $elem"))!= undefined) {
				response.canEdit = false;
			} else {response.canEdit = true;}
			response.data = getObjs();
		    }

		} else if (Request.Method == "POST") {

		    var objInfo = tools.read_object(Request.Body);
		    response.body = objInfo;
		    if (Request.Query.HasProperty('delete')) {
			deleteObj(objInfo.id);
		    } else {
			createOrModifyObj(objInfo);
		    }
		}

	} catch (err) {response.error = String(err);}

	Response.Write(tools.object_to_text(response, 'json'));
%>