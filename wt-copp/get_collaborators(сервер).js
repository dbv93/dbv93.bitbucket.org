<%
	function checkLoginUnique (login) {
		return ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/login = "+XQueryLiteral(login)+" and $elem/web_banned = false() return $elem")) == undefined;
	}

	function getGroupsList () {
		var result = [];
		rightsGroups =  XQuery("for $elem in groups where MatchSome($elem/role_id, ( 6761406065601834027)) return $elem");
		for (group in rightsGroups) {
			result.push({
				id: Int(group.id),
				name: String(group.name)
			});
		}
		return result;
	}
	function getOrgsList () {
		var result = [];
		orgsList =  XQuery("for $elem in orgs where $elem/id != 6748398834638268537 order by $elem/name descending return $elem");
		for (org in orgsList) {
			result.push({
				id: Int(org.id),
				name: String(org.disp_name)
			});
		}
		return result;
	}
	function getCompetences() {
		var result = [];
		var objsQuery = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6753954396609189672 return $elem");
		for (obj in objsQuery) {
			result.push({
				id: Int(obj.id),
				name: String(obj.name)
			});
		}
		return result;
	}
	function getCollabs () {
		var result = [];
		var userRole = ArrayOptFirstElem(XQuery("for $elem in group_collaborators where $elem/collaborator_id = "+XQueryLiteral(curUserID)+" and $elem/group_id = 6761406065601834022 return $elem"));  
		if (userRole != undefined) {  
			if (String(curUser.org_id) != "") {
				var collabsQuery = XQuery("for $c in collaborators, $p in positions where $p/org_id = "+XQueryLiteral(curUser.org_id)+" and $c/id = $p/basic_collaborator_id return $c"); //проверять по должности?
			} else {response.error = "Вы не состоите ни в одной организации."; return;}
		} else {
			var collabsQuery = XQuery("for $elem in collaborators where $elem/id != 6748398834638268620 and $elem/id != 6748398834638268527 and $elem/web_banned = false() order by $elem/fullname return $elem");
		}
		for (collab in collabsQuery) {
			result.push({
				id: Int(collab.id),
				fullname: String(collab.fullname),
				birth_date: String(collab.birth_date)
			});
		}
		return result;
	}
	function getTeachers (curUserRole) {
		var result = [];
		var collRole = {};
		if (curUserRole == "copp") {
			var collabsQuery = XQuery("for $elem in collaborators where $elem/id != 6748398834638268620 and $elem/id != 6748398834638268527 and $elem/web_banned = false() order by $elem/fullname return $elem");
		} else {
			curUserQuery = ArrayOptFirstElem(XQuery("for $elem in collaborators where $elem/id="+XQueryLiteral(curUserID)+" return $elem"));
			if (String(curUserQuery.org_id) != "") {
				var collabsQuery = XQuery("for $elem in collaborators where $elem/id != 6748398834638268620 and $elem/id != 6748398834638268527 and $elem/org_id = "+XQueryLiteral(Int(curUserQuery.org_id))+" and $elem/web_banned = false() order by $elem/fullname return $elem");
			} else {
				response.error = "Вы не состоите ни в одной организации."; return;
			}
		}
		var competences = [];
		var knowledge_parts = [];
		for (collab in collabsQuery) {
			isExpert = "Нет";
			if (ArrayOptFirstElem(XQuery("for $elem in group_collaborators where $elem/group_id = 6761406065601834023 and $elem/collaborator_id = "+XQueryLiteral(Int(collab.id))+" order by $elem/fullname return $elem"))!=undefined) {

				competences = [];
				knowledge_parts = [];
				expert = ArrayOptFirstElem(XQuery("for $elem in experts where $elem/person_id = "+XQueryLiteral(Int(collab.id))+" return $elem"));
				if (expert != undefined) {
					isExpert = "Да";
					knowledge_parts = XQuery("for $elem in knowledge_parts where $elem/experts !='' and $elem/knowledge_classifier_id = 6753954396609189672 return $elem");
					for (part in knowledge_parts) {
						partExperts = String(part.experts).split(';');
						if (ArrayOptFind(partExperts, 'This == ' + "String(expert.id)") != undefined ) {
							competences.push(String(part.name))
						}
					}
				}

				result.push({
					id: Int(collab.id),
					fullname: String(collab.fullname),
					birth_date: String(collab.birth_date),
					competences: competences,
					isExpert: String(isExpert),
				});
			}
		}
		return result;
	}

    function addToGroup(coll_id, group_id) {
        var groupDoc = OpenDoc(UrlFromDocID(Int(group_id)));
        var groupColl = groupDoc.TopElem.collaborators.ObtainChildByKey(coll_id);
        groupDoc.Save();
    }

    function deleteFromGroup(coll_id, group_id) {
        var groupDoc = OpenDoc(UrlFromDocID(Int(group_id)));
        var groupColl = groupDoc.TopElem.collaborators.DeleteChildByKey(coll_id);
        groupDoc.Save();
    }

	function createDefaultPosition (collab_id) {    //Создание дефолтной должности
		var new_doc = tools.new_doc_by_name('position');
		new_doc.TopElem.name = 'обучающийся';
		new_doc.TopElem.basic_collaborator_id = Int(collab_id);
		new_doc.TopElem.parent_object_id = 6748398834638268591;	//Обучающиеся ЦОПП
		new_doc.TopElem.org_id = 6748398834638268537;			//ЦОПП
		new_doc.BindToDb(DefaultDb);	
		new_doc.Save();
		return new_doc.DocID;
	}

	function changePosition (posDoc, org_id, position_name) {	//Создание/изменение недефолтной должности
		if (position_name == '') position_name = 'сотрудник';
        posDoc.TopElem.name = position_name;
		posDoc.TopElem.org_id = Int(org_id);
       // posDoc.TopElem.parent_object_id = null;		
		posDoc.Save();
		return posDoc.DocID;		
	}

    function getRole(collab_id) {
        var groupColl = ArrayOptFirstElem(XQuery("for $elem in group_collaborators where $elem/collaborator_id = "+String(collab_id)+" and contains($elem/code, 'edgr_')=false() return $elem"));  
        if (groupColl != undefined) {    
            return {id: Int(groupColl.group_id), name: String(groupColl.name)}
        } else {return {id: '', name: ''}}
    }

	function openCollaborator (collab_id) {

		var result = {};
		var teCollDoc = OpenDoc(UrlFromDocID(Int(collab_id))).TopElem;

		result.id = Int(teCollDoc.id);

		result.lastname= String(teCollDoc.lastname);
		result.firstname =String(teCollDoc.firstname);
		result.middlename = String(teCollDoc.middlename);

		result.sex = String(teCollDoc.sex);
		result.birth_date = String(teCollDoc.birth_date);
		result.email = String(teCollDoc.email);

        if (String(teCollDoc.org_id) != '' && String(teCollDoc.org_id) != 6748398834638268537 ) {  //дефолт не выводим
	    	result.org = {id: Int(teCollDoc.org_id), name: String(teCollDoc.org_name)};
            result.position = {id: Int(teCollDoc.position_id), name: String(teCollDoc.position_name)};
        } else {
  	    	result.org = {id: '', name: ''};          
            result.position = {id: '', name: 'обучающийся'};
        }

        result.role = getRole(collab_id);

        result.files = [];
	result.competences = [];
	var expert = ArrayOptFirstElem(XQuery("for $elem in experts where $elem/person_id = "+XQueryLiteral(Int(collab_id))+" return $elem"));
	if (expert != undefined) {
		result.expert = true;

		if (teCollDoc.files.ChildExists("file")) {
			for (file in teCollDoc.files) {
				file_doc = OpenDoc(UrlFromDocID(Int(file.file_id))).TopElem;
				if (String(file_doc.resource_type_id) == "6765428823044459124") { //если это паспорта ЭКСПЕРТА
					result.files.push({
						"id": Int(file_doc.id), 
						"name": String(file_doc.name),
						//"comment": String(file_doc.comment),
						"url": "/download_file.html?file_id="+String(file_doc.id)
					})
				}
			}
		}

		var knowledge_parts = XQuery("for $elem in knowledge_parts where $elem/experts !='' and $elem/knowledge_classifier_id = 6753954396609189672 return $elem");
			for (part in knowledge_parts) {
				partExperts = String(part.experts).split(';');
				if (ArrayOptFind(partExperts, 'This == ' + "String(expert.id)") != undefined ) {
					result.competences.push({
						id: Int(part.id),
						name: String(part.name),
						
					})
				}
			}
		
	} else {result.expert = false;}

		return result;

	}

	function deleteExpertFromCompetences (expertID) {
		var knowledge_parts = XQuery("for $elem in knowledge_parts where $elem/experts !='' and $elem/knowledge_classifier_id = 6753954396609189672 return $elem");
		for (part in knowledge_parts) {
			partExperts = String(part.experts).split(';');
			if (ArrayOptFind(partExperts, 'This == ' + "String(expertID)") != undefined ) { 
				partDoc =  OpenDoc(UrlFromDocID(Int(part.id)));
				partDoc.TopElem.experts.DeleteChildByKey(expertID,"expert_id");
				partDoc.Save();
			}
		}
	} 



	function createOrModifyCollaborator (data) {

		var new_doc;

		if (data.HasProperty('id')) {

			new_doc = OpenDoc(UrlFromDocID(Int(data.id)));

		} else {

			if (data.HasProperty('login')&&String(data.login)!=''&&!checkLoginUnique (String(data.login))) {
				response.error = 'Пользователь с таким логином уже существует в системе'; return;
			}
			new_doc = tools.new_doc_by_name('collaborator');
			new_doc.BindToDb(DefaultDb);
			new_doc.TopElem.custom_elems.ObtainChildByKey('notifications').value = true; //Присылать уведомления на почту
			new_doc.TopElem.custom_elems.ObtainChildByKey('portfolio').value = true; //Отображать портфолио у работодателя

		}

		new_doc.TopElem.lastname=String(data.lastname)
		new_doc.TopElem.firstname=String(data.firstname)
		new_doc.TopElem.middlename=String(data.middlename)

		if (data.HasProperty('login')&&String(data.login)!='') new_doc.TopElem.login = String(data.login);
		if (data.HasProperty('password')&&String(data.password)!='') new_doc.TopElem.password = String(data.password);

		if (String(data.birth_date) != '') new_doc.TopElem.birth_date = Date(data.birth_date);
		new_doc.TopElem.sex = String(data.sex);
		new_doc.TopElem.email = String(data.email);

        //Position Block - Start
		if (!data.HasProperty('id')) { //создание должности нового сотрудника
			new_doc.TopElem.position_id = createDefaultPosition(new_doc.DocID);
			if(String(new_doc.TopElem.position_id)!='')  OpenDoc(UrlFromDocID(Int(new_doc.TopElem.position_id))).Save(); //для отображения в админке
            if ( data.HasProperty('org')&&String(data.org.id)!='') { //есть организация в запросе
                newPosDoc = tools.new_doc_by_name('position');
                newPosDoc.BindToDb(DefaultDb);
                newPosDoc.TopElem.basic_collaborator_id = new_doc.DocID;
                changePosition (newPosDoc, String(data.org.id), String(data.position.name));
                new_doc.TopElem.position_id = newPosDoc.DocID;
            }
		} else {    //редактирование должности сотрудника

            if ( data.HasProperty('org')&&String(data.org.id)!='') { //есть организация в запросе

                if(String(new_doc.TopElem.position_id)!=''&&String(new_doc.TopElem.org_id)!='6748398834638268537')  {  //есть не дефолтная должность
                    posDoc = OpenDoc(UrlFromDocID(Int(new_doc.TopElem.position_id)));
                    changePosition (posDoc, String(data.org.id), String(data.position.name));   
                } else {
                    newPosDoc = tools.new_doc_by_name('position');
                    newPosDoc.BindToDb(DefaultDb);
                    newPosDoc.TopElem.basic_collaborator_id = new_doc.DocID;
                    changePosition (newPosDoc, String(data.org.id), String(data.position.name));
                    new_doc.TopElem.position_id = newPosDoc.DocID;
                }
            } else {
                //ничего не делаем по идее
            }
        }
        //Position Block - End

        // new_doc.TopElem.org_id = Int(data.org.id);
        // new_doc.TopElem.org_name =  String(data.org.name);

        new_doc.Save();

        if (data.HasProperty('role')&&String(data.role.id)!='') { 
            addToGroup (new_doc.DocID, Int(data.role.id));

		if (String(data.role.id)=='6761406065601834023') {	//преподаватели


            if (ArrayOptFirstElem(XQuery("for $elem in lectors where $elem/person_id = "+XQueryLiteral(new_doc.DocID)+" return $elem"))==undefined) {
                var lectDoc = tools.new_doc_by_name('lector');
                lectDoc.BindToDb(DefaultDb);
                lectDoc.TopElem.type='collaborator'
                lectDoc.TopElem.person_id=new_doc.DocID;
                lectDoc.Save();
            }

			if(data.HasProperty('expert')&&String(data.expert)!='') {
				if (String(data.expert)=='true') {	//tools web is true я забыл
                    var expert = ArrayOptFirstElem(XQuery("for $elem in experts where $elem/person_id = "+XQueryLiteral(Int(new_doc.DocID))+" return $elem"));
                        
					if (expert == undefined) {
						var expertDoc = tools.new_doc_by_name('expert');
						expertDoc.BindToDb(DefaultDb);
						expertDoc.TopElem.type='collaborator'
						expertDoc.TopElem.person_id=new_doc.DocID;
						expertDoc.TopElem.person_fullname=String(data.lastname)+" "+String(data.firstname)+" "+String(data.middlename);
						expertDoc.Save();
                        expertID = String(expertDoc.DocID);
					} else { 
                        expertID = String(expert.id);
                    }
					//if (data.competences.length != 0) {
						deleteExpertFromCompetences(expertID); //сначала удаляем всех старых
						for (comp in data.competences) {
							compDoc = OpenDoc(UrlFromDocID(Int(comp.id)));
							newexpert = compDoc.TopElem.experts.AddChild(); //Добавляем нового
							newexpert.expert_id = expertID;
							compDoc.Save()
						}
					//}

				} else { //удалить эксперта, если снять галку
					var expert = ArrayOptFirstElem(XQuery("for $elem in experts where $elem/person_id = "+XQueryLiteral(Int(new_doc.DocID))+" return $elem"));
					if (expert != undefined) {


						if (new_doc.TopElem.files.ChildExists("file")) { //удаляем у коллаба экспертские файлы
							for (file in new_doc.TopElem.files) {
								file_doc = OpenDoc(UrlFromDocID(Int(file.file_id))).TopElem;
								if (String(file_doc.resource_type_id) == "6765428823044459124") { //если это паспорта ЭКСПЕРТА
									new_doc.TopElem.files.DeleteChildByKey(file_doc.DocID,"file_id");
									DeleteDoc(UrlFromDocID(Int(file_doc.DocID)));
								}
							}
						}


						deleteExpertFromCompetences(expert.id); //удалить его из компетенций
						DeleteDoc(UrlFromDocID(Int(expert.id)));
					}
				}
			}
		}

            allOldGroups = XQuery("for $elem in group_collaborators where $elem/group_id != "+String(data.role.id)+" and $elem/collaborator_id = "+String(new_doc.DocID)+" and contains($elem/code, 'edgr_')=false() return $elem"); //старые группы
            for (oldgroup in allOldGroups) {
                deleteFromGroup(oldgroup.collaborator_id, oldgroup.group_id);           
            }
        }

        if(String(new_doc.TopElem.position_id)!='')  OpenDoc(UrlFromDocID(Int(new_doc.TopElem.position_id))).Save(); //для отображения в админке

		if (!data.HasProperty('id')&&data.HasProperty('login')&&data.HasProperty('password')) {	//уведомление при регистрации
			var notification_data = {login: String(new_doc.TopElem.login), password: String(new_doc.TopElem.password)}; //данные для сообщения о регистрации
			tools.create_notification("copp_success_register", new_doc.DocID, tools.object_to_text(notification_data, 'json'), '', '',null);
		}

		return new_doc.TopElem;

	}	
	
	function deleteCollaborator (id) {
		//DeleteDoc(UrlFromDocID(Int(id)));
		//var collPosition = ArrayOptFirstElem(XQuery("for $elem in positions where $elem/basic_collaborator_id = "+Int(id)+" return $elem"));
		//if (collPosition != undefined) DeleteDoc(UrlFromDocID(Int(collPosition.id)));
		collDoc = OpenDoc(UrlFromDocID(Int(id)));
		collDoc.TopElem.access.web_banned = true;

	    allOldGroups = XQuery("for $elem in group_collaborators where $elem/collaborator_id = "+String(id)+" and contains($elem/code, 'edgr_')=false() return $elem"); //старые группы
	    for (oldgroup in allOldGroups) {
		deleteFromGroup(oldgroup.collaborator_id, oldgroup.group_id);           
	    }
	
	if (ArrayOptFirstElem(XQuery("for $elem in lectors where $elem/person_id = "+String(id)+"return $elem")) != undefined) {
		DeleteDoc(UrlFromDocID(ArrayOptFirstElem(XQuery("for $elem in lectors where $elem/person_id = "+String(id)+"return $elem")).id));
	}
	if (ArrayOptFirstElem(XQuery("for $elem in experts where $elem/person_id = "+String(id)+"return $elem")) != undefined) {
		DeleteDoc(UrlFromDocID(ArrayOptFirstElem(XQuery("for $elem in experts where $elem/person_id = "+String(id)+"return $elem")).id));
	}
        collDoc.Save(); //баним, а не удаляем
	}

	function getFileType(file_extension) {
		var result = "file";    //default
		switch (file_extension) {
			case 'swf': result = "swf"; break;
			case 'flv': result = "flv"; break;
			case 'jpg': case 'jpeg': case 'png': result = "img"; break;
			case 'ppt': case 'pptx': result = "ppt"; break;
			case 'doc': case 'docx': result = "doc"; break;
			case 'xls': case 'xlsx': result = "excel"; break;
			case 'pdf': result = "pdf"; break;     
			case 'wav': case 'mp3': result = "audio"; break;        
			case 'avi': case 'mp4': case 'wmv': result = "video"; break;           
		}
		return result;
	}


	function uploadFile(form) {

		var file = form.file;
		var changedUser = Int(form.user_id);
		bytes = StrLen( file.FileName );
		fileName = String( file.FileName );
		dots = fileName.split(".");
		fileType = String(dots[dots.length-1]);
	    
		newDoc = tools.new_doc_by_name('resource');
		newDoc.BindToDb(DefaultDb);
		localFile = fileName;
		xLocalFile = String("x-local://wt_data/attachments/" + newDoc.TopElem.id + "." + fileType);
		PutUrlData( xLocalFile, file );
	    
		if (FilePathExists( UrlToFilePath( xLocalFile ) )) {
		fileSize = UrlFileSize( xLocalFile );
		} else {
		fileSize = bytes;
		}
		newDoc.TopElem.file_url = xLocalFile;
		newDoc.TopElem.size = fileSize;
		newDoc.TopElem.name = fileName;
		newDoc.TopElem.file_name = fileName;
		newDoc.TopElem.type = getFileType(fileType);
		newDoc.TopElem.resource_type_id = Int(6765428823044459124); //паспорта ЭКСПЕРТА
		newDoc.Save();

		userDoc = OpenDoc(UrlFromDocID(Int(changedUser)));
		newFile = userDoc.TopElem.files.AddChild();
		newFile.file_id = newDoc.DocID;
		userDoc.Save();

            
	}

	
	var response = {};

	try {

		if(Request.Method == "GET") {
			if (Request.Query.HasProperty("id")) {
				response.collabInfo = openCollaborator(Request.Query.id);		
			} else if (Request.Query.HasProperty("teachers")) {
				var currentUser = {};
				currentUser.org = {"id": Int(curUser.org_id), "name": String(curUser.org_name)};
				if (ArrayOptFirstElem(XQuery("for $elem in group_collaborators where $elem/group_id = 6748398834638268528 and $elem/collaborator_id = "+XQueryLiteral(Int(curUserID))+" return $elem"))!=undefined) {
					var curUserRole = "copp";
					currentUser.isPopper = false;
					response.collaborators = getTeachers(curUserRole);	
				} else if (ArrayOptFirstElem(XQuery("for $elem in group_collaborators where $elem/group_id = 6761406065601834021 and $elem/collaborator_id = "+XQueryLiteral(Int(curUserID))+" return $elem"))!=undefined) {
					var curUserRole = "org_represent";
					currentUser.isPopper = true;
					response.collaborators = getTeachers(curUserRole);
				} else {response.error = "Ошибка доступа"}
				response.currentUser = currentUser;
				response.groupsList = getGroupsList();
				response.orgsList = getOrgsList();
				response.competences = getCompetences();
			} else {
				response.collaborators = getCollabs();
				response.groupsList = getGroupsList();
				response.orgsList = getOrgsList();
				var currentUser = {}
				currentUser.isEmployer = false;
				if (ArrayOptFirstElem(XQuery("for $elem in group_collaborators where $elem/group_id = 6761406065601834022 and $elem/collaborator_id = "+XQueryLiteral(Int(curUserID))+" return $elem"))!=undefined) {
					currentUser.isEmployer = true;
				}
				currentUser.org = {"id": String(curUser.org_id), "name": String(curUser.org_name)};
				response.currentUser = currentUser;
			}
		} else if (Request.Method == "POST") {
			var collabInfo = tools.read_object(Request.Body);
			//response.body = collabInfo;
			if (Request.Query.HasProperty('delete')) {
				deleteCollaborator(collabInfo.id);
			} else {
				if (Request.Query.HasProperty('addfile')) {
				    var requestForm = Request.Form;

				    if (requestForm.HasProperty("file")&&requestForm.HasProperty("user_id")) {
				        uploadFile(requestForm);
				    }
			    } else {
                    createOrModifyCollaborator(collabInfo);
                }

			}
		}

	} catch (err) {response.error = String(err);}

	Response.Write(tools.object_to_text(response, 'json'));
%>