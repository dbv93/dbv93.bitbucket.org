<%
var response = {};
var newDoc = '';

try {
   if ( Request.Form.HasProperty('obj_id') && Request.Form.GetProperty('obj_id') != ""&& Request.Form.HasProperty( 'img' ) && Request.Form.GetProperty( 'img' ) != "") {

	_data_ref = Request.Form.GetProperty( 'img' );
	bytes = StrLen( _data_ref );
	if (bytes >= 1000240) {
		response.photo_error = tools.get_web_str("upi_avatar_too_big");
	} else {
		fileName = String(_data_ref.FileName);
		dots = fileName.split(".");
		fileType = String(dots[dots.length-1]);
		if (fileType!='png' && fileType!='jpg' && fileType!='jpeg') {
			response.photo_error = tools.get_web_str("upi_avatar_wrong_type");
		} else {

			objDoc = OpenDoc ( UrlFromDocID ( Int (Request.Form.obj_id )));
			
			if ( String(objDoc.TopElem.resource_id) != '') {
				newDoc = OpenDoc( UrlFromDocID ( Int ( objDoc.TopElem.resource_id )));
				DeleteUrl(newDoc.TopElem.file_url);		//удаляем старый ресурс
			}

			if (newDoc == '') {
				newDoc = tools.new_doc_by_name('resource');
				newDoc.BindToDb(DefaultDb);
			}

			localFile = fileName;
			xLocalFile = String("x-local://wt_data/attachments/" + newDoc.TopElem.id + "." + fileType);
			PutUrlData( xLocalFile, _data_ref );

			if (FilePathExists( UrlToFilePath( xLocalFile ) )) {
				fileSize = UrlFileSize( xLocalFile );
			} else {
				fileSize = bytes;
			}

			newDoc.TopElem.file_url = xLocalFile;
			newDoc.TopElem.size = fileSize;
			newDoc.TopElem.name = fileName;
			newDoc.TopElem.file_name = fileName;
			newDoc.TopElem.type = "img";

			newDoc.Save();

			objDoc.TopElem.resource_id = newDoc.DocID;
			objDoc.Save();

			response.image_ref = "/download_file.html?file_id=" + String(newDoc.DocID);
		}
	}
   }
} catch (err) {response.err = String(err)}

Response.Write(tools.object_to_text(response, 'json'));
%>
