<%
/*### Функции - конец###*/

/*### Обработка запроса - начало###*/
try {
    response = {}
    
	if (Request.Method == 'POST') {

        if (Request.Query.HasProperty("xml")) response.data = tools.read_object(Request.Body)
        else response.error = 'Ошибка запроса - недостаточно данных'

	} else response.error = 'Ошибка запроса'
    
} catch (e) {
	response.error = String(e)
}

Response.Write(tools.object_to_text(response, 'json'))
/*### Обработка запроса - конец###*/
%>