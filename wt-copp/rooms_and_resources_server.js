<%
  function getResourse(org_id) {
    var result = [];
    var organization = {};
    var cc_resource = [];
    var place_id = org_id ? XQuery("for $elem in orgs where $elem/id=" + org_id + " return $elem/place_id")[0] : undefined;
    var resourcesQuery = place_id ? XQuery("for $elem in object_resources where $elem/place_id=" + place_id + " return $elem") : XQuery("for $elem in object_resources return $elem");
    response.organization = place_id;
    for (resource in resourcesQuery) {
      cc_resource = ArrayOptFirstElem(XQuery("for $elem in cc_resources where $elem/object_resource_id = " + Int(resource.id) + " return $elem"));
      demo = "Нет";
	cc_resource_competence = {id: '', name: ''};
      if (cc_resource != undefined) {
        if (String(cc_resource.demo) == "true") demo = "Да";
	if (String(cc_resource.competence)!='') {
		cc_query_competence = ArrayOptFirstElem(XQuery("for $elem in knowledge_parts where $elem/id = "+String(cc_resource.competence)+"return $elem"));
		if (cc_query_competence != undefined) cc_resource_competence = {id: Int(cc_query_competence.id), name: String(cc_query_competence.name)};
	}
      }
      organization = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/place_id = " + XQueryLiteral(resource.place_id) + " return $elem/Fields('id','name')"));

      result.push({
        id: Int(resource.id),
        organization: (organization != undefined ? organization : {}),
        name: String(resource.name),
	competence: cc_resource_competence,
        demo: String(demo),
        type: String(resource.type) == "room" ? "Помещение" : "Техническое средство"

      });
    }
    return result;
  }

function getDate(date) {
  if (!date) return;// StrXmlDate(new Date(), false);
  newDate = new Date(date);
  return StrXmlDate(newDate, false)
}


function getEventTypeName(type) {
return String(ArrayOptFirstElem(XQuery("for $elem in event_types where $elem/code = "+XQueryLiteral(String(type))+" return $elem")).name);

}


function getResourceEvents(resource_id) {
  var result = [];
  var competences = [];
  var resourceEvents = XQuery("for $elem in event_object_resources where $elem/object_resource_id = " + XQueryLiteral(Int(resource_id)) + " return $elem");
  for (res_event in resourceEvents) {
    resDoc = OpenDoc(UrlFromDocID(Int(res_event.event_id))).TopElem;
    competences = [];
    try {
        for (part in resDoc.knowledge_parts) {
          competences.push(part);
        }
    } catch(err) {result.push()}
    response.doc = resDoc;
    result.push({
      id: Int(res_event.event_id),
      name: String(res_event.name),
      start_date: getDate(res_event.start_date),
      finish_date: getDate(res_event.finish_date),
      type: String(getEventTypeName(res_event.type_id)),
      knowledge_parts: competences
    });
  }
  return result;
}

function getImg(id) {
  fileExist = ArrayOptFirstElem(XQuery("for $elem in resources where $elem/id = " + XQueryLiteral(id) + " return $elem")) != undefined;
  if (fileExist) {
    imgDoc = OpenDoc(UrlFromDocID(Int(id))).TopElem;
    obj = {};
    obj.id = String(id);
    obj.name = String(imgDoc.name);
    obj.url = "/download_file.html?file_id=" + String(id);
    return obj;
  }
}

function openResourse(resource_id) {
  var expert = {};
  var result = {};
var cc_resource_competence = {id: '', name: ''};
  //ходим по таблице object_resource
  var teResDoc = OpenDoc(UrlFromDocID(Int(resource_id))).TopElem;
  result.id = Int(resource_id);
  result.name = String(teResDoc.name);
  if (String(teResDoc.type) == "room") {
    result.type = "Помещение";
  } else {
    result.type = "Техническое средство";
  }
  result.description = String(teResDoc.small_desc);
  result.organization = ArrayOptFirstElem(XQuery("for $elem in orgs where $elem/place_id = " + XQueryLiteral(teResDoc.place_id) + " return $elem/Fields('id','name')"));
  //ходим по кастомной таблице cc_resource
  var cc_resource_doc = ArrayOptFirstElem(XQuery("for $elem in cc_resources where $elem/object_resource_id = " + Int(resource_id) + " return $elem"));
  if (cc_resource_doc != undefined) {

if (String(cc_resource_doc.competence)!='') {
	cc_query_competence = ArrayOptFirstElem(XQuery("for $elem in knowledge_parts where $elem/id = "+String(cc_resource_doc.competence)+" return $elem"));
	if (cc_query_competence != undefined) cc_resource_competence = {id: Int(cc_query_competence.id), name: String(cc_query_competence.name)};
}

    result.resource_id = Int(cc_resource_doc.id);
    doc_resource = OpenDoc(UrlFromDocID(Int(cc_resource_doc.id))).TopElem;
    result.image = String(doc_resource.resource_id) != '' ? getImg(Int(doc_resource.resource_id)) : {};
    if (String(doc_resource.demo) == "true") {
      result.demo = true;
      if (String(doc_resource.workplace_count).length) {
        result.workplace = Int(doc_resource.workplace_count);
      } else { result.workplace = "" }
      result.experts = [];
      if (doc_resource.experts.ChildExists("expert")) {
        for (expertObj in doc_resource.experts) {
          expert = ArrayOptFirstElem(XQuery("for $elem in experts where $elem/id = " + XQueryLiteral((Int(expertObj.expert_id))) + " return $elem"));
          result.experts.push({
            id: Int(expertObj.expert_id),
            name: String(expert.person_fullname),
          });
        }
      }
    } else {
      result.demo = false;
      result.workplace = "";
      result.experts = [];
    }



  }
  result.competence = cc_resource_competence;
  result.events = getResourceEvents(resource_id);
  return result;

}

function getOrgs(org_id) {
  var orgs = [];
  try {
    if (org_id) {
      var orgsQuery = XQuery("for $elem in orgs where $elem/id=" + org_id + " return $elem");
    } else {
      var orgsQuery = XQuery("for $elem in orgs where $elem/place_id != null() and $elem/code != 'copp' return $elem");
    }
    for (org in orgsQuery) {
      orgs.push({
        id: Int(org.id),
        name: String(org.disp_name)
      });
    }
    return orgs;

  } catch (err) { response.err = String(err); }
}


function getExperts(org_id) {
  var experts = [];
  var expertsQuery = XQuery("for $e in experts, $c in collaborators where $c/org_id = " + XQueryLiteral(Int(org_id)) + " and $e/person_id = $c/id return $e");
  for (expert in expertsQuery) {
    experts.push({
      id: Int(expert.id),
      name: String(expert.person_fullname)
    });
  }
  return experts;
}

function createOrModifyResourse(data) {

  var new_doc_resource; //кастомная таблица
  var new_doc_equipment; //таблица object_resources
  var type_id = "";
  var placeID = "";


  if (data.HasProperty('id')) {

    new_doc_equipment = OpenDoc(UrlFromDocID(Int(data.id)));
    cc_resource_doc = ArrayOptFirstElem(XQuery("for $elem in cc_resources where $elem/object_resource_id = " + XQueryLiteral(Int(data.id)) + " return $elem"));
    if (cc_resource_doc != undefined) {
      new_doc_resource = OpenDoc(UrlFromDocID(Int(cc_resource_doc.id)))
    } else {
      new_doc_resource = tools.new_doc_by_name('cc_resource');
      new_doc_resource.BindToDb(DefaultDb);
    }

  } else {
    //создание object_resource
    new_doc_equipment = tools.new_doc_by_name('object_resource');
    new_doc_equipment.BindToDb(DefaultDb);
    new_doc_equipment.Save();
    //создание кастомного сс_resource
    new_doc_resource = tools.new_doc_by_name('cc_resource');
    new_doc_resource.BindToDb(DefaultDb);
    new_doc_resource.Save();
  }
  //заполнение таблицы object_resources
  new_doc_equipment.TopElem.name = String(data.name);
  if (String(data.type) == "Помещение") {
    type_id = "room";
  } else {
    type_id = "bi3l3";
  }
  new_doc_equipment.TopElem.type = type_id;
  new_doc_equipment.TopElem.small_desc = String(data.description);
  if (String(data.organization) != "") {
    var org_Query = XQuery("for $elem in orgs where $elem/id = " + Int(data.organization) + " return $elem/Fields('place_id')");
    if (org_Query != undefined) {
      placeID = OptInt(ArrayOptFirstElem(org_Query).place_id, 0);
      if (placeID) new_doc_equipment.TopElem.place_id = placeID;
    }
  }
  new_doc_equipment.Save();


  //заполнение кастомной таблицы
  new_doc_resource.TopElem.name = String(data.name);
  new_doc_resource.TopElem.type = String(data.type);
  if (placeID != "") {
    new_doc_resource.TopElem.place_id = placeID;
  }
  new_doc_resource.TopElem.small_desc = String(data.description);
  if (String(data.demo) == "true") {
    new_doc_resource.TopElem.demo = true;
    new_doc_resource.TopElem.workplace_count = Int(data.workplace);
    if (data.experts.length) {
      new_doc_resource.TopElem.experts.Clear(); //Удаляем старых
      for (var i = 0; i < ArrayCount(data.experts); i++) {
        expertID = Int(data.experts[i].id);
        newexpert = new_doc_resource.TopElem.experts.AddChild(); //Добавляем нового
        newexpert.expert_id = expertID;
      }
    }
    else if (ArrayCount(ArrayDirect(new_doc_resource.TopElem.experts)) != 0) { new_doc_resource.TopElem.experts.Clear() }
  } else {
    new_doc_resource.TopElem.demo = false;
  }
  new_doc_resource.TopElem.object_resource_id = new_doc_equipment.DocID;
  if (data.HasProperty("competence")) new_doc_resource.TopElem.competence = OptInt(data.competence.id, '');
  new_doc_resource.Save();
  return new_doc_resource;
}

function deleteResourse(id) {
  cc_resource_doc = ArrayOptFirstElem(XQuery("for $elem in cc_resources where $elem/object_resource_id = " + XQueryLiteral(id) + " return $elem"));
  if (cc_resource_doc != undefined) DeleteDoc(UrlFromDocID(Int(cc_resource_doc.id)));
  DeleteDoc(UrlFromDocID(Int(id)));
}

function getCompetences() {
  var result = [];
  var objsQuery = XQuery("for $elem in knowledge_parts where $elem/knowledge_classifier_id = 6753954396609189672 return $elem");
  for (obj in objsQuery) {
    result.push({
      id: Int(obj.id),
      name: String(obj.name)
    });
  }
  return result;
}



var response = {};

try {

  if (Request.Method == "GET") {
    // Если это не цоппер, то вытаскиваем организацию этого человека
    var curRole = XQuery("for $elem in group_collaborators where $elem/code='copp' and $elem/collaborator_id=" + XQueryLiteral(curUserID) + " return $elem/group_id");
    if (ArrayCount(curRole) > 1) { curRole = curRole.join(','); } else { curRole = String(curRole[0]); }
    curRole = StrContains(curRole, "6748398834638268528");
    org_id = curRole ? undefined : String(OpenDoc(UrlFromDocID(Int(curUserID))).TopElem.org_id);
    response.org = org_id;
    if (Request.Query.HasProperty("id")) {
      response.resourceInfo = openResourse(Request.Query.id);
      response.orgs = getOrgs(org_id);
      response.competences = getCompetences();
    } else if (Request.Query.HasProperty('new')) {
      response.orgs = getOrgs(org_id);
    } else {
      response.resources = getResourse(org_id);
      response.today = StrXmlDate(new Date(), false)
      response.competences = getCompetences();
    }
  } else if (Request.Method == "POST") {
    var resourceInfo = tools.read_object(Request.Body);
    if (Request.Query.HasProperty('delete')) {
      deleteResourse(Int(resourceInfo.id));
    } else if (Request.Query.HasProperty('experts')) {
      response.experts = getExperts(resourceInfo.id);
    } else {
      response.id = createOrModifyResourse(resourceInfo).DocID;
    }

  }

} catch (err) { response.error = String(err); }

Response.Write(tools.object_to_text(response, 'json'));
%>