var objs_list = new Vue({
  el: '#vue-page',
  vuetify: new Vuetify({
    lang: {
      locales: {
        ru: ruLocale
      },
      current: 'ru'
    }
  }),

  data () {
    return {
      loader: true,
      loading: false,
      edit: false,
      headers: [{
        text: 'Заголовок', value: 'name', width: '100%'
      }, {
        text: 'Дата создания', value: 'create_date', align: 'center', width: 100, sort: this.sortingDates
      },{
        text: 'Модератор', value: 'cp_moderator', align: 'center', width: 150
      },{
        text: 'Статус', value: 'status', align: 'center'
      },
      { text: 'Действия', value: 'action', sortable: false },
      ],
      programs: [],
      dev: true,
      snack: {
          view: false,
          text: '',
          color: '',
          timeout: 6000,
          confirm: false,
          textConfirm: ''
      },
      status_dialog: {
          view: false,
          status: null,
          comment: '',
      },
      tab: null,
      Mode: "edit", //режим (редактирование/просмотр)
      insPlace: 0, //место (номер позиции) куда будет добавлен новый элемент
      dialog: false,
      dialogItem: false, //диалог редактирования и просмотра занятия
      dialogElem: false, //диалог выбора типа нового элемента
      dialogFile: false, //диалог списка файлов
      dialogUpload: false, //диалог выбора нового файла (нового или из списка)
      dialogUploadImage: false, //диалог выбора нового изображения (нового или из списка)
      dialogOfList: false, //диалог списков контента элементов разных типов (домашнее задание, лонгрид, тест)
      dialogPollTable: false, //диалог списка опросов (таблица)
      tempFiles: [], //массив временных файлов (ожидающих подтверждения выгрузки на сервер)
      curFile: [], //текущий файл
      fileFilter:false, //флаг для фильтра списка файлов lists.resource по изображениям
      curEl: [0,0,0], //Индекс текущего элемента (Модуль, занятие, элемент) - (program, item, element)
      elemScore: [
        {name:"Зачет" , type: "1"},
        {name:"Балл" , type: "100"}
      ],
      statusHeaders: [
        { text: ' Дата', value: ' Дата' },
        { text: ' Кто менял', value: ' Кто менял' },
        { text: ' Сменил со статуса', value: ' Сменил со статуса' },
        { text: ' Поменял на статус', value: ' Поменял на статус' },
        { text: ' Комментарий ', value: ' Комментарий ' },
      ],
      logArr: [],
      elementTypes:[ //типы элементов 
      //flags: [fileInput,link,ckeditor,icon,poll,actions,max_score-edit] - флаги вложенного контента
        {
          typeName:'вложенный файл',
          icon:'mdi-file',
          type: 'resource',
          flags:[1,0,0,1,0,0,1],
          accept:'.doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip',
          form_evaluation: 'Автоматически',
        },
        // {
        //   typeName:'дистанционный курс',
        //   type: 'lesson',
        //   icon:'mdi-book',
        //   flags:[1,0,0,1,0,0,0],
        //   accept:'.doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip',
        //   form_evaluation: 'Автоматически',
        // },
        // {
        //   typeName:'текстовый документ',
        //   type: 'resource',
        //   icon:'mdi-file-document',
        //   flags:[1,0,0,1,0,0,0],
        //   accept:'.docx,.pdf'
        // },
        // {
        //   typeName:'презентация',
        //   type: 'resource',
        //   icon:'mdi-file-powerpoint',
        //   flags:[1,0,0,1,0,0,0],
        //   accept:'.ppt,.pptx'
        // },
        {
          typeName:'внешняя ссылка',
          type: 'inline',
          icon:'mdi-link-variant',
          flags:[0,1,0,0,0,0,1],
          accept:'',
          form_evaluation: 'Автоматически',
        },
        {
          typeName:'лонгрид',
          type: 'longread',
          icon:'mdi-clipboard-text',
          flags:[0,0,1,0,0,1,1],
          accept:'',
          form_evaluation: 'Автоматически',
        },
        {
          typeName:'домашнее задание',
          type: 'learning_task',
          icon:'mdi-home',
          flags:[0,0,1,0,0,1,0],
          accept:'.doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip',
          form_evaluation: 'Вручную',
        },
        {
          typeName:'тест',
          type: 'test',
          icon:'mdi-file-check',
          flags:[0,0,0,1,0,1,0],
          accept:'.doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip',
          form_evaluation: 'Автоматически',
        },
        // {
        //   typeName:'опрос',
        //   type: 'poll',
        //   icon:'mdi-file-chart',
        //   flags:[0,0,0,0,1,0,1],
        //   accept:'.doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip',
        //   form_evaluation: 'Автоматически',
        // },
        // {
        //   typeName:'видео',
        //   type: 'resource',
        //   icon:'mdi-file-video',
        //   flags:[1,0,0,1,0,0,0],
        //   accept:'.mp4',
        //   form_evaluation: 'Автоматически',
        // },
        // {
        //   typeName:'электронный учебно-методический комплекс',
        //   type: 'lesson',
        //   icon:'mdi-file-image',
        //   flags:[1,0,0,1,0,0,0],
        //   accept:'.doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip',
        //   form_evaluation: 'Автоматически',
        // },
        // {
        //   typeName:'оценочные средства',
        //   type: 'lesson',
        //   icon:'mdi-file',
        //   flags:[1,0,0,1,0,0,0],
        //   accept:'.doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip',
        //   form_evaluation: 'Автоматически',
        // },
        // {
        //   typeName:'виртуальный тренажер',
        //   type: 'lesson',
        //   icon:'mdi-file',
        //   flags:[1,0,0,1,0,0,0],
        //   accept:'.doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip',
        //   form_evaluation: 'Автоматически',
        // }
      ],
      items: [
          'Общая характеристика', 'Учебная программа', 'Ресурсы (обеспечение)', 'Статус'
          ],
      valid: false,
      accept: {
          image: '.jpg,.png,.jpeg',
          file: '.doc,.docx,.pdf,.ppt,.pptx,.xls,.xlsx,.zip'
      },
      editor: false,
      editors: {},
      elemEditors: {},
      editorConfig: {
        language: 'ru',
        removePlugins: ['ImageUpload'],
        mediaEmbed: {
          previewsInData: true
        },
        onReady: this.onEditorReady,
      },
      editorItemConfig: {
        language: 'ru',
        removePlugins: ['ImageUpload'],
        mediaEmbed: {
          previewsInData: true
        },
        onReady: this.onEditorItemReady,
      },
      editorsConfig: {
        language: 'ru',
        removePlugins: ['ImageUpload'],
        readOnly: false,
        mediaEmbed: {
          previewsInData: true
        },
        onReady: this.onEditorsReady,
      },
      templateModule: {id: '', name: '', items: [], hidden: false},
      templateItem: {id: '', name: '', image: null, elements: [], desc: '', type: ''},
      templateAddInfo: {id: '', name: '', flags: ''},
      templatePerson: {id: '', name: '', image: null, org: this.templateItem, position: this.templateItem, worldskills: { accept: false, number: '', date: '', competence: '' }, addInfo: {} },
      editLesson: false,
      form_education: null,
      editedItem: {},
      defaultProgram: {
  
          /* General sets */
          test_program: false,
          id: null,
          name: '',
          cp_extra_classification_cl: '',
          cost: 0,
          listeners_category: '',
          issued_document: '',
          competence: '',
          profession: '',
          duration: 0,
          desc: '',
          image: null,
          educ_org: '',
          dev_org: '',
          cp_moderator: '',
          knowledge_level_cl: '',

          regulations: [],

          results: {
              profession: '',
              knowledges: [],
              skills: [],
              capability: []
          },

          persons: [],
      
          /* Program sets */
          program: [
          {
            id: '_1',
            name: 'Модуль 1',
            items: [],
            hidden: false
          }
          ],

          /* Resources sets */
          resources: {
              technical: [],
              education: []
          },

          cp_status: 'Проект',
          hidden: false,
          form_education: '',

      },
      editedItem: {},
      lists: {},

      showSelectTypeOfQuestions: false, // Переменная, отвечающие за показ полей выбора кол-ва вариантов ответа
      numberOfAnswers: new Array(36).fill(0).map((elem, index)=>index+2), // Количество вариантов ответов
      numberOfCurrentShowAnswers: 0, // Текущее количество показанных полей ввода под ответы
      showTitleOfQuestion: false, // Показать поле ввода вопроса,
      activeTab: 0, // Открытая вкладка в модальном окне (шаг1)

      poll: { // Структура опроса
        name: '', // Имя опроса
        // start_date: '', // Дата начала опроса
        // end_date: '', // Дата окончания опроса
        usersOfPoll: '', // Категория пользователей, для кого доступен опрос
        status: '', // Текущий статус опроса (проводится или завершен)
        questions: [], // Массив вопросов опроса
        multiPages: '' // Все опросы на 1 странице или каждый вопрос на разных.
      },
      question: { // Структура вопроса
          id: 0,
          title: '', // Имя вопроса
          type: '', // Тип вопроса
          entries: [] // Массив ответов на 1 вопрос
        },
      entry: { // Структура ответа
        id: '', // id ответа
        value: '', // Текст ответа
      },

      nameOfPollForStatistic: '',

      arrayForStatistic: [],


      arrayOfValuesForTable: [], // данные для таблицы вопросов

      isFormInEditMode: false, // Режим редактирования формы 

      selectedIdOfItemForEdit: 0, // Вытаскиваем id выбранного вопроса

      showUpdateButton: false,
      dialogShowStatistic: false,

      tempArrayQuestions: [],
      tempArrayShit: [],
      multiPagesOptions:[{
        text: 'Вопросы на одной станице',
        value: false
      },
      {
        text: 'Вопросы на разных страницах',
        value: true
      }],

      
      dialogPoll: false,
      dialog2: false,
      validPollForm: false,
      validQuestionForm: false,
      menu: false,
      menuDate: false,

      // dates: [],
      loading: false,
      polls: [],
      searchInput: '',
      search: '',
      canEdit: true,


      typeOfQuestions: [{  // Тип вопроса
        text: 'Единственный выбор',
        value: 'choice'
      },
      {
        text: 'Множественный выбор',
        value: 'select'
      },
      {
        text: 'Выпадающий список',
        value: 'combo'
      },
      {
        text: 'Cтрока',
        value: 'string'
      },
      {
        text: 'Текстовое поле',
        value: 'text'
      },
      {
        text: 'Число',
        value: 'number'
      }],

      usersCategory: [{ // Список - Кому доступен опрос
        text: 'Все', 
        value: 'all'
      },
      {
        text: 'Зарегистрированные пользователи', 
        value: 'registered'
      }],

      headersPoll: [{ // Заголовки главной таблицы
        text: 'Название', 
        value: 'name',
      },
      {
        text: 'Добавивший пользователь',
        value: 'creator',
        width: '150'
      },
      {
        text: 'Участники',
        value: 'participants_num',
      },
      {
        text: 'Статус опроса',
        value: 'status'
      },
      {
        text: 'Действия',
        value: 'action',
        sortable: false
      }],

      headersOfQuestions: [{
        text: 'Номер вопроса',
        value: 'id'
      },
      {
        text: 'Тип вопроса',
        value: 'type'
      },
      {
        text: 'Текст вопроса',
        value: 'title'
      },
      {
        text: 'Количество вариантов ответов',
        value: 'entries.length'
      },
      {
        text: 'Действия',
        value: 'action', sortable: false
      }],

      rules: {
          required: value => !!value || 'Необходимо заполнить',
          min8: value => value.length >= 8 || 'Не менее 8 знаков',
          email: value => {
          const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          return pattern.test(value) || 'Введите корректный e-mail'
          },
          numbers: value => {
          const pattern = /^[0-9]*$/
          return pattern.test(value) || 'Допустимы только цифры'
          },
          longestWord: value => {//«Тетрагидропиранилциклопентилтетрагидропиридопиридиновые» (55 букв, химич. вещество)
            let result = true;
            if(!value) return true;
            let str = value.split(' ');
            let lw = 0;//длина слова
            for(var i = 0; i < str.length; i++){
              if(str[i].length > lw){lw = str[i].length;}
            };
            if (lw>55){result = false;console.log('result: ', lw);};            
            return result||'Самое длинное слово больше 55, проверьте правильность написания';
          },
          link: value => {
            const pattern = /((ftp|http|https):\/\/)?[A-Za-zА-Яа-яёЁ0-9-\.]+\.[A-Za-zА-Яа-яёЁ]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/
            return pattern.test(value) || 'Введите корректный URL'
          },
          max20: value => value.length <= 20 || 'Не более 20 символов',
          max100: value => value.length <= 100 || 'Не более 100 символов',
          max250: value => value != null && ( value.length <= 250 || 'Не более 250 символов' ),
          maxSize1: value => value != null && ( !value.size || value.size < 1024000 || 'Изображение не более 1 MB!' ),
          maxSize2: value => value != null && ( !value.size || value.size < 2048000 || 'Файл не более 2 MB!' ),
      }
    }
  },

  computed: {
      formTitle () { // Меняем заголовок в зависимости от переменной (режим редактирования)
        return this.isFormInEditMode === false ? 'Создать опрос' : 'Редактирование'
      },

      bodyScroll () { //убираем в элементе body возможность прокрутки при открытии диалогов
        if (this.dialog || this.dialogItem || this.dialogUploadImage || this.dialogPollTable)
          document.getElementById('wt-body').style.overflow='hidden'
        else
          document.getElementById('wt-body').style.overflow='visible'
      },
    },

  methods: {
    newProgram (name) { //Запрос - создание новой модульной программы
      if (!name) { this.dialog = true } else {
        console.log('newProgram: ');
        const serverUrl = 'custom_web_template.html?object_id=6806399838008855147&new';
        fetch(serverUrl, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          body: JSON.stringify(this.defaultProgram)
        })
          .then(response => {
            if (response.status !== 200) return;
            console.log('response: ', response);
            return response.json();
          })
          .then(res => {
            console.warn('res--NP->put-----',res);
            if (res.error) this.setSnackMessage("error", res.error);
            if (res.data.id) {
              this.defaultProgram.id = null, this.defaultProgram.dev_org = '', this.defaultProgram.cp_moderator = '';
              this.editProgram(res.data, 'edit');
              return res;
            }
          }).then((res) => {  this.dialog = false; })
          .catch(err => {
            console.error('Err:', err); this.setSnackMessage("error", err);
            this.dialog = false;
          });
      }
    },

    createObj (element, l) { //Создание object в елементе занятия
      element.objectBlock = true
      element.object.id = null
      this.newEditorElem(l, true)
    },

    showEditLessonDialog(i, k) { //Открыть диалог редактирования/просмотра занятия
      this.getPolls();
      this.curPos(i,k,0);
      this.longreadCheck();
      this.dialogItem = true;
    },

    sortingDates(a, b) { //Сравнение дат
      newDate = (date) => +new Date([date.split('.')[1], date.split('.')[0], date.split('.')[2]].join('.'))
      if (newDate(a) > newDate(b)) return 1
      if (newDate(a) < newDate(b)) return -1
      return 0
    },

    linkElem (listElem) { //Привязка контента (домашнее задание, лонгрид, тест, опрос) к элементу занятия
      console.log('link-Elem->listElem: ', listElem);
      this.offEditorElem();
      let elem=(this.editedItem.program[this.curEl[0]].items.length)?this.editedItem.program[this.curEl[0]].items[this.curEl[1]].elements[this.curEl[2]]:{};
      elem.object = Object.assign(elem.object, listElem);
      elem.object.desc = listElem.desc;
      elem.object.name = listElem.name;
      elem.desc = listElem.desc;
      elem.name = listElem.name;
      this.longreadCheck();
    },

    newFile (multi) { //Запрос - выгрузка нового файла на сервер
        if (!this.editedItem.dev_org.hasOwnProperty('id')) { this.setSnackMessage("warning", "Выберите организацию разработчика"); return }
        let fd = new FormData;
        if (multi) {
          fd.append("files", this.curFile);
        } else {
          fd.append("file", this.curFile[this.curFile.length - 1]);
        }
        fd.append("object", JSON.stringify(this.tempFiles[0]));
        fd.append("dev_org", this.editedItem.dev_org.id);
        
        console.warn(fd);
        const serverUrl = 'custom_web_template.html?object_id=6806399838008855147&file';
        fetch(serverUrl, {
          method: 'POST',
          body: fd
        })
          .then(response => {
            if (response.status !== 200) return;
            console.log('response: ', response);
            return response.json();
          })
          .then(res => {
            console.warn('res----&file----',res);
            if (this.tempFiles.length) {
              this.tempFiles[0].id = res.data.id;
              this.saveFileToElem(res.data);
            };
            if (res.error) this.setSnackMessage("error", res.error);
            if (res.message) this.setSnackMessage("success", res.message, 2500);
          }).then((res) => {  this.dialog = false; })
          .catch(err => {
            console.error('Err:', err); this.setSnackMessage("error", err);
            this.dialog = false;
          });
    },
    
    fileRemove (file, index) { //Запрос - удаление файла на сервере
      const idTag = '&file-remove=' + file.id;
      const settings = {
        method: 'POST',
      };
      return this.queryFn( idTag, settings, res => {
        console.warn('res---',idTag,'----',res);
        console.log('delete file[',index,'] ', file.name);
        this.removeItem(index, this.lists.resources);
        console.log('this.lists.resources: ', this.lists.resources);
        if (res.error) this.setSnackMessage("error", res.error);
      }, 'loading')

    },
    
    addFileFromList (file) { //Привязка файла из списка к элементу занятия
      if (this.fileFilter) {
        this.editedItem.image = Object.assign(file);
      } else {
        let elem = (this.editedItem.program[this.curEl[0]].items.length)? this.editedItem.program[this.curEl[0]].items[this.curEl[1]].elements[this.curEl[2]]:[];
        elem.object.files[0] = Object.assign(file);
        if (elem.name === ''){elem.name = elem.object.files[0].name}
      }
      this.dialogFile = false;
      this.dialogUpload = false;
      this.dialogUploadImage = false;
      this.tempFiles=[];
    },

    editProgram (item, mode) { // открыть программу для редактирования/просмотра
        console.warn(item);
        this.getProgram(item.id).then(() => { this.Mode = mode; this.edit = true });
    },
    
    onEditorReady (editor, data) { // СК-редактор в разделе сведения о программе - инициализация по готовности
      this.editor = editor;
      editor.setData(data);
      setTimeout(() => { $('.ck-editor').css('width', '100%');$('.ck-editor').css('max-width', '100%'); }, 100);
    },
    
    onEditorsReady (editor, data, l,fromObject) {// СК-редакторы в элементах занятия - инициализация по готовности
      let i = this.curEl[0];
      let k = this.curEl[1];
      
      let key = i+'-'+k+'-'+l;
      if (fromObject) key += '-obj'
      this.elemEditors[key] = editor;
      editor.setData(data);
      if(this.editedItem.program[this.curEl[0]].items.length) {
        if (fromObject) {editor.model.document.on('change:data', () => { 
          this.editedItem.program[i].items[k].elements[l].object.desc = this.elemEditors[key].getData() 
        })} else {editor.model.document.on('change:data', () => { 
          this.editedItem.program[i].items[k].elements[l].desc = this.elemEditors[key].getData()
        })}
      }
      setTimeout(() => {
        $('.ck-editor').css('width', '100%');$('.ck-editor').css('max-width', '100%');
        this.loading = false;
      }, 0);
    },

    onEditorItemReady (editor, data) {// СК-редактор занятия - инициализация по готовности
      if (data === undefined) {data = ''};
      let i = this.curEl[0];
      let k = this.curEl[1];
      let key = i+'-'+k;
      this.elemEditors[key] = editor;
      editor.setData(data);
      editor.model.document.on('change:data', () => { 
        this.editedItem.program[i].items[k].desc = this.elemEditors[key].getData() 
      })
      setTimeout(() => {
        $('.ck-editor').css('width', '100%');$('.ck-editor').css('max-width', '100%');
        this.loading = false;
      }, 0);
    },
    
    offEditor () { // СК-редакторы - сохранение, отключение 
      let text = document.getElementById("text");
      if (this.editor) this.editedItem.desc = this.editor.getData();
      if (this.editor) this.editor.destroy();
      if (text&&text.nextSibling) text.nextSibling.remove();
      this.editor = false;
    },
    
    newEditor () { // СК-редактор сведения о программе - размещение в DOM
      setTimeout( () => {
        let text = document.getElementById("text");
        if (text&&!text.nextSibling) ClassicEditor.create(text, this.editorConfig).then(editor => { this.onEditorReady(editor, this.editedItem.desc); });
      }, 100);
    },

    newEditorItem () { // СК-редактор занятия - размещение в DOM
      this.loading = true;
      let i = this.curEl[0];
      let k = this.curEl[1];
      setTimeout( () => {
        let itemDOMelem = document.getElementById('item-editor-'+i+k);
        if (itemDOMelem&&!itemDOMelem.nextSibling){ClassicEditor.create(itemDOMelem, this.editorItemConfig)
          .then(editor => {this.onEditorItemReady(editor, this.editedItem.program[i].items[k].desc)})
        }
      }, 100);
    },
    
    newEditorElem (l,isObj) { // СК-редакторы элементов занятия - размещение в DOM
      this.loading = true;
      let i = this.curEl[0];
      let k = this.curEl[1];
      setTimeout( () => {
        let elem = document.getElementById('text'+i+k+l);
        let obj = document.getElementById('object-text'+i+k+l);
        if (!isObj&&elem&&!elem.nextSibling){ClassicEditor.create(elem, this.editorsConfig)
          .then(editor => {this.onEditorsReady(editor, this.editedItem.program[i].items[k].elements[l].desc,l)})
        }
        if (isObj&&obj&&!obj.nextSibling){ClassicEditor.create(obj, this.editorsConfig)
          .then(editor => {this.onEditorsReady(editor, this.editedItem.program[i].items[k].elements[l].object.desc,l,true)})
        }
      }, 100);
    },
    
    saveEditor () { // СК-редактор сведения о программе - сохранение
      if (this.editor) this.editedItem.desc = this.editor.getData();
      this.newEditor();
    },

    saveItemEditor () { // СК-редактор занятия - сохранение
      let i = this.curEl[0];
      let k = this.curEl[1];
      let key = i+'-'+k;
      if (this.elemEditors[key]) this.editedItem.program[i].items[k].desc = this.elemEditors[key].getData();
      this.newEditorItem();
    },

    elemEditorsSave () { // СК-редакторы элементов занятия - сохранение
      try {
        let i = this.curEl[0];
        let k = this.curEl[1];
        let arr = (this.editedItem.program[i].items.length)?this.editedItem.program[i].items[k].elements:[];
        let editors = this.elemEditors;
        for (let l = 0; l < arr.length; l++) {
          let key = i+'-'+k+'-'+l;
          let element = arr[l];
          if (element&&editors[key]) {element.desc = editors[key].getData();};
          key += '-obj';
          if (element&&editors[key]) {element.object.desc = editors[key].getData();};
        }
      } catch (error) {};
    },
    
    offEditorElem () { // СК-редакторы элементов занятия - сохранение, отключение
      let object = this.elemEditors;
      this.elemEditorsSave();
      for (const key in object) {
        if (object.hasOwnProperty(key)) {
          if (object[key]) {
            try {object[key].destroy()} catch (error) {}
          };
          object[key]=false;
        }
      }
      let ckeditors = document.querySelectorAll(".ck");
      ckeditors.forEach(el => el.remove());
    },

    setSnackMessage (color, text, timeout, confirm, textConfirm) { // вывод всплывающего сообщения
      this.snack.color = color;
      this.snack.text = text;
      this.snack.timeout = timeout != undefined ? timeout : 6000;
      this.snack.confirm = confirm;
      this.snack.textConfirm = textConfirm;
      this.snack.view = true;
    },

    createEventForSave () {
      window.onbeforeunload = (e) => { 
        var e = e || window.event;
        if (e) {
          if (confirm('Вы хотите сохранить последнюю версию?'))this.saveEditor();// this.saveProgramInLocalStorage();
        }
      };
    },

    getItem (id, arr) { // получение элемента массива arr по id
      if (id.hasOwnProperty('id')) id = id.id;
      let result = arr.find(item => item.id == id);
      return result;
    },

    addItem (template, arr) { // добавление элемента из шаблона template в конец массива arr с уникальным id
      let item = JSON.parse(JSON.stringify(template));
      item.id = "_" + Date.now().toString().slice(-9);
      arr.push(item);
      template.name = "";
    },

    removeItem (index, arr) { // удаление элемента массива по индексу
      arr.splice(index,1);
    },

    addModuleInProgram () { // добавление модуля в программу
      let module = JSON.parse(JSON.stringify(this.templateModule));
      module.id = "_" + Date.now().toString().slice(-9);
      if (this.editedItem.test_program) module.name = '#Тестовая программа# - ' + module.name
      this.editedItem.program.push(module);
      this.templateModule.name = "";
    },

    addItemInModule (module, form) { // добавление занятия в модуль
      let item = JSON.parse(JSON.stringify(this.templateItem));
      item.id = "_" + Date.now().toString().slice(-9);
      if (this.editedItem.test_program) item.name = '#Тестовая программа# - ' + item.name
      item.course_id = "_" + Date.now().toString().slice(-9);
      if (form) {
        item.form_education = form
        if (item.form_education.name.includes("Очное")) item.form_evaluation = this.lists.form_evaluation.find( item => item.name.includes("Вручную"))
      }
      module.items.push(item);
      this.templateItem.name = "";
      this.form_education = null
      this.$refs["form-0"][0].resetValidation()
    },

    removeModuleInProgram (id) { // удаление модуля
      this.editedItem.program = this.editedItem.program.filter( item => item.id !== id );
    },

    curPos (i,k,l) { // установить текущий индекс: модуль, занятие, элемент
      if(!(this.curEl[0]===i&&this.curEl[1]===k&&this.curEl[2]===l)) {
        this.curEl = [i,k,l];
      };
    },

    debounce (fn, delay) { // установка задержки при добавлении элемента
      let debounceTimeout = null;
      return function () {
        clearTimeout(debounceTimeout)
        let args = arguments;
        let that = this;
        debounceTimeout = setTimeout(function () {
          fn.apply(that, args)
        }, delay)
      }
    },

    addElemToItem (m) { // создание нового элемента занятия
      // indexes: i-Program(module), k-item, m-moduleType
      let i = this.curEl[0];
      let k = this.curEl[1];
      let arr = this.editedItem.program[i].items[k].elements;
      let elem = {};
      let form_evaluation = this.lists.form_evaluation.filter(el=>el.name==this.elementTypes[m].form_evaluation)
      elem = {
              id: "_" + Date.now().toString().slice(-9),
              type: this.elementTypes[m].type,
              name: '',
              code: null,
              desc: '',
              is_visible: true,
              objectBlock: false,
              max_score: "1",
              form_evaluation: form_evaluation[0].id,
              files: [],
              object: {
                id: null,
                name: '',
                desc: '',
                files: [],
                comment: null,//редактор
              }
            };
      this.offEditorElem();
      arr.splice(this.insPlace,0,elem);
      this.dialogElem = false;
      this.longreadCheck();
    },

    addTempfile (elem,file) {
      try {
        if (file) {
        console.log('file: ', file);
        this.curFile[0] = file;
          const tfile = {
            'lastModified'     : file.lastModified,
            'lastModifiedDate' : file.lastModifiedDate,
            'name'             : file.name,
            'size'             : file.size,
            'type'             : file.type,
            'url'              : '',
            'id'               : null
          }
          if (!this.fileFilter) {tfile.object = elem};
          if (this.editedItem.test_program) tfile.name = '#Тестовая программа# - ' + tfile.name
          if(file.size<2000000){
            this.tempFiles = [];
            this.tempFiles.push(tfile);
          };
          if (!this.fileFilter) { document.getElementById('tempfileInput').value = '' }
          else { document.getElementById('progImg').value = '' };
        };

      } catch (error) {}
    },

    saveFileToElem (data) { // привязка нового файла к элементу занятия
      let i = this.curEl[0];
      let k = this.curEl[1];
      let l = this.curEl[2];
      if (!this.fileFilter) {
      if(this.editedItem.program.length && this.editedItem.program[i].items.length){
        let elem = this.editedItem.program[i].items[k].elements[l];
        elem.object.files[0]=this.tempFiles[0];
        if (elem.name===''){elem.name=elem.object.files[0].name};
      }} else {
        this.editedItem.image = this.tempFiles[0];
      }
      this.tempFiles = [];
      this.fileFilter = false;
    },

    removeElem () { // удаление элемента занятия
      console.log("removeElem");
      this.offEditorElem();
      let arr = (this.editedItem.program[this.curEl[0]].items.length)?this.editedItem.program[this.curEl[0]].items[this.curEl[1]].elements:[];
      arr = arr.splice(this.curEl[2],1);
      this.longreadCheck();
    },

    copyElem (elemIndex, arr) { // копирование элемента занятия
      console.log("copyElem");
      let elem = Object.assign({}, arr[elemIndex]);
      elem.id = "_" + Date.now().toString().slice(-9);
      arr = arr.splice(arr.length,0,elem);
      this.elemEditorsSave();
      this.longreadCheck();
    },

    longreadCheck () { // проверка актуальности СК-редакторов
      try {
        this.newEditorItem();
        let i = this.curEl[0];
        let k = this.curEl[1];  
        let arr = (this.editedItem.program[this.curEl[0]].items.length)?this.editedItem.program[i].items[k].elements:[];
        this.offEditorElem();
        for (let l = 0; l < arr.length; l++) {
          let key = i+'-'+k+'-'+l;
          if (arr[l].max_score==null) arr[l].max_score = 1;
          this.elemEditors[key]=1;
          this.newEditorElem(l,false);
          key = i+'-'+k+'-'+l+'-obj';
          if ((arr[l].type==='learning_task')||(arr[l].type==='longread')){
            this.elemEditors[key]=1;
            this.newEditorElem(l,true);
          }else{
            this.elemEditors[key]=0;
          }
        }
      } catch (error) {}
    },

    swap (index,delta,arr) { // поменять местами 2 элемента
      this.offEditorElem();
      let b = Object.assign(arr[index]);
      arr[index] = arr[index+delta];
      arr[index+delta] = b;
      this.longreadCheck();
      return arr;
    },

    getLists () { // Запрос - получение списков (организации, персоны, и т.д.)
      const addURL = '&lists';
      return this.queryFn(addURL, undefined, res => {
          console.warn('res--this.lists----',res);
          if (res.error) this.setSnackMessage("error", res.error);
          this.lists = res.data;
        }, 'loading')
    },

    getProgram (id) { // Запрос - полунение программы
      const idTag = id ? ('&id=' + id) : '&programs';
      return this.queryFn( idTag, undefined, res => {
        console.log('get-prog->res: ', res);
        if (res.error) this.setSnackMessage("error", res.error);
        if (Array.isArray(res.data)) {
          this.programs = res.data;
        } else {
          if (res.data.id) {
            this.editedItem = res.data;
          }
        this.newEditor();
        this.getPolls();
        }
        if (res.hasOwnProperty("lists")) {
          this.uploadedFiles = res.lists.resources;
          this.lists = Object.assign(this.lists, res.lists);
        }
        if (this.editedItem.status_log) { this.statusLogParse();}
      }, this.edit ? 'loading' : 'loader')
    },
    statusLogParse () {
      this.editedItem.status_log = this.editedItem.status_log.slice(1);
      this.logArr = this.editedItem.status_log.split('/');
      this.logArr = this.logArr.map(str => '\{"'+str.split('<brk>').join('","').split('<splt>').join('":"')+'"\}');
      this.logArr = this.logArr.map(str => JSON.parse(str));
      console.log('logArr: ', this.logArr);
    },
    sendProgram () { // Запрос - выгрузка программы на сервер
      console.log('send Program: ')
      if (!this.editedItem.cp_status.includes('Проект')) {
        this.setSnackMessage("error", 'Ошибка! Вы можете изменять программу только в режиме проекта.'); return
      }
      const settings = {
        method: 'POST',
        headers: {'Content-Type': 'application/json;charset=utf-8'},
        body: JSON.stringify(this.editedItem),
      };
      const idTag = '&id=' + this.editedItem.id;
      return this.queryFn( idTag, settings, res => {
        console.warn('res-------',idTag,'-----', res);
        if (res.error) this.setSnackMessage("error", res.error);
        if (res.warning) this.setSnackMessage("warning", res.warning);
        if (res.data && res.data.hasOwnProperty('id')) {
          this.setSnackMessage('success', 'Программа успешно сохранена', 2500);
          this.editedItem = res.data;
        }    
      }, 'loading')
    },

    setStatus () { // Запрос - установка статуса
      const settings = {
        method: 'POST',
        headers: {'Content-Type': 'application/json;charset=utf-8'},
        body: JSON.stringify(this.status_dialog),
      }
      const idTag = '&status=' + this.editedItem.id;
      this.status_dialog.view = false;
      return this.queryFn( idTag, settings, res => {
        console.warn('res-------',idTag,'-----', res);
        if (res.error) this.setSnackMessage("error", res.error);
        else if (res.data == "success") {
          this.setSnackMessage("success", "Статус успешно изменён", 2500);
          this.status_dialog.comment = '';
          this.getProgram(this.editedItem.id);
        }      
          }, 'loading')
    },

    newElement (element, name) { //Запрос - Создание домашнего задания или лонгрида, добавление в элемент
      if (!this.editedItem.dev_org.hasOwnProperty('id')) { this.setSnackMessage("warning", "Выберите организацию разработчика"); return }
      if (this.editedItem.test_program) element.name = '#Тестовая программа# - ' + element.name
      const settings = {
        method: 'PUT',
        headers: {'Content-Type': 'application/json;charset=utf-8'},
        body: JSON.stringify(element),
      };
      const idTag = '&org_id=' + this.editedItem.dev_org.id + '&' + name;
      return this.queryFn( idTag, settings, res => {
        console.warn('res-------', idTag,' = new-' + name + ' = ', res);
        if (res.error) this.setSnackMessage("error", res.error);
        else if (res.data == "success") {
          this.setSnackMessage("success", "Элемент успешно создан", 2500);
          this.getProgram(this.editedItem.id);
          this.longreadCheck();
        }
        console.warn(res.data)
        element.object.id = res.data.id;
      }, 'loading')
    },
    
    queryFn (addPath, settings, callback, loading) {//Функция запросов
      this[loading] = true;
      const serverUrl = 'custom_web_template.html?object_id=6806399838008855147' + addPath;
      return fetch(serverUrl,settings)
        .then(response => {
          if (response.status !== 200) return;
          return response.json();
        })
        .then(res => { callback(res); this[loading] = false })
        .catch(err => {
          console.error('Err:', err); this.setSnackMessage("error", err);
          this[loading] = false;
        })
    },

    typeProp (type) { // получение элемента масива elementTypes, по его типу
      let m=0;
      for (let i = 0; i < this.elementTypes.length; i++) {
        if (this.elementTypes[i].type===type){m=i; break;}
      };
      return this.elementTypes[m];
    },

    changeCourseType () { // получение элемента масива elementTypes, по его типу
      if (this.editedItem.program[this.curEl[0]].items[this.curEl[1]].form_education.name.includes("Дистанционное")) return
      else this.editedItem.program[this.curEl[0]].items[this.curEl[1]].form_evaluation = this.lists.form_evaluation.find( item => item.name.includes("Вручную"))
    },


                    //--------------------POLLS-----------------------
    changeUsers (role) {
      this.poll.usersOfPoll = role;
    },
    
    addQuestionEntry () {   //Добавить новый вариант ответа
        if (this.question.entries.length < 100) { 
        this.entry.id = this.question.entries.length + 1;
        this.question.entries.push(JSON.parse(JSON.stringify(this.entry)));
        // console.log(this.question.entries)
        }
    },
    
    deleteQuestionEntry (entryIndex) {  //Удалить вариант ответа по индексу
        if (this.question.entries.length > 2) {
            this.question.entries.splice(entryIndex,1);
            this.question.entries.forEach((el,index) => { el.id = index + 1});
            //console.log(this.question.entries)
        }
    },
    
    showThisNumberOfAnswers () { // Показывает варианты ответов в зависимости от выбранного количества в select'e
      this.question.entries = [] // Сброс массива ответов
      for ( i = 0; i < this.numberOfCurrentShowAnswers; i++) { // В зависимости от кол-ва вариантов ответа записываем в массив ответов такое же количество пустых объектов (с итерируемым id)
        this.entry.id = i;
        this.question.entries.push(JSON.parse(JSON.stringify(this.entry)));
      }
    },
    
    showNextTab () {
      this.activeTab = 1;
    },
    
    showThisTypeOfNewQuestions () { // Отображает поля в соответствии с типом выбранного вопроса
      this.showTitleOfQuestion = true;

      for (key in this.showTypeOfQuestions) { // Закрытие всех лишних полей
        this.showTypeOfQuestions[key] = false;
      }

      if (this.question.type === 'choice' || this.question.type === 'select' || this.question.type === 'combo') { // Показываем нужны поля в зависимости от типа вопроса
        this.showSelectTypeOfQuestions = true

        if (!this.question.entries.length) {
            this.addQuestionEntry()
              this.addQuestionEntry()
        }

      }
      else { // При показе вопросов типа "строка", "текстовое поле", "число" -> делаем сброс массива ответов + убираем поля и чистим значения
        this.showSelectTypeOfQuestions = false;
        this.numberOfCurrentShowAnswers = 0;
        this.resetCreateQuestion ();
      }
    },

    resetCreateQuestion () { // Сброс создания вопроса
      this.entry.id = '', // сброс id вопроса
      this.entry.value = '' // сброс value вопроса
      this.showThisNumberOfAnswers() // Скрываем поля с вариантами ответов
    },

    addNewQuestion () {
      this.question.id = this.poll.questions.length // id вопроса равен длине массива вопросов

      if (this.question.type === 'number' || this.question.type === 'string' || this.question.type=== 'text') { // Если вопрос этих типов -> id = 0, значение ответа(value) = null
        this.entry.id = 0
        this.entry.value = null
        this.question.entries.push(JSON.parse(JSON.stringify(this.entry))) // Пушим в вопрос все варианты ответов
      }

      this.poll.questions.push(JSON.parse(JSON.stringify(this.question))) // Пушим собранный вопрос в опрос
      this.arrayOfValuesForTable.push(this.reverseMyObject((JSON.parse(JSON.stringify(this.question))), true)) // Функция записывает в массив изменненый объект для отрисовки в таблице
      this.question.title= '' // Сброс заголовка
      this.resetCreateQuestion () // Сброс создания вопроса
      this.dialog2 = false;
      this.question.entries = [];
      this.numberOfCurrentShowAnswers = 0;
      this.$refs.questionForm.resetValidation();
      this.setSnackMessage('green', 'Вопрос успешно добавлен!');
    },

    savePollToElem (item) { // прикрепление опроса к элементу занятия
      if (this.polls.length) {
        let elem = this.editedItem.program[this.curEl[0]].items[this.curEl[1]].elements[this.curEl[2]];
        elem.object.id = item.id;
        elem.object.name = item.name;
        if (elem.name==='') elem.name = item.name;
        this.dialogPollTable = false;
      }
    },

    delIDFromElem (element) { // отвязка от элемента занятия контента (операция обратная linkElem и save-PollToElem)
      this.offEditorElem();
      element.name = '';
      element.desc = '';
      element.object = {
        id: null,
        name: '',
        desc: '',
        files: [],
        comment: null,//редактор
      }
      this.longreadCheck();
    },
    
    saveMyPoll () { // Сохранение опроса
      this.poll.id = "_" + Date.now().toString().slice(-9);
      this.pushEditedTableArrayToQuestionsArray ();
      $.ajax({
        method: "POST",
        data: JSON.stringify(this.poll),
        contentType: false,
        processData: false,
        url: "custom_web_template.html?object_id=6753954396609189744&org_id=" + this.editedItem.dev_org.id,
        success: (response) => {
          let res = JSON.parse(response);
          this.poll.name = res.body.name;
          this.poll.id = res.body.id;
          this.close();
          this.clearMyForm();
          this.$refs.form.resetValidation();
          this.getPolls();
          setTimeout(() => this.savePollToElem(this.polls.filter(el=>el.name==res.body.name)[0]),100);
          this.setSnackMessage('green', 'Опрос успешно сохранен!');
        },
        error: (err) => { 
          this.setSnackMessage("red", "Ошибка при добавлении/изменении вакансии: " + err);
        }
      });
    },

    getPolls() { // Получить массив опросов с сервера
      this.loading = true;
      $.ajax({
        url: "custom_web_template.html?object_id=6753954396609189744&org_id=" + this.editedItem.dev_org.id,
        success: (response) => {
          try {
            this.polls = JSON.parse(response).data;
            // this.canEdit = JSON.parse(response).canEdit;
          } catch (err) { 
            this.setSnackMessage("red", "Ошибка получения данных " + err);
            console.error(err) 
            }
        },
        error: (err) => { 
          this.setSnackMessage("red", "Ошибка получения данных " + err);
        },
        complete: () => { this.loading = false }
      });
    },

    close () { // Закрытие диалогового окна
      this.dialogPoll = false;
      setTimeout(() => {
        this.clearMyForm();
        this.$refs.form.resetValidation();
        this.isFormInEditMode = false;
      }, 300)
    },

    closeQuestionDialog () {
      this.dialog2 = false;
    },

    showMyForm () { // Открытие окна
      this.dialogPoll = true;
    },

    async download(item) {
      let dataFromServer = await 
      $.ajax({
        method: "GET",
        url: `custom_web_template.html?object_id=6753954396609189744&id=${item.id}`,
        success: (response) => {
          return;
        },
        error: (err) => { 
          this.setSnackMessage("red", "При скачивании статистики произошла ошибка. Попробуйте еще раз. Ошибка:" + err);
        }
      })

      const currentStatistic = JSON.parse(dataFromServer).data
      currentStatistic.pollsRes.forEach((element) => {
        element.type = this.getCorrectNameOfTypeQuestions(element.type)
      })
      console.log(currentStatistic)
      var newDiv = document.createElement("div");

      newDiv.innerHTML = `
      <style>
      @page portrait_A4_page  {
        size:595.3pt 841.9pt;
        margin:72.0pt 72.0pt 72.0pt 72.0pt;
        mso-header-margin:35.4pt;
        mso-footer-margin:35.4pt;
        mso-paper-source:0;
      }   
        h1 {font-family: "Calibri", Georgia, Serif; font-size: 16pt;}
        p {font-family: "Calibri", Georgia, Serif; font-size: 14pt;}


        table, tr, td, th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 5px;
        text-align: left;
      }

      .test {
        border: none;
      }


      .header-main {
        font-size: 30pt;
        margin: 15pt 15pt 15pt 15pt;
      }

      .header_copp {
        font-size: 10pt;
        line-height: 12pt;
        text-align: right;
        margin: 0;
      }

      </style>
      <center class="header-main">${currentStatistic.name}</center>
      <center>
      <table>
        <tr>
          <th>Статус опроса</th>
        </tr>
        <tr>
          <td>${currentStatistic.status}</td>
        </tr>
      </table>
      </center>
      
      `

      currentStatistic.pollsRes.forEach(element => {
        var innerDiv = document.createElement("div");

        innerDiv.innerHTML = `
        <strong><p>Текст вопроса: </strong>${element.question}</p>
        <strong><p>Тип вопроса: </strong>${element.type}</p>
        `

        let table = document.createElement("table");
        let tr = document.createElement("tr");

        table.innerHTML = `
          <table">
            <tr>
              <th>№</th>
              <th>Вариант ответа</th>
              <th>Выбрали этот ответ</th>
            </tr>
          </table>
        `
        element.answers.forEach((item,index)=> {
          if (item.counter === null) {
            item.counter= ''
          }
          let tr = document.createElement("tr");
          tr.innerHTML = `
          <tr>
            <td>${index + 1}</td>
            <td>${item.answer}</td>
            <td>${item.counter}</td>
          </tr>
          `
          table.querySelector('tbody').appendChild(tr)
          innerDiv.appendChild(table)
        })

        newDiv.appendChild(innerDiv)

      })
      
      const date = new Date()
      this.Export2Doc(newDiv, `Статистика от:${date.toLocaleDateString('ru-RU', {day: 'numeric', month: 'long', year: 'numeric', hour: '2-digit', minute: '2-digit' } ) }`)
    },
    
    Export2Doc(element, filename = ''){
      var preHtml = `<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'>
      <head>
      <meta charset='utf-8'>
      <title>Statistic</title>
      <xml>
      <w:worddocument xmlns:w="#unknown">
        <w:view>Print</w:view>
        <w:zoom>90</w:zoom>
        <w:donotoptimizeforbrowser />
      </w:worddocument>
    </xml>
      </head>
      <body>
      <div class="portrait_A4_page">`;
      var postHtml = "</div></body></html>";
      var html = preHtml+element.innerHTML+postHtml;



      var blob = new Blob(['\ufeff', html], {
          type: 'application/msword'
      });

      console.log(blob)
      
      // Specify link url
      var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
      
      // Specify file name
      filename = filename?filename+'.doc':'document.doc';
      
      // Create download link element
      var downloadLink = document.createElement("a");

      document.body.appendChild(downloadLink);
      
      if(navigator.msSaveOrOpenBlob ){
          navigator.msSaveOrOpenBlob(blob, filename);
      }else{
          // Create a link to the file
          downloadLink.href = url;
          
          // Setting the file name
          downloadLink.download = filename;
          
          //triggering the function
          downloadLink.click();
      }
    
      document.body.removeChild(downloadLink);
    },

    editPoll(item) { // Редактирование опроса
      $.ajax({
        method: "GET",
        url: `custom_web_template.html?object_id=6753954396609189744&id=${item.id}`,
        success: (response) => {
          this.clearMyForm();
          this.poll = JSON.parse(response).data;
          this.poll.questions.forEach((question) => {
            this.arrayOfValuesForTable.push(this.reverseMyObject((JSON.parse(JSON.stringify(question))), true))
          });
          this.isFormInEditMode = true;
          this.showUpdateButton = false;
          this.dialogPoll = true;
          this.savePollToElem(item);
        },
        error: (err) => { 
          this.setSnackMessage("red", "При редактировании опроса произошла ошибка. Попробуйте еще раз. Ошибка:" + err);
          // console.warn('error', err)
        }
      })
    },

    editQuestion (iTakeThisQuestionToChangeIt) { // при редактировании вопроса его значение вставляются в input'ы и показываеться кнопка обновить вопрос
      this.showUpdateButton = true
      this.InsertValuesOfThisQuestionIntoInputs (this.reverseMyObject((JSON.parse(JSON.stringify(iTakeThisQuestionToChangeIt))), false))
      this.selectedIdOfItemForEdit = iTakeThisQuestionToChangeIt.id
      this.dialog2 = true
    },

    saveMyEditedQuestion () { // Сохранение отредактированного вопроса
      this.poll.questions[this.selectedIdOfItemForEdit] = JSON.parse(JSON.stringify(this.question)) // Пушим собранный вопрос в опрос
      this.arrayOfValuesForTable = [];
      this.poll.questions.forEach((question) => {
      this.arrayOfValuesForTable.push(this.reverseMyObject((JSON.parse(JSON.stringify(question))), true))
      })
      this.showUpdateButton = false;
      this.question.title= ''; // Сброс заголовка
      this.resetCreateQuestion (); // Сброс создания вопроса
      this.dialog2 = false;
      // this.$refs.questionForm.resetValidation()
      this.setSnackMessage('green', 'Вопрос успешно обновлён!');
    },

    InsertValuesOfThisQuestionIntoInputs (questionForEdit) { // Вставка значений полученного вопроса в input'ы
      this.question.id = questionForEdit.id;
      this.question.type = questionForEdit.type;
      this.showThisTypeOfNewQuestions ();
      this.numberOfCurrentShowAnswers = questionForEdit.entries.length;
      this.showThisNumberOfAnswers();
      this.question.title = questionForEdit.title;
      this.question.entries = questionForEdit.entries;
    },

    reverseMyObject(object, state) { // В таблице вопросов данные должны быть в человекочитаемом виде - эта функция их преобразует и возвращает объект для этой таблицы (работает в обе стороны)
      if (state) { // Если нужно переделать объект для таблицы, то...
        this.typeOfQuestions.forEach((element) => {
          if (element.value === object.type) {
            object.type = element.text;
          }
        })
      } else {
          this.typeOfQuestions.forEach((element) => {
            if (element.text === object.type) {
              object.type = element.value;
            }
          })
        }
        return object;
    },

    deletePoll(item) { // Удаление опроса
      if (confirm('Вы действительно хотите удалить опрос?')) {
        $.ajax({
          method: "POST",
          data: JSON.stringify(item),
          url: "custom_web_template.html?object_id=6753954396609189744&delete",
          success: (response) => {
            // console.warn('Опрос удален', JSON.parse(response))
            this.getPolls();
            if(this.editedItem.program[this.curEl[0]].items.length)
              this.delIDFromElem(this.editedItem.program[this.curEl[0]].items[this.curEl[1]].elements[this.curEl[2]]);
            this.setSnackMessage('green', 'Опрос успешно удалён!');
          },
          error: (err) => { 
            this.setSnackMessage("red", "Ошибка при удалении опроса: " + err);
            // console.log('error', err) 
          }

        })
      }
    },

    deleteQuestion(item) { // При удалении вопроса пересобираем массив вопросов
      this.dialog2 = true;
      const index = this.arrayOfValuesForTable.indexOf(item);
      confirm('Вы действительно хотите удалить вопрос?') && this.arrayOfValuesForTable.splice(index, 1);

      this.pushEditedTableArrayToQuestionsArray();
      this.dialog2 = false;
      this.setSnackMessage('green', 'Вопрос успешно удалён!');
    },

    pushEditedTableArrayToQuestionsArray () { // перезаписываем значения id в правильно порядке (сверху вниз) и пушим преобразованные объекты в массив для дальнейшей отправки на сервер
      let counter = 0;
      this.poll.questions = [];
      this.arrayOfValuesForTable.forEach((element) => {
        element.id = counter;
        counter += 1;
        this.poll.questions.push(this.reverseMyObject((JSON.parse(JSON.stringify(element))), false))
      })
    },

    clearMyForm() { // сброс полей формы + переменных отвечающих за select и тд.
      this.showSelectTypeOfQuestions = false;
      this.numberOfCurrentShowAnswers = 0;
      this.showTitleOfQuestion = false;
      this.activeTab = 0 ;
      this.poll.name = '';
      this.poll.usersOfPoll = '';
      this.poll.questions = [];
      this.poll.multiPages = '';
      this.question.id = 0;
      this.question.title = '';
      this.question.type =  '',
      this.question.entries =  [];
      this.entry.id = '';
      this.entry.value = '';
      this.arrayOfValuesForTable = [];
      this.multiPages = '';
    },

    upThisQuestion (elem) { // Поменять местами вопросы (с верхним)
      if (elem.id === 0) {
        return;
      } 
      this.correctArr(elem.id, elem.id - 1);
      this.pushEditedTableArrayToQuestionsArray();
    },

    downThisQuestion (elem) { // Поменять местами вопросы (с нижним)
      if (elem.id === this.arrayOfValuesForTable.length - 1) {
        return
      } 
      this.correctArr(elem.id, elem.id + 1)
      this.pushEditedTableArrayToQuestionsArray()
    },

    correctArr (firstElementId, secondElementId) { // Метод для перестановки 2 элементов в массиве по id
      this.arrayOfValuesForTable[firstElementId] = this.arrayOfValuesForTable.splice(secondElementId,1, this.arrayOfValuesForTable[firstElementId])[0]
    },

    getCorrectArray () {
      this.tempArrayQuestions.forEach((element, index) => {
        element.entries.forEach(item => {
          for (key in this.tempArrayShit[index]) {
            if(item.value === key) item.stat = this.tempArrayShit[index][key];
          }
        })
      })
    },

    showFullInfo (item) {
        $.ajax({
        method: "GET",
        url: `custom_web_template.html?object_id=6753954396609189744&id=${item.id}`,
        success: (response) => {
          response = JSON.parse(response);
      if (response.error) {
          this.setSnackMessage("red", "Ошибка при открытии опроса.");
          console.error(response.error)
      } else {
        iGetThisPoll = response.data
        console.log(iGetThisPoll)
        this.nameOfPollForStatistic =  iGetThisPoll.name
        this.arrayForStatistic = iGetThisPoll.pollsRes
        this.arrayForStatistic.forEach((element) => {
          element.type = this.getCorrectNameOfTypeQuestions(element.type)
        })
          this.dialogShowStatistic = true
      }
        },
        error: (err) => {
          this.setSnackMessage("red", "Ошибка:" + err);
        }
      })

    },
    
    getCorrectNameOfTypeQuestions(string) {
      switch (string) {
        case 'number':
            return 'Число'
            break;
        case 'choice':
            return 'Единственный выбор'
            break;
        case 'select':
            return 'Множественный выбор'
            break;
        case 'combo':
            return'Выпадающий список'
            break;
        case 'string':
            return 'Строка'
            break;
        case 'text':
            return 'Текстовое поле'
            break;
        default:
            break;
        }
      },
  },

  mounted () {
    this.createEventForSave();
    this.getProgram().then(() => this.getLists());
    this.$el.classList.remove('hidden');
  },

  watch: {
    bodyScroll () { //убираем в элементе body возможность прокрутки при открытии диалогов
      if (this.dialog || this.dialogItem || this.dialogUploadImage || this.dialogPollTable)
        {document.getElementById('wt-body').style.overflow='hidden'}
      else
        {document.getElementById('wt-body').style.overflow='visible'}
    },
    
    dialogPoll (val) {
      if (val === false && this.isFormInEditMode === true) {
        this.clearMyForm()
        this.$refs.form.resetValidation()
      }
      if (val === false) {
        this.activeTab = 0
        this.isFormInEditMode = false
      }
    },

    dialog2 (val) {
      if (val === false && this.isFormInEditMode === true) {
      this.showUpdateButton = false
      this.question.title= '' // Сброс заголовка
      this.resetCreateQuestion () // Сброс создания вопроса
      this.question.entries = []
      this.numberOfCurrentShowAnswers = 0
      }
    },

    dialogItem () {
      if (!this.dialogItem || !this.$refs["form-1"]) return
      this.$refs["form-1"].resetValidation()
    },

    // dialogUploadImage (val) {
    //   if (!val) this.fileFilter = false;
    // }
  }
});